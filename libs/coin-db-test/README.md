# Coin collector - Coin database test library

This is library providing helpers for creating [Coin database](../../dbs/coin-db/README.md) for testing.

## How to use

Include in `settings.gradle` of your project

    includeBuild '${REPOSITORY_ROOT}/libs/coin-db-test'

Add `testCompile` dependency to your project

    dependencies {
        testCompile 'com.jellysource.coin:coin-db-test'
    }

Then in your `junit` test class you can use `TestDbRule`

    @Rule
    public TestDbRule testDbRule = new TestDbRule();

Rule will create by default in memory database and apply all migration scripts there. It povides **jooq context**  (via `getJooqContext`) which can be used for access to test db.

## How to use with PostgreSQL

When you want to use local PostgreSQL instance instead of in memory database then you can add following to your build script

    test {
        systemProperty 'db', project.findProperty('db')
        systemProperty 'db.name', project.findProperty('db.name') ?: 'quotecenter'
        systemProperty 'db.user', project.findProperty('db.user') ?: 'quotecenter'
        systemProperty 'db.password', project.findProperty('db.password')
    }

And set minimally `db` property during build

    ./gradlew build -Pdb=local-postgres

Then for each test new schema with unique name will be created in configured database and migration scripts will be applied to this schema.

>If you do not have local PostgreSQL instance you can use docker image
>
>`docker run -it -e POSTGRES_DB=quotecenter -e POSTGRES_USER=qutecenter -p 5432:5432 postgres:9.6-alpine`
    