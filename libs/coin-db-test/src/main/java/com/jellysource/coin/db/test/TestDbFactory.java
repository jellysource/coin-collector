package com.jellysource.coin.db.test;

public interface TestDbFactory {
    TestDb create();
}
