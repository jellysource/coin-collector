package com.jellysource.coin.db.test;

import org.jooq.DSLContext;
import org.junit.rules.ExternalResource;

public class TestDbRule extends ExternalResource {
    private final TestDbFactory testDbFactory;

    private TestDb testDb;

    public TestDbRule() {
        this(new SystemPropertiesAwareFactory());
    }

    public TestDbRule(TestDbFactory testDbFactory) {
        this.testDbFactory = testDbFactory;
    }

    @Override
    protected void before() {
        testDb = testDbFactory.create();
    }

    @Override
    protected void after()  {
        if (testDb != null) {
            testDb.close();
        }
    }

    public TestDb getDb() {
        if (testDb == null) {
            throw new IllegalStateException("Test db not initialized yet. Before method of rule need to be called first.");
        }
        return testDb;
    }

    public DSLContext getJooqContext() {
        return getDb().getJooqContext();
    }
}
