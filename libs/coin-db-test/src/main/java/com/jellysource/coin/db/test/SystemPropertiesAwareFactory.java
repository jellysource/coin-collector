package com.jellysource.coin.db.test;

public class SystemPropertiesAwareFactory implements TestDbFactory {

    @Override
    public TestDb create() {
        if ("local-postgres".equals(System.getProperty("db"))) {
            return createInLocalPostgres();
        }
        return TestDb.createInMemory();
    }

    private static TestDb createInLocalPostgres() {
        String db = getMandatoryProperty("db.name");
        String user = getMandatoryProperty("db.user");
        String password = getOptionalProperty("db.password");
        return TestDb.createInLocalPostgres(db, user, password);
    }

    private static String getMandatoryProperty(String name) {
        String property = System.getProperty(name);
        if (property == null || property.trim().isEmpty()) {
            throw new RuntimeException("Missing system property `" + name + "`");
        }
        return property;
    }

    private static String getOptionalProperty(String name) {
        String property = System.getProperty(name);
        return property != null ? property : "";
    }

}
