package com.jellysource.coin.db.test;

public class TestDbConfig {
    private final String driver;
    private final String jdbcUrl;
    private final String user;
    private final String password;

    public TestDbConfig(String driver, String jdbcUrl, String user, String password) {
        this.driver = driver;
        this.jdbcUrl = jdbcUrl;
        this.user = user;
        this.password = password;
    }

    public String getDriver() {
        return driver;
    }

    public String getJdbcUrl() {
        return jdbcUrl;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public static TestDbConfig anonymous(String driver, String jdbcUrl) {
        return new TestDbConfig(driver, jdbcUrl, "", "");
    }
}
