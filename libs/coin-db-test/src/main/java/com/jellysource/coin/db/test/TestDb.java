package com.jellysource.coin.db.test;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.DatabaseException;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

public class TestDb {
    private final Connection connection;
    private final DSLContext jooqContext;

    private TestDb(Connection connection) {
        this.connection = connection;
        this.jooqContext = DSL.using(connection);
    }

    public static TestDb createInMemory() {
        return create(TestDbConfig.anonymous(
            "org.hsqldb.jdbc.JDBCDriver",
            "jdbc:hsqldb:mem:test"
        ));
    }

    public static TestDb createInLocalPostgres(String db, String user, String password) {
        return create(new TestDbConfig(
            "org.postgresql.Driver",
            "jdbc:postgresql://localhost/" + db,
            user,
            password
        ));
    }

    private static TestDb create(TestDbConfig config) {
        try {
            Class.forName(config.getDriver());
            Connection connection = DriverManager.getConnection(config.getJdbcUrl(), config.getUser(), config.getPassword());
            JdbcConnection jdbc = new JdbcConnection(connection);
            String schema = generateSchemaName();
            createSchema(jdbc, schema);
            applyChangelog(jdbc, schema);
            return new TestDb(connection);
        } catch (ClassNotFoundException | SQLException | LiquibaseException e) {
            throw new RuntimeException(e);
        }
    }

    public void close() {
        try {
            connection.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private static void createSchema(JdbcConnection jdbc, String schema) throws SQLException, DatabaseException {
        try (Statement statement = jdbc.createStatement()) {
            statement.execute("CREATE SCHEMA " + schema);
        }
    }

    public Connection getConnection() {
        return connection;
    }

    public DSLContext getJooqContext() {
        return jooqContext;
    }

    private static String generateSchemaName() {
        return (
            "TEST__" 
            + LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME)
            + "__" 
            + UUID.randomUUID()
        ).replaceAll("[-:.\\s]", "_");
    }

    private static void applyChangelog(JdbcConnection jdbc, String schema) throws LiquibaseException {
        Database db = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(jdbc);
        Liquibase liquibase = new Liquibase("db/changelog/db.changelog-master.xml", new ClassLoaderResourceAccessor(), db);
        liquibase.setChangeLogParameter("schema", schema);
        liquibase.dropAll();
        liquibase.update("test");
    }
}
