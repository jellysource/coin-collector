# Coin collector - Coin database java model library

Library containing java classes describing [Coin database](../../dbs/coin-db/README.md). Classes are generated using [jOOQ framework](https://www.jooq.org/).

## How to build for HSQLDB

    ./gradlew clean build

## How to build for PostgreSQL

    ./gradlew clean build -Pdb=local-postgres

You need to have locally runnig PostgreSQL database with database **quotecenter** with user **quotecenter** without password
 
>If you do not have local PostgreSQL instance you can use docker image
>
>`docker run -it -e POSTGRES_DB=quotecenter -e POSTGRES_USER=quotecenter -p 5432:5432 postgres:9.6-alpine`