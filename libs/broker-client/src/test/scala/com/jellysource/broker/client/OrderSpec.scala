package com.jellysource.broker.client

import com.jellysource.broker.api.protocol
import com.jellysource.broker.api.protocol._
import org.scalatest.{FlatSpec, Matchers}

class OrderSpec extends FlatSpec with Matchers {

  "Order" should "be converted to place command when buy limit" in {
    buyLimitOrder.toPlaceCommand shouldBe buyLimitCmd
  }

  it should "be converted to place command when sell stop" in {
    sellStopOrder.toPlaceCommand shouldBe sellStopCmd
  }

  it should "be converted to place command when sell market with generated id" in {
    sellMarketOrder.toPlaceCommand shouldBe sellMarketCmd
  }

  private val buyLimitOrder = Order.buy(10, "FB").limit(200.02).withID("id")
  private val buyLimitCmd = PlaceOrder(protocol.Order(
    id = "id",
    symbol = "FB",
    quantity = 10,
    action = Buy,
    execution = Limit(200.02)
  ))

  private val sellStopOrder = Order.sell(1, "T").stop(31.78).withID("id")
  private val sellStopCmd = PlaceOrder(protocol.Order(
    id = "id",
    symbol = "T",
    quantity = 1,
    action = Sell,
    execution = Stop(31.78)
  ))

  private val sellMarketOrder = Order.sell(1, "KO").market().withUUID()
  private val sellMarketCmd = PlaceOrder(protocol.Order(
    sellMarketOrder.id,
    symbol = "KO",
    quantity = 1,
    action = Sell,
    execution = Market
  ))

}
