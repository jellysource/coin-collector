package com.jellysource.broker.client.result

import java.util.function.Consumer

import scala.util.{Failure, Success, Try}

/**
  * Try like wrapper for client results to improve usage from java.
  * In scala you can call toTry and get scala Try object instead.
  */
sealed trait Result[T] {
  def isSuccess: Boolean
  def onSuccess(callback: Consumer[T]): Result[T]
  def onFailure(callback: Consumer[BrokerClientException]): Result[T]
  def get: T
  def toTry: Try[T]

  lazy val isFailure: Boolean = !isSuccess

  def process(onSuccess: Consumer[T], onFailure: Consumer[BrokerClientException]): Unit =
    if (isSuccess) this.onSuccess(onSuccess) else this.onFailure(onFailure)
}

case class SuccessResult[T](content: T) extends Result[T] {
  override def isSuccess: Boolean = true
  override def onSuccess(callback: Consumer[T]): Result[T] = { callback.accept(content); this }
  override def onFailure(callback: Consumer[BrokerClientException]): Result[T] = this
  override def get: T = content
  override def toTry: Try[T] = Success(content)
}

case class FailedResult[T](error: BrokerClientException) extends Result[T] {
  override def isSuccess: Boolean = false
  override def onSuccess(callback: Consumer[T]):  Result[T] = this
  override def onFailure(callback: Consumer[BrokerClientException]):  Result[T] = { callback.accept(error); this }
  override def get: T = throw new IllegalStateException("Cannot get content from failed result")
  override def toTry: Try[T] = Failure(error)
}

// use instead Unit in Results without content
sealed trait Done
case object Done extends Done
