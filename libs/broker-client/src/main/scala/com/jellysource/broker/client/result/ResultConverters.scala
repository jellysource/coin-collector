package com.jellysource.broker.client.result

import java.net.ConnectException

import scalaj.http.HttpRequest

import scala.util.Try

private[client] trait ResultConverters {

  implicit class HttpRequestOps(request: HttpRequest) {

    def asResult[T](implicit parseBody: Array[Byte] => T): Result[T] = {
      try {
        doRequestAndParseBody(parseBody)
      } catch {
        case e: ConnectException => FailedResult(ConnectionFailed(e))
        case other: Throwable => FailedResult(Unknown(other))
      }
    }

    private def doRequestAndParseBody[T](parse: Array[Byte] => T): Result[T] = {
      val response = request.asBytes
      if (response.isSuccess)
        Try(parse(response.body)).fold(e => FailedResult(InvalidResponseBody(e)), SuccessResult.apply)
      else
        FailedResult(InvalidResponseCode(received = response.code, expected = "2xx"))
    }
  }

  implicit object DoneBodyParser extends (Array[Byte] => Done) {
    override def apply(v1: Array[Byte]): Done = Done
  }
}

object ResultConverters extends ResultConverters
