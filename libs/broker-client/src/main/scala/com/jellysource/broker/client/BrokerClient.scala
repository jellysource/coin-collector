package com.jellysource.broker.client

import com.jellysource.broker.api.protocol.Order
import com.jellysource.broker.api.protocol.json.JsonSerDe
import com.jellysource.broker.client.result.{Done, Result}
import com.jellysource.broker.client.result.ResultConverters._
import scalaj.http.Http


class BrokerClient(config: BrokerClientConfig)  {

  private lazy val orders = Http(s"${config.uri}/orders").header("Content-Type", "application/json")

  def placeOrder(order: Order): Result[Done] =
    orders.postData(JsonSerDe.serializeOrder(order)).asResult

}
