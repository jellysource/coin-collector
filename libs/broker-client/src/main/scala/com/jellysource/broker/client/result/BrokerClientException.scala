package com.jellysource.broker.client.result

sealed abstract class BrokerClientException(message: String, cause: Throwable = null) extends RuntimeException(message, cause)

case class ConnectionFailed(cause: Throwable)
  extends BrokerClientException("There was error in connection", cause)

case class InvalidResponseCode(received: Int, expected: String)
  extends BrokerClientException(s"Invalid response code received: $received. Expected: $expected.")

case class InvalidResponseBody(cause: Throwable)
  extends BrokerClientException("There was error during parsing response body", cause)

case class Unknown(cause: Throwable)
  extends BrokerClientException("Unknown error occured", cause)
