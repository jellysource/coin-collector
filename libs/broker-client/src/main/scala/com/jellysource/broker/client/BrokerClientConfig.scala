package com.jellysource.broker.client

case class BrokerClientConfig(
  uri: String
)

object BrokerClientConfig {
  def create(uri: String): BrokerClientConfig = BrokerClientConfig(uri)
}
