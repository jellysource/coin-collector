package com.jellysource.broker.api.protocol

import java.time.OffsetDateTime


sealed trait OrderEvent {
  def id: String
  def time: OffsetDateTime
  def typeName: String
}

case class OrderCreated(
  id: String,
  time: OffsetDateTime,
  symbol: String,
  quantity: Int,
  action: Action,
  execution: Execution
) extends OrderEvent {
  override def typeName: String = "CREATED"
}

case class OrderPlaced(
  id: String,
  time: OffsetDateTime
) extends OrderEvent {
  override def typeName: String = "PLACED"
}

case class OrderFilled(
  id: String,
  time: OffsetDateTime,
  price: BigDecimal
) extends OrderEvent {
  override def typeName: String = "FILLED"
}

case class OrderFilledPartially(
  id: String,
  time: OffsetDateTime,
  quantity: Int,
  price: BigDecimal
) extends OrderEvent {
  override def typeName: String = "FILLED_PARTIALLY"
}

case class OrderCancelled(
  id: String,
  time: OffsetDateTime
) extends OrderEvent {
  override def typeName: String = "CANCELLED"
}
