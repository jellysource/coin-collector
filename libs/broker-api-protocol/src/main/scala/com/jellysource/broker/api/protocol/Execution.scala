package com.jellysource.broker.api.protocol

sealed trait Execution
case object Market extends Execution
case class Limit(price: BigDecimal) extends Execution
case class Stop(price: BigDecimal) extends Execution
