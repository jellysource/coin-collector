package com.jellysource.broker.api.protocol

sealed trait OrderCommand
case class PlaceOrder(order: Order) extends OrderCommand
case class CancelOrder(id: String) extends OrderCommand