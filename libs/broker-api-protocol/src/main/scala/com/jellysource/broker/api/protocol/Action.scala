package com.jellysource.broker.api.protocol

sealed trait Action
case object Sell extends Action
case object Buy extends Action