package com.jellysource.broker.api.protocol

import java.util.UUID

import com.jellysource.broker.api.protocol

/**
  * Order representation
  *
  * Use builder methods buy / sell to create new orders.
  *
  * {{{
  *   Order.sell(10, "FB").market().withUUID()
  *   Order.sell(10, "FB").limit(10).withID("my-id")
  *   Order.sell(10, "FB").stop(20).withUUID()
  *   Order.buy(1, "T").market().withUUID()
  * }}}
  *
  */
case class Order(
  id: String,
  symbol: String,
  quantity: Int,
  action: Action,
  execution: Execution
) {

  def toPlaceCommand: PlaceOrder =
    PlaceOrder(
      order = protocol.Order(
        id = id,
        symbol = symbol,
        quantity = quantity,
        action = action,
        execution = execution
      )
    )
}
object Order {

  def sell(quantity: Int, symbol: String): OrderWithoutExecutionBuilder =
    new OrderWithoutExecutionBuilder(Sell, quantity, symbol)

  def buy(quantity: Int, symbol: String): OrderWithoutExecutionBuilder =
    new OrderWithoutExecutionBuilder(Buy, quantity, symbol)
}

class OrderWithoutExecutionBuilder(action: Action, quantity: Int, symbol: String) {

  def market(): OrderWithoutIdBuilder = build(Market)
  def limit(price: BigDecimal): OrderWithoutIdBuilder = build(Limit(price))
  def stop(price: BigDecimal): OrderWithoutIdBuilder = build(Stop(price))

  private def build(execution: Execution): OrderWithoutIdBuilder =
    new OrderWithoutIdBuilder(action, quantity, symbol, execution)
}

class OrderWithoutIdBuilder(action: Action, quantity: Int, symbol: String, execution: Execution) {

  def withUUID(): Order = withID(UUID.randomUUID().toString)

  def withID(id: String): Order =
    Order(
      id = id,
      symbol = symbol,
      quantity = quantity,
      action = action,
      execution = execution
    )
}