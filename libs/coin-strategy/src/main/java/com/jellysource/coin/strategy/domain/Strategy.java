package com.jellysource.coin.strategy.domain;


import javaslang.collection.List;

public interface Strategy {

  ExecParams getExecParams();

  List<Signal> process(StrategyContext strategyContext, DataFeed dataFeed);
}
