package com.jellysource.coin.strategy.domain;

import javaslang.collection.List;
import lombok.AllArgsConstructor;
import lombok.Value;
import lombok.experimental.Wither;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static java.math.BigDecimal.valueOf;


@Value
@Wither
@AllArgsConstructor
public class Position {

  String symbol;
  List<Trade> trades;

  public Integer totalShares() {
    return trades.foldLeft(0, (acc, trade) -> acc + trade.getShares());
  }

  public BigDecimal averagePrice() {
    return totalCost().divide(valueOf(totalShares()), RoundingMode.HALF_DOWN);
  }

  public BigDecimal totalCost() {
    Number sum = trades.map(trade ->
        trade
            .getPrice()
            .multiply(valueOf(trade.getShares()))
    ).sum();
    return valueOf(sum.floatValue());
  }

  public Integer size() {
    return trades.size();
  }
}
