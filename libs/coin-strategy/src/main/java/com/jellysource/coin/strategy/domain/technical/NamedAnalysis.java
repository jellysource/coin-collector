package com.jellysource.coin.strategy.domain.technical;

import com.jellysource.coin.strategy.domain.Analysis;
import com.jellysource.coin.strategy.domain.BarData;
import javaslang.collection.List;
import org.ta4j.core.Bar;
import org.ta4j.core.BaseBar;
import org.ta4j.core.Decimal;
import org.ta4j.core.Indicator;

import java.math.BigDecimal;
import java.time.ZoneId;
import java.util.function.Supplier;


public abstract class NamedAnalysis implements Analysis {

    private String name;

    public NamedAnalysis(String name) {
        this.name = name;
    }

    protected java.util.List<Bar> toBars(List<BarData> bars) {
        return bars.map(bar -> (Bar) new BaseBar(
            bar.getDateTime().atZone(ZoneId.systemDefault()),
            bar.getOpen().doubleValue(),
            bar.getHigh().doubleValue(),
            bar.getLow().doubleValue(),
            bar.getClose().doubleValue(),
            bar.getVolume().doubleValue()
        )).reverse().toJavaList();
    }

    protected List<BarData> toBarData(List<BarData> bars, Indicator<Decimal> indicator) {
        return bars
            .zipWithIndex()
            .map(t -> t._1.withAnalysis(name, indicatorValue(indicator, bars.size() - 1 - t._2)));
    }

    private Supplier<BigDecimal> indicatorValue(Indicator<Decimal> indicator, Long index) {
        return () ->
            BigDecimal.valueOf(
                indicator.getValue(index.intValue()).doubleValue()
            );
    }
}
