package com.jellysource.coin.strategy.domain;


public enum SignalType {
    BUY, SELL
}
