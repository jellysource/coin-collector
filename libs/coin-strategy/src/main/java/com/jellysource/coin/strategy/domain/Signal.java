package com.jellysource.coin.strategy.domain;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Value;

import java.math.BigDecimal;

import static java.math.BigDecimal.ZERO;
import static java.math.BigDecimal.valueOf;


@Value
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Signal {

    String symbol;
    Integer quantity;
    SignalType type;
    SignalExecution signalExecution;
    BigDecimal price;


    public static SignalBuilder buy(String symbol) {
        return new SignalBuilder(SignalType.BUY, symbol);
    }

    public static SignalBuilder sell(String symbol) {
        return new SignalBuilder(SignalType.SELL, symbol);
    }

    public Boolean isBuy() {
        return SignalType.BUY.equals(type);
    }

    public Boolean isSell() {
        return SignalType.SELL.equals(type);
    }

    public static class SignalBuilder {
        SignalType type;
        String symbol;
        Integer quantity;

        SignalBuilder(SignalType type, String symbol) {
            this.type = type;
            this.symbol = symbol;
        }

        public SignalBuilder quantity(Integer quantity) {
            this.quantity = quantity;
            return this;
        }

        public Signal at(SignalExecution execution) {
            return new Signal(symbol, quantity, type, execution, ZERO);
        }

        public Signal at(SignalExecution execution, Double price) {
            return new Signal(symbol, quantity, type, execution, valueOf(price));
        }
    }
}
