package com.jellysource.coin.strategy.domain.technical;

import com.jellysource.coin.strategy.domain.BarData;
import javaslang.collection.List;
import org.ta4j.core.BaseTimeSeries;
import org.ta4j.core.TimeSeries;
import org.ta4j.core.indicators.SMAIndicator;
import org.ta4j.core.indicators.helpers.ClosePriceIndicator;


public class SimpleMovingAverage extends NamedAnalysis {

    private Integer period;

    public SimpleMovingAverage(Integer period, String name) {
        super(name);
        this.period = period;
    }

    @Override
    public List<BarData> apply(List<BarData> bars) {
        TimeSeries ts = new BaseTimeSeries(toBars(bars));
        ClosePriceIndicator cpe = new ClosePriceIndicator(ts);
        SMAIndicator sma = new SMAIndicator(cpe, period);

        return toBarData(bars, sma);
    }

}
