package com.jellysource.coin.strategy.domain;


import javaslang.collection.List;
import lombok.Value;
import lombok.experimental.Wither;

import java.time.LocalDateTime;

@Value
@Wither
public class TickerData {

    String ticker;
    List<BarData> bars;

    public TickerData withAnalysis(Analysis analysis) {
        return withBars(bars.transform(analysis));
    }

    public TickerData withMaxDateBars(LocalDateTime dateTime) {
        return withBars(bars.filter(bar -> dateTime.compareTo(bar.getDateTime()) >= 0));
    }

    public enum Ordering {
        ASCENDING,
        DESCENDING
    }
}
