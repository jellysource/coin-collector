package com.jellysource.coin.strategy.domain;

import javaslang.collection.List;
import lombok.Value;

import java.time.temporal.TemporalUnit;

@Value
public class ExecParams {
  String strategyId;
  List<String> tickers;
  Integer unitsBack;
  TemporalUnit frequency;
  Integer maxSlots;
  Integer minimalSlotValue;
  Integer maxNewSymbolsPerDay;
}
