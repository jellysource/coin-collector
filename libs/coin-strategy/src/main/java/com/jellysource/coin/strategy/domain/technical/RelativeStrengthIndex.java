package com.jellysource.coin.strategy.domain.technical;

import com.jellysource.coin.strategy.domain.BarData;
import javaslang.collection.List;
import org.ta4j.core.BaseTimeSeries;
import org.ta4j.core.TimeSeries;
import org.ta4j.core.indicators.RSIIndicator;
import org.ta4j.core.indicators.helpers.ClosePriceIndicator;


public class RelativeStrengthIndex extends NamedAnalysis {

    private Integer period;

    public RelativeStrengthIndex(Integer period, String name) {
        super(name);
        this.period = period;
    }

    @Override
    public List<BarData> apply(List<BarData> bars) {
        TimeSeries ts = new BaseTimeSeries(toBars(bars));
        ClosePriceIndicator cpe = new ClosePriceIndicator(ts);
        RSIIndicator rsi = new RSIIndicator(cpe, period);

        return toBarData(bars, rsi);
    }
}
