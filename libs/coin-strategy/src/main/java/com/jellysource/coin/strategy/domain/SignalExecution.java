package com.jellysource.coin.strategy.domain;


public enum SignalExecution {
    MARKET,
    LIMIT,
    STOP
}
