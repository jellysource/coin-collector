package com.jellysource.coin.strategy.domain;

import javaslang.collection.List;

import java.util.function.Function;


public interface Analysis extends Function<List<BarData>, List<BarData>>{

}
