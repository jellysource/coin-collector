package com.jellysource.coin.strategy.domain;

import javaslang.collection.List;
import javaslang.collection.Seq;
import javaslang.control.Option;
import lombok.Value;

import java.time.LocalDateTime;


@Value
public class StrategyContext {

    private LocalDateTime now;
    private List<Position> positions;

    public Option<Position> getPosition(String symbol) {
        return positions.find(position -> position.getSymbol().equals(symbol));
    }

    public Integer getTradeCount() {
        return positions.map(Position::size).sum().intValue();
    }

    public Integer getTotalShares(String symbol) {
        return getPosition(symbol)
            .map(Position::totalShares)
            .getOrElse(0);
    }

    public Seq<Position> filterPositionsBy(DataFeed feed) {
        return feed
            .filter(symbolData ->
                getPosition(symbolData.getTicker()).isDefined())
            .map(symbolData ->
                getPosition(symbolData.getTicker()).get());
    }
}