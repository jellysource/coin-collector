package com.jellysource.coin.strategy.domain;

import lombok.Value;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Value
public class AllSharesTrade {

  LocalDateTime dateTime;
  BigDecimal price;
}
