package com.jellysource.coin.strategy.domain.strategy;

import com.jellysource.coin.strategy.domain.StrategyContext;
import com.jellysource.coin.strategy.domain.TickerData;

public interface QuantityRule {

    Integer quantityBase(
        TickerData tickerData,
        StrategyContext strategyContext);
}
