package com.jellysource.coin.strategy.domain;

import javaslang.Lazy;
import javaslang.collection.HashMap;
import javaslang.collection.Map;
import javaslang.control.Option;
import lombok.AllArgsConstructor;
import lombok.Value;
import lombok.experimental.Wither;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.function.Supplier;


@Value
@Wither
@AllArgsConstructor
public class BarData {

    LocalDateTime dateTime;
    BigDecimal open;
    BigDecimal high;
    BigDecimal low;
    BigDecimal close;
    Integer volume;
    BigDecimal adjClose;

    Map<String, Lazy<BigDecimal>> analysisMap;

    public BarData(LocalDateTime dateTime, BigDecimal open, BigDecimal high, BigDecimal low, BigDecimal close, Integer volume, BigDecimal adjClose) {
        this.dateTime = dateTime;
        this.open = open;
        this.high = high;
        this.low = low;
        this.close = close;
        this.volume = volume;
        this.adjClose = adjClose;
        this.analysisMap = HashMap.empty();
    }

    public BarData withAnalysis(String key, Supplier<BigDecimal> analysis) {
        return withAnalysisMap(analysisMap.merge(HashMap.of(key, Lazy.of(analysis))));
    }

    public BarData withAnalysis(String key, BigDecimal analysis) {
        return withAnalysisMap(analysisMap.merge(HashMap.of(key, Lazy.of(() -> analysis))));
    }

    public Option<BigDecimal> getAnalysis(String analysis) {
        return analysisMap.get(analysis)
            .map(lazy -> Option.of(lazy.get()))
            .getOrElse(Option.none());
    }
}
