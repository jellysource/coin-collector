package com.jellysource.coin.strategy.domain.strategy;

import com.jellysource.coin.strategy.domain.BarData;
import com.jellysource.coin.strategy.domain.DataFeed;
import com.jellysource.coin.strategy.domain.ExecParams;
import com.jellysource.coin.strategy.domain.Position;
import com.jellysource.coin.strategy.domain.Signal;
import com.jellysource.coin.strategy.domain.SignalExecution;
import com.jellysource.coin.strategy.domain.Strategy;
import com.jellysource.coin.strategy.domain.StrategyContext;
import com.jellysource.coin.strategy.domain.TickerData;
import javaslang.Tuple;
import javaslang.collection.List;
import javaslang.control.Option;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.val;

import java.time.LocalDateTime;
import java.util.function.Predicate;

import static com.jellysource.coin.strategy.domain.TickerData.Ordering.ASCENDING;

@Getter
@RequiredArgsConstructor()
public abstract class StockPickingStrategy implements Strategy {

    private ExecParams execParams;
    private QuantityRule quantityRule;

    public StockPickingStrategy(ExecParams execParams) {
        this.execParams = execParams;
        quantityRule = new FirstTradeSharesRule();
    }

    public abstract boolean buyNew(TickerData tickerData);

    public abstract Integer buyNewScore(TickerData tickerData);

    public abstract boolean buyMore(TickerData tickerData, Position position);

    public Integer buyMoreScore(TickerData tickerData) {
        return buyNewScore(tickerData);
    }

    public abstract boolean sell(TickerData tickerData);

    public Integer getNextTradeSlotSize(Position position) {
        return position.getTrades().size() > 0 ? 1 : 0;
    }

    public Integer getCurrentSlots(Position position) {
        return position.getTrades().size();
    }

    @Override
    public List<Signal> process(StrategyContext strategyContext, DataFeed plainFeed) {
        DataFeed allSymbolsFeed = prepareDataFeed(
            plainFeed.max(strategyContext.getNow())
        );

        DataFeed sellFeed = allSymbolsFeed.filter(sellSymbolsFilter(strategyContext));
        DataFeed buyNewFeed = allSymbolsFeed.filter(buyNewSymbolsFilter(strategyContext));
        DataFeed buyExistingFeed = allSymbolsFeed.filter(buyExistingSymbolsFilter(strategyContext));

        Integer slotsToBeReleased = toSlots(sellFeed, strategyContext);
        Integer activeSlots = toSlots(strategyContext);
        Integer slotsRemaining = getMaxSlots() - activeSlots + slotsToBeReleased;

        List<Signal> sellSignals = sellFeed.toSellSignals(strategyContext).toList();
        List<Signal> buySignals = buyNewFeed
            .sortBy(this::buyNewScore, ASCENDING)
            .toBuySignals(getSlotValue())
            .take(Math.min(slotsRemaining, getMaxNewSymbolsPerDay()))
            .toList();

        List<Signal> buyUpSignals = collectBuyUpSignals(
            buyExistingFeed.sortBy(this::buyMoreScore, ASCENDING),
            strategyContext,
            slotsRemaining - buySignals.size());

        return sellSignals.appendAll(buySignals.appendAll(buyUpSignals));
    }

    protected DataFeed prepareDataFeed(DataFeed plainFeed) {
        return plainFeed;
    }

    private Predicate<TickerData> sellSymbolsFilter(StrategyContext strategyContext) {
        return symbolData -> {
            String ticker = symbolData.getTicker();
            return isFirstBarDateToday(symbolData.getBars(),strategyContext.getNow())
                && strategyContext.getPosition(ticker).isDefined()
                && sell(symbolData);
        };
    }

    private Predicate<TickerData> buyNewSymbolsFilter(StrategyContext strategyContext) {
        return symbolData -> {
            String ticker = symbolData.getTicker();
            return isFirstBarDateToday(symbolData.getBars(),strategyContext.getNow())
                && strategyContext.getPosition(ticker).isEmpty()
                && buyNew(symbolData);
        };
    }

    private Predicate<TickerData> buyExistingSymbolsFilter(StrategyContext strategyContext) {
        return symbolData -> {
            String ticker = symbolData.getTicker();
            Option<Position> position = strategyContext.getPosition(ticker);
            return isFirstBarDateToday(symbolData.getBars(),strategyContext.getNow())
                && position.isDefined()
                && buyMore(symbolData, position.get());
        };
    }

    private static Boolean isFirstBarDateToday(List<BarData> bars, LocalDateTime executionDate) {
      if(bars != null && bars.nonEmpty()) {
        BarData firstBar = bars.head();
        return firstBar.getDateTime().toLocalDate().isEqual(executionDate.toLocalDate());
      }
      return false;
    }

    private Integer toSlots(StrategyContext strategyContext) {
        return strategyContext
            .getPositions()
            .map(this::getCurrentSlots)
            .sum()
            .intValue();
    }

    private Integer toSlots(DataFeed feed, StrategyContext strategyContext) {
        return strategyContext
            .filterPositionsBy(feed)
            .map(this::getCurrentSlots)
            .sum()
            .intValue();
    }

    private List<Signal> collectBuyUpSignals(DataFeed feed, StrategyContext strategyContext, Integer remainingSlots) {
        return feed
            .map(symbolData -> Tuple.of(
                symbolData,
                getNextTradeSlotSize(strategyContext.getPosition(symbolData.getTicker()).get())))
            .foldLeft(
                Tuple.of(remainingSlots, List.<Signal>empty()),
                (acc, entry) -> {
                    val remaining = acc._1;
                    val signals = acc._2;
                    val symbolData = entry._1;
                    val slotsRequired = entry._2;
                    return remaining >= slotsRequired
                        ? Tuple.of(remaining - slotsRequired, signals.append(toSignal(symbolData, slotsRequired, strategyContext)))
                        : acc;
                })._2;
    }

    private Signal toSignal(TickerData tickerData, Integer multiplier, StrategyContext strategyContext) {
        return Signal
            .buy(tickerData.getTicker())
            .quantity(multiplier * quantityRule.quantityBase(tickerData, strategyContext))
            .at(SignalExecution.MARKET);
    }

    protected Integer getMaxSlots() {
        return execParams.getMaxSlots();
    }

    protected Integer getSlotValue() {
        return execParams.getMinimalSlotValue();
    }

    protected Integer getMaxNewSymbolsPerDay() {
        return execParams.getMaxNewSymbolsPerDay();
    }
}
