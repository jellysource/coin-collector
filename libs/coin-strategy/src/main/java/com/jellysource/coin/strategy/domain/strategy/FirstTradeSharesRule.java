package com.jellysource.coin.strategy.domain.strategy;

import com.jellysource.coin.strategy.domain.StrategyContext;
import com.jellysource.coin.strategy.domain.TickerData;

public class FirstTradeSharesRule implements QuantityRule {

    @Override
    public Integer quantityBase(TickerData tickerData, StrategyContext strategyContext) {
        return firstTradeShares(
            strategyContext,
            tickerData.getTicker());
    }

    private Integer firstTradeShares(StrategyContext strategyContext, String symbol) {
        return strategyContext
            .getPosition(symbol)
            .map(p -> p.getTrades().head().getShares())
            .getOrElse(0);
    }
}
