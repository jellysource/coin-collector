package com.jellysource.coin.strategy.domain;


import javaslang.collection.List;
import javaslang.collection.Seq;
import javaslang.control.Option;
import lombok.val;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.function.Function;
import java.util.function.Predicate;

import static java.math.BigDecimal.valueOf;


public class DataFeed {

    private List<TickerData> data;

    public DataFeed(List<TickerData> data) {
        this.data = data;
    }

    public DataFeed with(Analysis analysis) {
        return new DataFeed(
            data.map(symbolData -> symbolData.withAnalysis(analysis))
        );
    }

    public DataFeed max(LocalDateTime dateTime) {
        return new DataFeed(
            data.map(symbolData -> symbolData.withMaxDateBars(dateTime))
        );
    }

    public DataFeed sortAscBy(String analysis) {
        return new DataFeed(
            data.sortBy(symbolData -> symbolData
                .getBars().get()
                .getAnalysis(analysis)
                .getOrElse(BigDecimal.valueOf(Double.MAX_VALUE)))
        );
    }

    /**
     * Sorting function should provide Integer value for comparison of TickerData
     *
     * @param sortingFunction
     * @param ordering           - if no ordering specified, ASCENDING ordering is used
     * @return sorted DataFeed by sorting function
     */
    public DataFeed sortBy(Function<TickerData, Integer> sortingFunction, TickerData.Ordering ordering) {
        List<TickerData> lowestFirst = this.data.sortBy(sortingFunction);
        return new DataFeed(
            TickerData.Ordering.DESCENDING.equals(ordering)
                ? lowestFirst.reverse()
                : lowestFirst
        );
    }

    public <T> Seq<T> map(Function<TickerData, T> fn) {
        return data.map(fn);
    }

    public DataFeed filter(Predicate<TickerData> predicate) {
        return new DataFeed(data.filter(predicate));
    }

    public DataFeed filterOnlyWithAnalysis(String analysis) {
        return new DataFeed(data.filter(symbolData ->
            symbolData.getBars().get().getAnalysis(analysis).isDefined()
        ));
    }

    public List<String> symbols() {
        return data.map(TickerData::getTicker);
    }

    public Option<List<BarData>> get(String symbol) {
        return data
            .find(symbolData -> symbolData.getTicker().equals(symbol))
            .map(TickerData::getBars);
    }

    public Option<BigDecimal> get(String symbol, String analysis) {
        return get(symbol).flatMap(bars -> bars.get().getAnalysis(analysis));
    }

    public Option<BigDecimal> open(String symbol) {
        return get(symbol).map(bars -> bars.get().getOpen());
    }

    public Seq<Signal> toBuySignals(Integer positionSize) {
        return this.map(symbolData -> {
            val symbol = symbolData.getTicker();
            val bars = symbolData.getBars();
            return Signal
                .buy(symbol)
                .quantity(valueOf(positionSize).divideToIntegralValue(bars.get().getClose()).intValue())
                .at(SignalExecution.MARKET);
        });
    }

    public Seq<Signal> toSellSignals(StrategyContext strategyContext) {
        return this.map(symbolData -> {
            val symbol = symbolData.getTicker();
            return Signal
                .sell(symbol)
                .quantity(strategyContext.getTotalShares(symbol))
                //TODO: execution cant be enum, e.g. Limit.of(price), StopLimit.of(price, price)
                .at(SignalExecution.MARKET);
        });
    }

}
