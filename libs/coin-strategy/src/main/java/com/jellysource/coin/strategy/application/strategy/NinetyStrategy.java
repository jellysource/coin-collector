package com.jellysource.coin.strategy.application.strategy;

import com.jellysource.coin.strategy.domain.*;
import com.jellysource.coin.strategy.domain.strategy.StockPickingStrategy;
import com.jellysource.coin.strategy.domain.technical.RelativeStrengthIndex;
import com.jellysource.coin.strategy.domain.technical.SimpleMovingAverage;

import java.math.BigDecimal;

public class NinetyStrategy extends StockPickingStrategy {

    private final String SMA200 = "SMA200";
    private final String RSI2 = "RSI2";
    private final String SMA5 = "SMA5";

    public NinetyStrategy(ExecParams execParams) {
        super(execParams);
    }

    @Override
    protected DataFeed prepareDataFeed(DataFeed plainFeed) {
        return plainFeed
            .with(new SimpleMovingAverage(200, SMA200))
            .with(new SimpleMovingAverage(5, SMA5))
            .with(new RelativeStrengthIndex(2, RSI2))
            .filterOnlyWithAnalysis(SMA200);
    }

    @Override
    public boolean buyNew(TickerData tickerData) {
        BarData recentBar = tickerData.getBars().get();
        return
            recentBar.getAnalysis(SMA200)
                .map(sma200 -> recentBar.getClose().compareTo(sma200) == 1)
                .getOrElse(false)
            &&
            recentBar.getAnalysis(RSI2)
                .map(rsi2 -> BigDecimal.TEN.compareTo(rsi2) == 1)
                .getOrElse(false);
    }

    @Override
    public Integer buyNewScore(TickerData tickerData) {
        return tickerData
            .getBars().get()
            .getAnalysis(RSI2)
            .getOrElse(BigDecimal.valueOf(Double.MAX_VALUE))
            .intValue();
    }

    @Override
    public boolean buyMore(TickerData tickerData, Position position) {
        BarData recentBar = tickerData.getBars().get();
        return
            recentBar.getClose()
                .compareTo(position.getTrades().last().getPrice()) == -1
            &&
            recentBar.getAnalysis(SMA5)
                .map(sma5 -> recentBar.getClose().compareTo(sma5) == -1)
                .getOrElse(false);
    }

    @Override
    public boolean sell(TickerData tickerData) {
        BarData recentBar = tickerData.getBars().get();
        return recentBar.getAnalysis(SMA5)
            .map(sma5 -> recentBar.getClose().compareTo(sma5) == 1)
            .getOrElse(false);
    }

    @Override
    public Integer getNextTradeSlotSize(Position position) {
        switch (getCurrentSlots(position)) {
            case 1:
                return 2;
            case 3:
                return 3;
            case 6:
                return 4;
            default:
                return 0;
        }
    }

    @Override
    public Integer getCurrentSlots(Position position) {
        Integer trades = position.size();
        return trades * (trades + 1) / 2;
    }
}
