package com.jellysource.coin.strategy.application.strategy;

import com.jellysource.TestUtils;
import com.jellysource.coin.strategy.domain.*;
import javaslang.collection.List;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import static org.assertj.core.api.Assertions.assertThat;


public class NinetyStrategyTest {

    private DataFeed csvDataFeed;

    @Before
    public void setup() {
        csvDataFeed = TestUtils.csv("PFE", "GOOG");
    }

    @Test
    public void that_strategy_wont_fill_slots_when_no_signal() {
        ExecParams params = strategyParams(20, 5000, 1);
        NinetyStrategy nps = new NinetyStrategy(params);
        List<Signal> signals = nps.process(
            new StrategyContext(
                LocalDateTime.of(2017, 4, 21, 22, 0, 1),
                List.empty()),
            csvDataFeed);

        assertThat(buy(signals)).isEmpty();
    }

    @Test
    public void that_strategy_creates_order_when_signal_occurs_and_slots_free() {
        ExecParams params = strategyParams(20, 5000, 1);
        NinetyStrategy nps = new NinetyStrategy(params);
        List<Signal> signals = nps.process(
            new StrategyContext(
                LocalDateTime.of(2017, 4, 7, 22,0,1),
                List.empty()),
            csvDataFeed.max(LocalDateTime.of(2017, 4, 7, 22, 0)));

        assertThat(buy(signals))
            .containsOnly(
                Signal
                    .buy("GOOG")
                    .quantity(5000 / 824)
                    .at(SignalExecution.MARKET)
            );
    }

    @Test
    /*
     * Testing condition that in order for symbol to be acceptable for buying
     * it must have bar data (OHLC) for execution date. Otherwise symbol is
     * excluded from selection - same rule applies for selling, or buying more
     */
    public void that_strategy_wont_create_order_when_no_data_for_execution_date() {
        ExecParams params = strategyParams(20, 5000, 1);
        NinetyStrategy nps = new NinetyStrategy(params);
        List<Signal> signals = nps.process(
            new StrategyContext(
                LocalDateTime.of(2017, 4, 8, 22,0,1),
                List.empty()),
            csvDataFeed.max(LocalDateTime.of(2017, 4, 7, 22, 0)));

        assertThat(buy(signals)).isEmpty();
    }

    @Test
    public void that_strategy_wont_create_order_when_slots_full() {
        ExecParams params = strategyParams(2, 5000, 1);
        NinetyStrategy nps = new NinetyStrategy(params);
        List<Signal> signals = nps.process(
            new StrategyContext(
                LocalDateTime.of(2017, 4, 7, 22,0, 1),
                List.of(
                    new Position("ABC", List.of(new Trade(LocalDateTime.now(), 10, BigDecimal.TEN))),
                    new Position("DEF", List.of(new Trade(LocalDateTime.now(), 10, BigDecimal.ONE)))
                )),
            csvDataFeed.max(LocalDateTime.of(2017, 4, 7, 22, 0)));

        assertThat(buy(signals)).isEmpty();
    }

    @Test
    public void that_strategy_creates_multiplied_order_when_symbol_already_bought() {
        Integer slotValue = 5000;
        Integer maxSlots = 3;
        Integer last = 830;
        Integer lastShares = 10;

        ExecParams params = strategyParams(maxSlots, slotValue, 1);
        NinetyStrategy nps = new NinetyStrategy(params);
        List<Signal> signals = nps.process(
            new StrategyContext(
                LocalDateTime.of(2017, 4, 7, 22,0, 1),
                List.of(
                    new Position("GOOG", List.of(new Trade(LocalDateTime.now(), lastShares, BigDecimal.valueOf(last))))
                )),
            csvDataFeed.max(LocalDateTime.of(2017, 4, 7, 22, 0)));

        assertThat(buy(signals))
            .containsOnly(
                Signal
                    .buy("GOOG")
                    .quantity(lastShares * 2)
                    .at(SignalExecution.MARKET)
            );
    }

    @Test
    public void that_strategy_wont_create_multiplied_order_when_not_enough_slots() {
        Integer slotValue = 5000;
        Integer maxSlots = 9;
        Integer last = 830;
        Integer lastShares = 10;

        ExecParams params = strategyParams(maxSlots, slotValue, 1);
        NinetyStrategy nps = new NinetyStrategy(params);
        List<Signal> signals = nps.process(
            new StrategyContext(
                LocalDateTime.of(2017, 4, 7, 22,0, 1),
                List.of(
                    new Position("GOOG", List.of(
                        new Trade(LocalDateTime.now(), lastShares, BigDecimal.valueOf(last)),
                        new Trade(LocalDateTime.now(), lastShares * 2, BigDecimal.valueOf(last)),
                        new Trade(LocalDateTime.now(), lastShares * 3, BigDecimal.valueOf(last))
                    ))
                )
            ),
            csvDataFeed.max(LocalDateTime.of(2017, 4, 7, 22, 0)));

        assertThat(buy(signals)).isEmpty();
    }

    @Test
    /*
     * Both GOOG and PFE are on 11.4.2017 lower than SMA-5, and lower than RSI-10
     * Strategy should favour and scale lower RSI (PFE)
     * There are enough slots to scale only PFE
     */
    public void that_strategy_creates_multiplied_order_prioritizing_lower_rsi() {
        Integer slotValue = 5000;
        Integer maxSlots = 4;
        Integer lastGoog = 830;
        Integer lastPfe = 35;
        Integer lastSharesGoog = 10;
        Integer lastSharesPfe = 10;

        ExecParams params = strategyParams(maxSlots, slotValue, 1);
        NinetyStrategy nps = new NinetyStrategy(params);
        List<Signal> signals = nps.process(
            new StrategyContext(
                LocalDateTime.of(2017, 4, 11, 22,0, 1),
                List.of(
                    new Position("GOOG", List.of(new Trade(LocalDateTime.now(), lastSharesGoog, BigDecimal.valueOf(lastGoog)))),
                    new Position("PFE", List.of(new Trade(LocalDateTime.now(), lastSharesPfe, BigDecimal.valueOf(lastPfe))))
                )),
            csvDataFeed.max(LocalDateTime.of(2017, 4, 11, 22, 0)));

        assertThat(buy(signals))
            .containsOnly(
                Signal
                    .buy("PFE")
                    .quantity(lastSharesPfe * 2)
                    .at(SignalExecution.MARKET)
            );
    }

    @Test
    /*
     * Both GOOG and PFE are on 11.4.2017 lower than SMA-5, and lower than RSI-10
     * Strategy should scale all symbols based on trade count
     */
    public void that_strategy_creates_multiplied_order_for_all_if_enough_slots() {
        Integer slotValue = 5000;
        Integer maxSlots = 20;
        Integer lastGoog = 830;
        Integer lastPfe = 35;
        Integer lastSharesGoog = 10;
        Integer lastSharesPfe = 10;

        ExecParams params = strategyParams(maxSlots, slotValue, 1);
        NinetyStrategy nps = new NinetyStrategy(params);
        List<Signal> signals = nps.process(
            new StrategyContext(
                LocalDateTime.of(2017, 4, 11, 22,0, 1),
                List.of(
                    new Position("GOOG", List.of(
                        new Trade(LocalDateTime.now(), lastSharesGoog, BigDecimal.valueOf(lastGoog)),
                        new Trade(LocalDateTime.now(), lastSharesGoog * 2, BigDecimal.valueOf(lastGoog)))),
                    new Position("PFE", List.of(
                        new Trade(LocalDateTime.now(), lastSharesPfe, BigDecimal.valueOf(lastPfe)),
                        new Trade(LocalDateTime.now(), lastSharesPfe * 2, BigDecimal.valueOf(lastPfe)),
                        new Trade(LocalDateTime.now(), lastSharesPfe * 3, BigDecimal.valueOf(lastPfe))))
                )),
            csvDataFeed.max(LocalDateTime.of(2017, 4, 11, 22, 0)));

        assertThat(buy(signals))
            .containsOnly(
                Signal
                    .buy("PFE")
                    .quantity(lastSharesPfe * 4)
                    .at(SignalExecution.MARKET),
                Signal
                    .buy("GOOG")
                    .quantity(lastSharesPfe * 3)
                    .at(SignalExecution.MARKET)
            );
    }

    @Test
    /*
     * GOOG is on 17.4.2017 above SMA-5
     * Strategy should create sell order with all symbol's shares if it is above SMA-5
     */
    public void that_sell_order_is_present() {
        Integer slotValue = 5000;
        Integer maxSlots = 20;
        Integer last = 830;
        Integer lastShares = 10;

        ExecParams params = strategyParams(maxSlots, slotValue, 1);
        NinetyStrategy nps = new NinetyStrategy(params);
        List<Signal> signals = nps.process(
            new StrategyContext(
                LocalDateTime.of(2017, 4, 17, 22,0, 1),
                List.of(
                    new Position("GOOG", List.of(
                        new Trade(LocalDateTime.now(), lastShares, BigDecimal.valueOf(last)),
                        new Trade(LocalDateTime.now(), lastShares * 2, BigDecimal.valueOf(last)),
                        new Trade(LocalDateTime.now(), lastShares * 3, BigDecimal.valueOf(last))
                    ))
                )
            ),
            csvDataFeed.max(LocalDateTime.of(2017, 4, 17, 22, 0)));

        assertThat(buy(signals)).isEmpty();
        assertThat(sell(signals))
            .containsOnly(
                Signal
                    .sell("GOOG")
                    .quantity(lastShares * 6)
                    .at(SignalExecution.MARKET)
            );
    }

    @Test
    /*
     * GOOG is on 8.3.2017 above SMA-5, therefore ready to sell
     * PFE has that day RSI-2 < 10, price > SMA-200 and is ready to buy
     * There is no free slot. GOOG occupies all 3 slots by its 2 trades
     * Strategy should release slots that GOOG occupies and make buy order for PFE
     */
    public void that_sell_order_should_release_slot_for_buy_order() {
        Integer slotValue = 5000;
        Integer maxSlots = 3;
        Integer last = 830;
        Integer lastShares = 10;

        ExecParams params = strategyParams(maxSlots, slotValue, 1);
        NinetyStrategy nps = new NinetyStrategy(params);
        List<Signal> signals = nps.process(
            new StrategyContext(
                LocalDateTime.of(2017, 3, 8, 22,0, 1),
                List.of(
                    new Position("GOOG", List.of(
                        new Trade(LocalDateTime.now(), lastShares, BigDecimal.valueOf(last)),
                        new Trade(LocalDateTime.now(), lastShares * 2, BigDecimal.valueOf(last))
                    ))
                )
            ),
            csvDataFeed.max(LocalDateTime.of(2017, 3, 8, 22, 0)));

        assertThat(sell(signals))
            .containsOnly(
                Signal
                    .sell("GOOG")
                    .quantity(lastShares * 3)
                    .at(SignalExecution.MARKET)
            );
        assertThat(buy(signals))
            .containsOnly(
                Signal
                    .buy("PFE")
                    .quantity(BigDecimal.valueOf(slotValue / 33.91).intValue())
                    .at(SignalExecution.MARKET)
            );
    }

    private List<Signal> buy(List<Signal> signals) {
        return signals.filter(Signal::isBuy);
    }

    private List<Signal> sell(List<Signal> signals) {
        return signals.filter(Signal::isSell);
    }

    private ExecParams strategyParams(
        Integer maxSlots,
        Integer slotValue,
        Integer maxNewSymbolsPerDay) {

        return new ExecParams(
            "STRATEGY",
            List.empty(),
            300,
            ChronoUnit.DAYS,
            maxSlots,
            slotValue,
            maxNewSymbolsPerDay
        );
    }
}