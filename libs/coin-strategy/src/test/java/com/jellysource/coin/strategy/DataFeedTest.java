package com.jellysource.coin.strategy;

import com.jellysource.BarDataUtils;
import com.jellysource.TestUtils;
import com.jellysource.coin.strategy.domain.BarData;
import com.jellysource.coin.strategy.domain.DataFeed;
import com.jellysource.coin.strategy.domain.TickerData;
import com.jellysource.coin.strategy.domain.technical.RelativeStrengthIndex;
import com.jellysource.coin.strategy.domain.technical.SimpleMovingAverage;
import javaslang.collection.List;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.Month;

import static org.assertj.core.api.Assertions.assertThat;


public class DataFeedTest {

    private DataFeed dataFeed;
    private DataFeed csvDataFeed;

    private final String RSI2 = "RSI2";
    private final String SMA5 = "SMA5";
    private final String SMA200 = "SMA200";


    @Before
    public void setup() {
        dataFeed = new DataFeed(
            List.of(
                new TickerData("WDI",
                    List.of(
                        bar(date(10), 10f, 12f, 8f, 1234, 11f)
                            .withAnalysis(SMA200, BigDecimal.ONE)
                            .withAnalysis(RSI2, BigDecimal.TEN),
                        bar(date(9), 1f, 1f, 1f, 12345, 1f)
                            .withAnalysis(RSI2, BigDecimal.TEN),
                        bar(date(8), 133f, 134f, 121f, 56789, 130f)
                    )
                ),
                new TickerData("GOOG",
                    List.of(
                        bar(date(10), 10f, 12f, 8f, 1234, 11f )
                            .withAnalysis(RSI2, BigDecimal.ONE),
                        bar(date(9), 1f, 1f, 1f, 12345, 1f )
                            .withAnalysis(RSI2, BigDecimal.TEN),
                        bar(date(8), 133f, 134f, 121f, 56789, 130f)
                    )
                )
            )
        );
        csvDataFeed = TestUtils.csv("WDI", "GOOG");
    }

    @Test
    public void thatDataFeedIsFiltered() {
        DataFeed filtered = dataFeed.filter(symbolData -> symbolData.getBars().get().getAnalysis(SMA200).isDefined());
        assertThat(filtered.symbols()).hasSize(1);
    }

    @Test
    public void thatDataFeedIsSorted() {
        DataFeed sorted = dataFeed.sortAscBy(RSI2);
        assertThat(sorted.symbols()).first().isEqualTo("GOOG");
    }

    @Test
    public void thatCsvDataFeedHasAnalysis() {
        DataFeed withAnalysis = csvDataFeed
            .with(new SimpleMovingAverage(200, SMA200))
            .with(new SimpleMovingAverage(5, SMA5))
            .with(new RelativeStrengthIndex(2, RSI2));

        assertThat(withAnalysis.symbols()).hasSize(2);
        assertThat(withAnalysis.get("GOOG").isDefined());

        assertThat(
            withAnalysis
                .get("GOOG", SMA200).get()
                .setScale(2, RoundingMode.HALF_UP))
            .isEqualTo("790.54");

        assertThat(
            withAnalysis
                .get("GOOG", SMA5).get()
                .setScale(2, RoundingMode.HALF_UP))
            .isEqualTo("839.41");

        assertThat(
            withAnalysis
                .get("GOOG", RSI2).get()
                .setScale(2, RoundingMode.HALF_UP))
            .isEqualTo("98.01");

        //SMA200 790.54
        //SMA5   839.41
        //RSI2   98.01
    }

    private LocalDateTime date(int dayOfMonth) {
        return LocalDateTime.of(2000, Month.JANUARY, dayOfMonth, 1, 1);
    }

    public static BarData bar(LocalDateTime date, Float open, Float high, Float low, Integer vol, Float close) {
        return BarDataUtils.bar(date, open, high, low, close, vol, close);
    }
}
