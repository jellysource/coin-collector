package com.jellysource;

import com.jellysource.coin.strategy.domain.BarData;
import javaslang.collection.HashMap;
import javaslang.collection.List;

import java.io.InputStream;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Scanner;

import static java.time.temporal.ChronoField.DAY_OF_MONTH;
import static java.time.temporal.ChronoField.MONTH_OF_YEAR;
import static java.time.temporal.ChronoField.YEAR;


public class BarDataUtils {

    private final static String NEW_LINE_RGX = "\n";
    private final static String START_INPUT_RGX = "\\A";
    private final static String COL_SPLITTER = ",";

    public static List<BarData> loadCsvData(InputStream csv) {
        return
            List.of(
                new Scanner(csv)
                    .useDelimiter(START_INPUT_RGX)
                    .next()
                    .split(NEW_LINE_RGX))
                .drop(1)
                .map(line -> fromCsv(line, COL_SPLITTER));
    }

    private static BarData fromCsv(String csvBar, String splitter) {
        String[] row = csvBar.split(splitter);
        return bar(
            LocalDate.parse(row[0], CSV_DATE).atTime(22, 0),
            Float.parseFloat(row[1]),
            Float.parseFloat(row[2]),
            Float.parseFloat(row[3]),
            Float.parseFloat(row[4]),
            Integer.parseInt(row[5]),
            Float.parseFloat(row[6])
            );
    }

    public static BarData bar(LocalDateTime date, Float open, Float high, Float low, Float close, Integer vol, Float adjClose) {
        return new BarData(
            date,
            BigDecimal.valueOf(open),
            BigDecimal.valueOf(high),
            BigDecimal.valueOf(low),
            BigDecimal.valueOf(close),
            vol,
            BigDecimal.valueOf(adjClose),
            HashMap.empty());
    }

    static DateTimeFormatter CSV_DATE =
        new DateTimeFormatterBuilder()
            .parseCaseInsensitive()
            .appendValue(YEAR, 4)
            .appendLiteral("-")
            .appendValue(MONTH_OF_YEAR, 2)
            .appendLiteral("-")
            .appendValue(DAY_OF_MONTH, 2)
            .toFormatter();
}
