package com.jellysource;

import com.jellysource.coin.strategy.domain.DataFeed;
import com.jellysource.coin.strategy.domain.TickerData;
import javaslang.collection.List;


public class TestUtils {

    public static DataFeed csv(String... symbols) {
        ClassLoader loader = TestUtils.class.getClassLoader();
        return new DataFeed(
            List.of(symbols).map(symbol ->
                new TickerData(
                    symbol,
                    BarDataUtils.loadCsvData(loader.getResourceAsStream(symbol.concat(".csv")))
                )
            )
        );
    }
}
