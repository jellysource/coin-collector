package com.jellysource.broker.api.protocol.json.format

import com.jellysource.broker.api.protocol._
import spray.json._

trait OrderFormat extends DefaultJsonProtocol {

  implicit val actionFormat: JsonFormat[Action] = ActionFormat

  implicit val executionFormat: JsonFormat[Execution] = ExecutionFormat

  implicit val orderFormat: RootJsonFormat[Order] = jsonFormat5(Order.apply)

}
