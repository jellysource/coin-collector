package com.jellysource.broker.api.protocol.json

import java.nio.charset.StandardCharsets

import com.jellysource.broker.api.protocol.{Order, OrderEvent}
import com.jellysource.broker.api.protocol.json.format.OrderEventFormats._
import spray.json._

object JsonSerDe extends {

  def serializeOrder(order: Order): Array[Byte] = toBytes(order.toJson)

  def serializeOrderEvent(event: OrderEvent): Array[Byte] = toBytes(event.toJson)

  def deserializeOrder(json: Array[Byte]): Order = fromBytes(json).convertTo[Order]

  def deserializeOrderEvent(json: Array[Byte]): OrderEvent = fromBytes(json).convertTo[OrderEvent]

  private def toBytes(json: JsValue): Array[Byte] = json.prettyPrint.getBytes()

  private def fromBytes(bytes: Array[Byte]): JsValue = new String(bytes, StandardCharsets.UTF_8).parseJson

}
