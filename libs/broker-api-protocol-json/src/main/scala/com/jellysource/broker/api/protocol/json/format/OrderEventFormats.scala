package com.jellysource.broker.api.protocol.json.format

import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter

import com.jellysource.broker.api.protocol._
import spray.json._

trait OrderEventFormats extends OrderFormat {

  implicit object OffsetDateTimeFormat extends RootJsonFormat[OffsetDateTime] {
    private val formatter = DateTimeFormatter.ISO_DATE_TIME
    override def write(time: OffsetDateTime): JsValue = JsString(formatter.format(time))
    override def read(json: JsValue): OffsetDateTime = json match {
      case JsString(time) => OffsetDateTime.parse(time, formatter)
      case other          => deserializationError(s"Ivalid time json: $json")
    }
  }

  private val orderCreatedFormat: RootJsonFormat[OrderCreated] = jsonFormat6(OrderCreated)
  private val orderPlacedFormat: RootJsonFormat[OrderPlaced] = jsonFormat2(OrderPlaced)
  private val orderFilledPartiallyFormat: RootJsonFormat[OrderFilledPartially] = jsonFormat4(OrderFilledPartially)
  private val orderFilledFormat: RootJsonFormat[OrderFilled] = jsonFormat3(OrderFilled)
  private val orderCancelledFormat: RootJsonFormat[OrderCancelled] = jsonFormat2(OrderCancelled)

  private val TypeNameToFormat = Map(
    "CREATED" -> orderCreatedFormat,
    "PLACED" -> orderPlacedFormat,
    "FILLED_PARTIALLY" -> orderFilledPartiallyFormat,
    "FILLED" -> orderFilledFormat,
    "CANCELLED" -> orderCancelledFormat
  )

  implicit def orderEventFormat[T <: OrderEvent]: RootJsonFormat[T] = new RootJsonFormat[T] {
    override def read(json: JsValue): T = json match {
      case JsObject(fields) => read(json, fields)
      case _                => deserializationError(s"Invalid order event json: $json")
    }

    private def read(json: JsValue, fields: Map[String, JsValue]) =
      fields.get("type") match {
        case Some(JsString(typeName)) if TypeNameToFormat.contains(typeName.toUpperCase) =>
          json.convertTo(TypeNameToFormat(typeName)).asInstanceOf[T]
        case unknown =>
          deserializationError(s"Unknown order event type: $unknown")
      }

    override def write(event: T): JsValue = {
      val format = TypeNameToFormat(event.typeName)
      val fields = event.toJson(format.asInstanceOf[RootJsonFormat[T]]).asJsObject.fields
      JsObject(fields + ("type" -> JsString(event.typeName.toUpperCase)))
    }
  }

}

private[json] object OrderEventFormats extends OrderEventFormats
