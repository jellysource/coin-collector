package com.jellysource.broker.api.protocol.json.format

import com.jellysource.broker.api.protocol.{Execution, Limit, Market, Stop}
import spray.json._

object ExecutionFormat extends JsonFormat[Execution] {
  override def read(json: JsValue): Execution = json match {
    case JsObject(fields) => read(fields)
    case other            => deserializationError(s"Unsuported execution: $other")
  }

  override def write(execution: Execution): JsValue = execution match {
    case Market       => JsObject(Map("type" -> JsString("MARKET")))
    case Limit(price) => JsObject(Map("type" -> JsString("LIMIT"), "price" -> JsNumber(price)))
    case Stop(price)  => JsObject(Map("type" -> JsString("STOP"), "price" -> JsNumber(price)))
  }

  private def read(fields: Map[String, JsValue]): Execution =
    (fields.get("type"), fields.get("price")) match {
      case (Some(JsString("MARKET")), _)                            => Market
      case (Some(JsString("LIMIT")), Some(JsNumber(price)))         => Limit(price)
      case (Some(JsString("STOP")), Some(JsNumber(price)))          => Stop(price)
      case (Some(JsString("LIMIT")) | Some(JsString("STOP")), None) => fieldPriceMissing
      case _                                                        => unsupportedExecutionFields(fields)
    }

  private def unsupportedExecutionFields(fields: Map[String, JsValue]) =
    deserializationError(
    "Unsupported execution fields. " +
      "Required 'type' with value 'MARKET', 'LIMIT' or 'STOP' and 'price' for 'LIMIT' and 'STOP'. " +
      s"But found: $fields"
    )

  private def fieldPriceMissing = deserializationError("Field price is missing")
}
