package com.jellysource.broker.api.protocol.json.format

import com.jellysource.broker.api.protocol.{Action, Buy, Sell}
import spray.json._

object ActionFormat extends JsonFormat[Action] {
  override def read(json: JsValue): Action = json match {
    case JsString("BUY")  => Buy
    case JsString("SELL") => Sell
    case unknown => deserializationError(s"Not valid action type: $unknown. Expected BUY or SELL")
  }

  override def write(action: Action): JsValue =
    JsString(action match {
      case Buy  => "BUY"
      case Sell => "SELL"
    })
}
