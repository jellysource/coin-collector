package com.jellysource.broker.api.protocol.json

import java.nio.charset.StandardCharsets

import org.scalatest.{FlatSpec, Matchers}
import spray.json._

class JsonSerDeSpec extends FlatSpec with Matchers with OrderEventsFixture with OrderFixture {

  "Json serde" should "provide serialization for order events to json bytes" in {
    JsonSerDe.serializeOrderEvent(orderCancelledEvent).toJson shouldBe orderCancelledJson
  }

  it should "provide deserialization of order events in json as bytes" in {
    JsonSerDe.deserializeOrderEvent(orderCancelledJson.prettyPrint.getBytes) shouldBe orderCancelledEvent
  }

  it should "provide serialization for orders to json bytes" in {
    JsonSerDe.serializeOrder(buyMarketOrder).toJson shouldBe buyMarketJson
  }

  it should "provide deserialization of orders in json as bytes" in {
    JsonSerDe.deserializeOrder(buyMarketJson.prettyPrint.getBytes) shouldBe buyMarketOrder
  }

  implicit class BytesToJson(bytes: Array[Byte]) {
    def toJson: JsValue = new String(bytes, StandardCharsets.UTF_8).parseJson
  }
}
