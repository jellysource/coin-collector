package com.jellysource.broker.api.protocol.json.format

import com.jellysource.broker.api.protocol._
import com.jellysource.broker.api.protocol.json.OrderEventsFixture
import org.scalatest.{FlatSpec, Matchers}
import spray.json._

class OrderEventFormatsSpec extends FlatSpec with Matchers with OrderEventFormats with OrderEventsFixture {

  "Order event formats" can "be used to convert order created event to json" in {
    orderCreatedEvent.toJson shouldBe orderCreatedJson
  }

  it can "be used to convert json to order created event" in {
    orderCreatedJson.convertTo[OrderCreated] shouldBe orderCreatedEvent
  }

  it can "be used to convert order placed event to json" in {
    orderPlacedEvent.toJson shouldBe orderPlacedJson
  }

  it can "be used to convert json to order placed event" in {
    orderPlacedJson.convertTo[OrderPlaced] shouldBe orderPlacedEvent
  }

  it can "be used to convert order filled partially event to json" in {
    orderFilledPartiallyEvent.toJson shouldBe orderFilledPartiallyJson
  }

  it can "be used to convert json to order filled partially event" in {
    orderFilledPartiallyJson.convertTo[OrderFilledPartially] shouldBe orderFilledPartiallyEvent
  }

  it can "be used to convert order filled event to json" in {
    orderFilledEvent.toJson shouldBe orderFilledJson
  }

  it can "be used to convert json to order filled event" in {
    orderFilledJson.convertTo[OrderFilled] shouldBe orderFilledEvent
  }

  it can "be used to convert order cancelled event to json" in {
    orderCancelledEvent.toJson shouldBe orderCancelledJson
  }

  it can "be used to convert json to order cancelled event" in {
    orderCancelledJson.convertTo[OrderCancelled] shouldBe orderCancelledEvent
  }

}
