package com.jellysource.broker.api.protocol.json.format

import com.jellysource.broker.api.protocol._
import com.jellysource.broker.api.protocol.json.OrderFixture
import org.scalatest.{FlatSpec, Matchers}
import spray.json._

class OrderFormatSpec extends FlatSpec with Matchers with OrderEventFormats with OrderFixture {

  "Order format" can "be used to convert buy market order to json" in {
    buyMarketOrder.toJson shouldBe buyMarketJson
  }

  it can "be used to convert json to buy market order" in {
    buyMarketJson.convertTo[Order] shouldBe buyMarketOrder
  }

  it can "be used to convert sell limit order to json" in {
    sellLimitOrder.toJson shouldBe sellLimitJson
  }

  it can "be used to convert json to sell limit order" in {
    sellLimitJson.convertTo[Order] shouldBe sellLimitOrder
  }

  it can "be used to convert sell stop order to json" in {
    sellStopOrder.toJson shouldBe sellStopJson
  }

  it can "be used to convert json to sell stop order" in {
    sellStopJson.convertTo[Order] shouldBe sellStopOrder
  }

}
