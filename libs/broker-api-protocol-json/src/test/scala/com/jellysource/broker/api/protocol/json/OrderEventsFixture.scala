package com.jellysource.broker.api.protocol.json

import java.time.{LocalDateTime, ZoneOffset}

import com.jellysource.broker.api.protocol._
import spray.json._

trait OrderEventsFixture {

  val orderCreatedEvent = OrderCreated(
    id = "1",
    time = time,
    symbol = "T",
    quantity = 10,
    action = Buy,
    execution = Market
  )

  val orderCreatedJson: JsValue =
    """{
      |  "type": "CREATED",
      |  "time": "2018-07-21T22:08:00Z",
      |  "id": "1",
      |  "symbol": "T",
      |  "quantity": 10,
      |  "action": "BUY",
      |  "execution": {
      |    "type": "MARKET"
      |  }
      |}""".stripMargin.parseJson


  val orderPlacedEvent = OrderPlaced(id = "1", time = time)

  val orderPlacedJson: JsValue =
    """{
      |  "type": "PLACED",
      |  "time": "2018-07-21T22:08:00Z",
      |  "id": "1"
      |}""".stripMargin.parseJson


  val orderFilledPartiallyEvent = OrderFilledPartially(id = "1", time = time, quantity = 5, price = 32.05)

  val orderFilledPartiallyJson: JsValue =
    """{
      |  "type": "FILLED_PARTIALLY",
      |  "time": "2018-07-21T22:08:00Z",
      |  "id": "1",
      |  "price": 32.05,
      |  "quantity": 5
      |}
      |""".stripMargin.parseJson


  val orderFilledEvent = OrderFilled(id = "1", time = time, price = 32.05)

  val orderFilledJson: JsValue =
    """{
      |  "type": "FILLED",
      |  "time": "2018-07-21T22:08:00Z",
      |  "id": "1",
      |  "price": 32.05
      |}""".stripMargin.parseJson


  val orderCancelledEvent = OrderCancelled(id = "1", time = time)

  val orderCancelledJson: JsValue =
    """{
      |  "type": "CANCELLED",
      |  "time": "2018-07-21T22:08:00Z",
      |  "id": "1"
      |}""".stripMargin.parseJson

  private lazy val time =
    LocalDateTime.of(
      2018,
      7,
      21,
      22,
      8
    ).atOffset(ZoneOffset.UTC)

}
