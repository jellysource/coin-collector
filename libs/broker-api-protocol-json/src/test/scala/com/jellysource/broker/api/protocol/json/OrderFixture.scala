package com.jellysource.broker.api.protocol.json

import com.jellysource.broker.api.protocol._
import spray.json._

trait OrderFixture {

  val buyMarketOrder = Order(
    id = "1",
    symbol = "T",
    quantity = 10,
    action = Buy,
    execution = Market
  )

  val buyMarketJson: JsValue =
    """{
      |  "id": "1",
      |  "symbol": "T",
      |  "quantity": 10,
      |  "action": "BUY",
      |  "execution": {
      |    "type": "MARKET"
      |  }
      |}""".stripMargin.parseJson


  val sellLimitOrder: Order = buyMarketOrder.copy(action = Sell, execution = Limit(35.05))

  val sellLimitJson: JsValue =
    """{
      |  "id": "1",
      |  "symbol": "T",
      |  "quantity": 10,
      |  "action": "SELL",
      |  "execution": {
      |    "type": "LIMIT",
      |    "price": 35.05
      |  }
      |}""".stripMargin.parseJson


  val sellStopOrder: Order = sellLimitOrder.copy(execution = Stop(30.00))

  val sellStopJson: JsValue =
    """{
      |  "id": "1",
      |  "symbol": "T",
      |  "quantity": 10,
      |  "action": "SELL",
      |  "execution": {
      |    "type": "STOP",
      |    "price": 30.00
      |  }
      |}""".stripMargin.parseJson
}
