package com.jellysource.gradle.scala

import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.provider.Property
import org.gradle.util.ConfigureUtil

class ScalaExtension {
    final Property<ScalaVersion> version

    ScalaExtension(Project project) {
        version = project.objects.property(ScalaVersion)
        version.set(new ScalaVersion(
            major: "2.12", 
            minor: "8", 
            logging: "3.9.2", 
            test: "3.0.7", 
            check: "1.14.0", 
            pagedown: "1.6.0"
        ))    
    }

    ScalaVersion getVersion() {
        version.get()
    }

    void version(@DelegatesTo(ScalaVersion) Closure c) {
        version(ConfigureUtil.configureUsing(c))
    }

    void version(Action<? super ScalaVersion> action) {
        action.execute(version.get())
    }

    String dependency(String lib) {
        ScalaDependencies.addScalaMajorVersion(lib)
    }

}