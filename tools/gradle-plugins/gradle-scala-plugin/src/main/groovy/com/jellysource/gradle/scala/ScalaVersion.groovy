package com.jellysource.gradle.scala

class ScalaVersion {
    String major
    String minor
    String logging
    String test
    String check
    String pagedown
}