package com.jellysource.gradle.scala

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.artifacts.DependencyResolveDetails

import static com.jellysource.gradle.scala.ScalaDependencies.*

class ScalaPlugin implements Plugin<Project> {

    @Override
    void apply(Project project) {
        project.plugins.apply 'scala'
        project.plugins.apply 'com.github.maiflai.scalatest'
        
        registerScalaExtension(project) 
        registerScalaDependenciesResolution(project)
    
        project.repositories {         
            mavenCentral()      
        }

        project.dependencies {
            implementation "org.scala-lang:scala-library:$SCALA_MAJOR_VERSION.$SCALA_MINOR_VERSION"
            implementation project.scala.dependency("com.typesafe.scala-logging:scala-logging:$SCALA_LOGGING_VERSION")

            testImplementation project.scala.dependency("org.scalatest:scalatest:$SCALA_TEST_VERSION")
            testImplementation project.scala.dependency("org.scalacheck:scalacheck:$SCALA_CHECK_VERSION")
            testRuntime "org.pegdown:pegdown:$PAGEDOWN_VERSION"
        }
    }

    private ScalaExtension registerScalaExtension(Project project) {
        project.extensions.create("scala", ScalaExtension, project)
    }

    private void registerScalaDependenciesResolution(Project project) {
        project.configurations.all {
            resolutionStrategy.eachDependency { DependencyResolveDetails details ->
                if (isPlaceholderSubstitutionNeeded(details.requested.name, details.requested.version)) {               
                    final scalaVersion = project.scala.version
                    final newName = substitutePlaceholders(scalaVersion, details.requested.name)
                    final newVersion = substitutePlaceholders(scalaVersion, details.requested.version)                    
                    details.useTarget group: details.requested.group, name: newName, version: newVersion
                    details.because "scala plugin dependencies placeholder substitution"                            
                }
            }        
        }   
    }
}
