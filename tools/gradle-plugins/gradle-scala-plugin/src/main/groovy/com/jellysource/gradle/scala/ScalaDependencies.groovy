package com.jellysource.gradle.scala

class ScalaDependencies {

    static final SCALA_MAJOR_VERSION = '#SCALA_MAJOR_VERSION'
    static final SCALA_MINOR_VERSION = '#SCALA_MINOR_VERSION'
    static final SCALA_LOGGING_VERSION = '#SCALA_LOGGING_VERSION'
    static final SCALA_TEST_VERSION = '#SCALA_TEST_VERSION'
    static final SCALA_CHECK_VERSION = '#SCALA_CHECK_VERSION'
    static final PAGEDOWN_VERSION = '#PAGEDOWN_VERSION'

    // static final SCALA_MAJOR_VERSION = "2.12"
    // static final SCALA_MINOR_VERSION = "8"
    // static final SCALA_LOGGING_VERSION = "3.9.2"
    // static final SCALA_TEST_VERSION = "3.0.7"
    // static final SCALA_CHECK_VERSION = "1.14.0"
    // static final PAGEDOWN_VERSION = "1.6.0"
     

    private static final SUBSTITUTIONS = [
        "$SCALA_MAJOR_VERSION":     { ScalaVersion version -> version.major },        
        "$SCALA_MINOR_VERSION":     { ScalaVersion version -> version.minor },
        "$SCALA_LOGGING_VERSION":   { ScalaVersion version -> version.logging },
        "$SCALA_TEST_VERSION":      { ScalaVersion version -> version.test },
        "$SCALA_CHECK_VERSION":     { ScalaVersion version -> version.check },
        "$PAGEDOWN_VERSION":        { ScalaVersion version -> version.pagedown }
    ]


    static boolean isPlaceholderSubstitutionNeeded(String name, String version) {
        SUBSTITUTIONS.find { name.contains(it.key) || version.contains(it.key) } != null    
    }

    static String isPlaceholderSubstitutionNeeded(String label) {
        SUBSTITUTIONS.find { label.contains(it.key) } != null
    }

    static String substitutePlaceholders(ScalaVersion scalaVersion, String label) {
        SUBSTITUTIONS.findAll { label.contains(it.key) }.inject(label) { result, e -> 
            result.replace(e.key, e.value(scalaVersion)) 
        }
    }

    static String addScalaMajorVersion(String lib) {
        def enriched = lib.split(":")            
        enriched[1] = enriched[1] + '_' + SCALA_MAJOR_VERSION
        enriched.join(":")
    }
}