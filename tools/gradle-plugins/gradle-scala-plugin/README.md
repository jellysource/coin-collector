# Scala plugin

Add support for scala development and testing to project.

Plugin will add following dependencies to project

- implementation "org.scala-lang:scala-library"
- implementation "com.typesafe.scala-logging:scala-logging"
- testImplementation "org.scalatest:scalatest"
- testImplementation "org.scalacheck:scalacheck"
- testRuntime "org.pegdown:pegdown"

It will apply `scala` and `com.github.maiflai.scalatest` plugins.

It will add method `scala.dependency` which can be used to add scala major version part to artifact name like in following:

    dependencies {
        compile scala.dependency("org.scalaj:scalaj-http:2.4.0")
    }
    

## How to build

    gradlew clean build

## How to use

Include build in your **settings.xml**.

    includeBuild '${REPOSITORY_ROOT}/tools/gradle-plugins/gradle-scala-plugin'

Apply plugin in your **build.gradle**.

    plugins {
        id 'com.jellysource.gradle.scala'
    }

Or add build script dependency in your **build.gradle** and apply

    buildscript {
        dependencies {
            classpath "com.jellysource.gradle:gradle-scala-plugin"        
        }
    }

    apply plugin: 'com.jellysource.gradle.scala'

## How to override default versions

It is possible to override versions of all dependencies added by scala plugin

    scala {
        version {
            major = "2.12"
            minor = "8"        
            logging = "3.9.2"
            test = "3.0.7"
            check = "1.14.0"
            pagedown = "1.6.0"
        }
    }
