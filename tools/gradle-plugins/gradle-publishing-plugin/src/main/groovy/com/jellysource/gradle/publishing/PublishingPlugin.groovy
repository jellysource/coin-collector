package com.jellysource.gradle.publishing

import com.jfrog.bintray.gradle.tasks.RecordingCopyTask
import org.gradle.api.Plugin
import org.gradle.api.Project

class PublishingPlugin implements Plugin<Project> {

    @Override
    void apply(Project project) {
        project.plugins.apply 'com.jfrog.bintray'
        
        project.afterEvaluate {
            final repoName  = resolveTargetRepo(project)
            applyBintrayConfig(project, repoName)
        }                  
    }

    private String resolveTargetRepo(Project project) {
        def repoName  = 'maven'
        if (project.tasks.findByPath('distTar') || project.tasks.findByPath('distZip')) {
            repoName = 'universal'
        }
        if (project.tasks.findByPath('buildRpm')) {
            repoName = 'rpm'
        }
        repoName
    }

    private void applyBintrayConfig(Project project, String repoName) {
        project.bintray {
            user = project.hasProperty('bintrayUser') ? project.property('bintrayUser') : System.getenv('BINTRAY_USER')
            key = project.hasProperty('bintrayKey') ? project.property('bintrayKey') : System.getenv('BINTRAY_KEY')
            publish = true
            if (repoName == 'maven') {
                publications = ['maven']
            }
            filesSpec {
                if (repoName == 'universal') {
                    if (project.tasks.findByPath('distTar')) {
                        from project.tasks.distZip
                    }
                    if (project.tasks.findByPath('distTar')) {
                        from project.tasks.distTar
                    }
                } else if (repoName == 'rpm') {
                    from project.tasks.buildRpm.outputs.files 
                }
                into '.'
            }
            pkg {
                name = project.rootProject.name
                repo = repoName
                licenses = ['Apache-2.0']
                vcsUrl = 'https://bitbucket.org/jellysource/coin-collector.git'
                version {
                    name = project.version
                    released = new Date()
                }
            }
        }
        if (project.bintray.filesSpec) {
            project.tasks.bintrayUpload.dependsOn(project.bintray.filesSpec as RecordingCopyTask)
        }
    }
}
