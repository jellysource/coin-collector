# Publishing plugin

Add common configuration for publishing modules currently to [bintray](https://bintray.com/jellysource).

There are three repositories: 
 - **rpm**
 - **unviversal** 
 - **maven**

Plugin will automatically choose one repository by distribution type according following rules:
 - if project contains task `buildRpm` then it's result will be uploaded to **rpm** repo
 - if project contains task `distZip` or `distTar` then their's results will be uploaded to **universal** repo
 - if project does not contain `buildRpm`, `distZip` neither `distTar` task then **jar** will be uploaded to **maven** repo

>Currently there is no support for publishing to more then one repository per project. 
>If project contains multiple distribution types **rpm** is prefered over **zip** / **tar** which are prefered over **jar**.

## How to build

    gradlew clean build

## How to use

Include build in your **settings.xml**.

    includeBuild '${REPOSITORY_ROOT}/tools/gradle-plugins/gradle-publishing-plugin'

Apply plugin in your **build.gradle**.

    
    plugins {
        id 'com.jellysource.gradle.publishing'
    }

Or add build script dependency in your **build.gradle** and apply

    buildscript {
        dependencies {
            classpath "com.jellysource.gradle:gradle-publishing-plugin"        
        }
    }

    apply plugin: 'com.jellysource.gradle.publishing'

To publish your project call task `bintrayUpload`

    ./gradlew bintrayUpload -PbintrayUser=YOUR_USER -PbintrayKey=YOUR_KEY

>You need replace YOUR_USER and YOUR_KEY with bintray credentials.
>Alternatively you can set environment variables `BINTRAY_USER` and `BINTRAY_KEY`