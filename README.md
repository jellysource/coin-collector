# Coin collector

Automated trading system.

>Detailed description comming soon.

## Repository structure

Coin collector repository is designed as monorepository which should contain sources of all parts of the system.

Repository contains following main directories:

 - **apps** - self deployable and runnable applications = system components
 - **dbs** - definitions of shared databases
 - **infra** - scripts defining system's infrastructure
 - **libs** - shared libraries used as dependencies in different **apps**
 - **strategies** - implementation of trading strategies (**Does NOT exist yet**)
 - **tools** - any other scripts or shared code used for operations

## Components

### Applications

> **TODO**: add diagram with overview of how components are related to each other

 - [Broker server](apps/broker-server/README.md)
 - [Broker command line interface](apps/broker-cli/README.md)
 - [Quotecenter](apps/quotecenter/README.md)
 - [Strategy executor](apps/strategy-executor/README.md)

### Databases

 - [Coin database](dbs/coin-db/README.md)

### Infrastructure

 - [Coin ansible playbooks](infra/coin-ansible-playbooks/README.md)

### Libraries
 - Broker API protocol
 - Broker API protocol in JSON
 - Broker client
 - [Coin database model](libs/coin-db-model/README.md)
 - [Coin database test](libs/coin-db-test/README.md)
 - Coin strategy

### Tools

We have some additional tool projects:

 - [Gradle Publishing plugin](tools/gradle-plugins/gradle-publishing-plugin/README.md)
 - [Gradle Scala plugin](tools/gradle-plugins/gradle-scala-plugin/README.md)
 - [Gradle Versioning plugin](tools/gradle-plugins/gradle-versioning-plugin/README.md)

## Build tool

Preferred build tool used in projects is [Gradle](https://gradle.org/). Each project should provide access to **Gradle** via [gradle wrapper](https://docs.gradle.org/current/userguide/gradle_wrapper.html).

## Project structure

Typical project has following structure:

 - **build/** - output files created during build 
 - **gradle/wrapper/** - gradle wrapper binary and properties
 - **build.gradle** - main build script definition file
 - **gradle.properties** - definitions of properties used in build scripts
 - **README.md** - readme file with basic information about project, about how to build, configure and run it
 - **gradlew** - gradle runner for LINUX / macOS
 - **gradlew.bat** - gradle runner for Windows
 - **settings.gradle** - [build settings file](https://docs.gradle.org/current/dsl/org.gradle.api.initialization.Settings.html) containing definitions of project name, it's modules and includes of other projects (dependencies within out system / repository

Files with source code are usually under **src** directory under project root or under project's modules directories.

## Dependencies between projects

When **Projet A** depends on **Project B** we want to assure that always the most recent version of **Project B** is used during **Project A** build. To achive this, feature of **Gradle** called [Composite builds](https://docs.gradle.org/current/userguide/composite_builds.html) is used for definition of dependencies between projects. Therefor there is no need to define version of dependency on internal projects.

To add dependency in **Project A** on **Project B**:

 1. Include build of **Project B** in **Project A** **settings.gradle** using `includeBuild` function with path to the root directory of **Project B** as parameter
 2. Add dependency in **Project A** **build.gradle** but ommit version part

## CI tool

[CircleCI](https://circleci.com/bb/jellysource/coin-collector) is used as CI tool. Configuration can be found in **.circleci/config.yml**. 

>Custom logic is used for discovery which projects should be build (which jobs should be run) when new changes are pushed into repository. This logic is implemented as few shell scripts under **.circleci** directory and it is described in more detail in it's [template repository](https://github.com/zladovan/monorepo).

When new project is added:

 1. Add new job in **.circleci/config.yml** named same as the root directory of project
 2. Ensure there is pattern including project's root directory in **.circleci/projects.txt**
 3. Include any other projects which should cause rebuild of new projects in **settings.gradle** with **includeBuild** function
