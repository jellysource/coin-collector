#!/bin/sh
getent group coin || groupadd -r coin
getent passwd executor || useradd -r -d /opt/strategy-executor -s /sbin/nologin -g coin executor

case "$1" in
  1)
    # This is an initial install.
  ;;
  2)
    # This is an upgrade
  ;;
esac