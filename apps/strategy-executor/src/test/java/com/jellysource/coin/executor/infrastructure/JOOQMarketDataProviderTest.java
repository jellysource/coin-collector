package com.jellysource.coin.executor.infrastructure;

import com.jellysource.coin.db.test.TestDbRule;
import com.jellysource.coin.executor.application.marketdata.OHLCDataRepository;
import com.jellysource.coin.executor.application.marketdata.OHLCRequestNarrower;
import com.jellysource.coin.executor.application.marketdata.OHLCRow;
import com.jellysource.coin.executor.infrastructure.marketdata.JOOQMarketDataProvider;
import javaslang.collection.List;
import lombok.val;
import org.junit.Rule;
import org.junit.Test;

import java.time.LocalDateTime;

import static com.jellysource.coin.executor.application.marketdata.OHLCRequestNarrower.between;
import static javaslang.collection.List.of;
import static org.assertj.core.api.Assertions.assertThat;


public class JOOQMarketDataProviderTest {

    @Rule
    public TestDbRule testDbRule = new TestDbRule();

    @Test
    public void getDataWithDateRange() throws Exception {
        val ohlcRepository = new JOOQMarketDataProvider(testDbRule.getJooqContext());

        val narrower = between(
            LocalDateTime.of(2000, 1, 3, 0, 1),
            LocalDateTime.of(2000, 1, 7, 23, 59)
        );
        List<OHLCRow> ohlcData = ohlcRepository.getData(narrower);

        List<OHLCRow> colaOHLC = ohlcData.filter(ohlc -> "KO".equals(ohlc.getTicker()));
        List<OHLCRow> attOHLC = ohlcData.filter(ohlc -> "T".equals(ohlc.getTicker()));
        assertThat(colaOHLC).hasSize(5);
        assertThat(attOHLC).hasSize(5);
    }

    @Test
    public void getDataWithTickerAndDateRange() throws Exception {
        OHLCDataRepository ohlcRepository = new JOOQMarketDataProvider(testDbRule.getJooqContext());

        OHLCRequestNarrower narrower = between(
            LocalDateTime.of(2000, 1, 3, 0, 1),
            LocalDateTime.of(2000, 1, 7, 23, 59)
        ).withTickers(of("KO"));

        List<OHLCRow> ohlcData = ohlcRepository.getData(narrower);

        List<OHLCRow> colaOHLC = ohlcData.filter(ohlc -> "KO".equals(ohlc.getTicker()));
        assertThat(colaOHLC).hasSize(5);
        assertThat(ohlcData)
            .filteredOn(ohlc -> !"KO".equals(ohlc.getTicker()))
            .hasSize(0);

        LocalDateTime firstDateTime = colaOHLC.get(0).getDateTime();
        LocalDateTime secondDateTime = colaOHLC.get(1).getDateTime();
        LocalDateTime lastDateTime = colaOHLC.get(4).getDateTime();
        assertThat(firstDateTime).isAfter(secondDateTime);
        assertThat(firstDateTime).isAfter(lastDateTime);
        assertThat(secondDateTime).isAfter(lastDateTime);
    }

}