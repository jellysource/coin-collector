package com.jellysource.coin.executor.application.position;

import com.jellysource.coin.strategy.domain.AllSharesTrade;
import com.jellysource.coin.strategy.domain.Position;
import com.jellysource.coin.strategy.domain.Trade;
import javaslang.collection.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDateTime;

import static java.math.BigDecimal.valueOf;
import static java.time.LocalDateTime.now;
import static java.util.UUID.randomUUID;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PositionServiceImplTest {

    @Mock
    LongShortPositionRepository longShortPositionRepository;

    private final static String EXECUTOR_ID = randomUUID().toString();

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getAllPositions() {
        when(longShortPositionRepository.getLongPositions(EXECUTOR_ID, false))
            .thenReturn(longPositions());

        PositionServiceImpl positionService = new PositionServiceImpl(
            longShortPositionRepository);

        List<Position> allPositions = positionService.getAllPositions(EXECUTOR_ID);
        List<Position> colaPosition = allPositions.filter(pos -> "KO".equals(pos.getSymbol()));
        assertThat(allPositions).hasSize(3);
        assertThat(colaPosition).hasSize(1);
        assertThat(colaPosition.get(0).getTrades()).hasSize(2);
    }

    @Test
    public void newLongTrade() {
        PositionServiceImpl positionService = new PositionServiceImpl(
            longShortPositionRepository);

        Trade colaTrade = new Trade(now(), 20, valueOf(44));
        positionService.newLongTrade(
            EXECUTOR_ID,
            "KO",
            colaTrade
        );
        verify(longShortPositionRepository).saveNewPosition(
            new LongShortPosition(
                EXECUTOR_ID,
                "KO",
                colaTrade.getShares(),
                colaTrade.getPrice(),
                LongShortPosition.PositionType.LONG,
                colaTrade.getDateTime()
            )
        );
    }

    @Test
    public void closePosition() {
        PositionServiceImpl positionService = new PositionServiceImpl(
            longShortPositionRepository);

        LocalDateTime now = now();
        AllSharesTrade allSharesTrade = new AllSharesTrade(now, valueOf(45));
        positionService.closePosition(EXECUTOR_ID, "KO", allSharesTrade);
        verify(longShortPositionRepository).closeAllPositions(EXECUTOR_ID, "KO", now, allSharesTrade.getPrice());
    }

    private List<LongShortPosition> longPositions() {
        return List.of(
            longPos("CAH", 20, 65f),
            longPos("KO", 20, 43f),
            longPos("KO", 20, 42f),
            longPos("T", 10, 30f),
            longPos("T", 10, 28f)
        );
    }

    private LongShortPosition longPos(String ticker, Integer quantity, Float price) {
        return new LongShortPosition(
            EXECUTOR_ID,
            ticker,
            quantity,
            valueOf(price),
            LongShortPosition.PositionType.LONG,
            now());
    }
}