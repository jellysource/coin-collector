package com.jellysource.coin.executor.infrastructure;

import com.jellysource.coin.db.test.TestDbRule;
import com.jellysource.coin.executor.application.position.LongShortPosition;
import com.jellysource.coin.executor.application.position.LongShortPosition.PositionType;
import com.jellysource.coin.executor.infrastructure.position.JOOQMarketPositionProvider;
import lombok.val;
import org.junit.Rule;
import org.junit.Test;

import java.time.LocalDateTime;

import static java.math.BigDecimal.valueOf;
import static java.util.UUID.randomUUID;
import static org.assertj.core.api.Assertions.assertThat;

public class JOOQMarketPositionProviderTest {

    private final String EXECUTOR_ID = "EXECUTOR-ID";

    @Rule
    public TestDbRule testRule = new TestDbRule();

    @Test
    public void getOpenedLongPositions() throws Exception {
        val positionProvider = new JOOQMarketPositionProvider(testRule.getJooqContext());
        val longPositions = positionProvider.getLongPositions(EXECUTOR_ID, false);
        assertThat(longPositions).hasSize(3);
        assertThat(longPositions).allMatch(LongShortPosition::isOpen);
    }

    @Test
    public void getAllLongPositions() throws Exception {
        val positionProvider = new JOOQMarketPositionProvider(testRule.getJooqContext());
        val longPositions = positionProvider.getLongPositions(EXECUTOR_ID, true);
        assertThat(longPositions).hasSize(5);
        assertThat(longPositions)
            .filteredOn(LongShortPosition::isClosed)
            .hasSize(2);
    }

    @Test
    public void saveNewPosition() throws Exception {
        val positionProvider = new JOOQMarketPositionProvider(testRule.getJooqContext());
        val testExecutorId = randomUUID().toString();
        positionProvider.saveNewPosition(
            longPos(testExecutorId, "TEST", 100, 50f)
        );

        val longPositions = positionProvider.getLongPositions(testExecutorId, false);
        assertThat(longPositions).hasSize(1);
        assertThat(longPositions)
            .first()
            .matches(LongShortPosition::isOpen)
            .matches(testPosition -> "TEST".equals(testPosition.getTicker()));
    }

    private LongShortPosition longPos(
        String executor,
        String ticker,
        Integer quantity,
        Float price) {
        return new LongShortPosition(
            executor,
            ticker,
            quantity,
            valueOf(price),
            PositionType.LONG,
            LocalDateTime.now()
        );
    }

    @Test
    public void closeAllOpenedPositions() throws Exception {
        val positionProvider = new JOOQMarketPositionProvider(testRule.getJooqContext());
        val allOpenPositions = positionProvider.getLongPositions(EXECUTOR_ID, false);
        assertThat(allOpenPositions)
            .filteredOn(pos -> "KO".equals(pos.getTicker()))
            .allMatch(LongShortPosition::isOpen);

        positionProvider.closeAllPositions(EXECUTOR_ID, "KO", LocalDateTime.now(), valueOf(50));
        val allOpenPositionsAgain = positionProvider.getLongPositions(EXECUTOR_ID, false);
        assertThat(allOpenPositionsAgain)
            .filteredOn(pos -> "KO".equals(pos.getTicker()))
            .allMatch(LongShortPosition::isClosed)
            .allMatch(position -> position.getClosedPrice().doubleValue() == 50);
    }

    /**
     * Closing has no effect on already closed positions
     * @throws Exception
     */
    @Test
    public void toCloseClosedPositions() throws Exception {
        val positionProvider = new JOOQMarketPositionProvider(testRule.getJooqContext());
        val positionsBeforeClose = positionProvider.getLongPositions(EXECUTOR_ID, true);
        assertThat(positionsBeforeClose)
            .filteredOn(pos -> "CAH".equals(pos.getTicker()))
            .allMatch(LongShortPosition::isClosed);

        positionProvider.closeAllPositions(EXECUTOR_ID, "CAH", LocalDateTime.now(), valueOf(65));
        val allPositionsAgain = positionProvider.getLongPositions(EXECUTOR_ID, true);
        assertThat(allPositionsAgain)
            .filteredOn(pos -> "CAH".equals(pos.getTicker()))
            .allMatch(LongShortPosition::isClosed)
            .allMatch(positionsBeforeClose::contains);
    }
}