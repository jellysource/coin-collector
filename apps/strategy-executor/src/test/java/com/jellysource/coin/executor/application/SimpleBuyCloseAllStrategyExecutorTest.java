package com.jellysource.coin.executor.application;

import com.jellysource.coin.executor.domain.*;
import com.jellysource.coin.strategy.domain.ExecParams;
import com.jellysource.coin.strategy.domain.Signal;
import com.jellysource.coin.strategy.domain.SignalExecution;
import com.jellysource.coin.strategy.domain.Strategy;
import javaslang.collection.List;
import org.assertj.core.api.Condition;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import static com.jellysource.coin.executor.domain.OrderExecution.market;
import static com.jellysource.coin.strategy.domain.Signal.buy;
import static com.jellysource.coin.strategy.domain.Signal.sell;
import static java.util.UUID.randomUUID;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SimpleBuyCloseAllStrategyExecutorTest {

    @Mock
    Strategy strategy;

    @Mock
    PositionService positionService;

    @Mock
    MarketDataService marketDataService;

    Condition<Order> isSellOrder = new Condition<>(Order::isSell, "is sell");
    Condition<Order> isBuyOrder = new Condition<>(Order::isBuy, "is buy");

    private final static String STRATEGY_ID = randomUUID().toString();

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void executeStrategy() {
        List<Signal> signals = sellSignals().appendAll(buySignals());
        when(strategy.process(any(), any())).thenReturn(signals);
        when(strategy.getExecParams()).thenReturn(strategyParams());

        LocalDateTime now = LocalDateTime.now();
        List<Order> orders = strategyExecutor().executeStrategy(strategy, now);

        assertThat(orders).hasSize(4);
        assertThat(orders).areExactly(2, isBuyOrder);
        assertThat(orders).areExactly(2, isSellOrder);
        verify(positionService).getAllPositions(STRATEGY_ID);
        verify(marketDataService).getData(any(), eq(now), eq(List.of( "T", "KO", "CAH")));
    }

    @Test
    public void orderStatusUpdate() {
        StrategyExecutor strategyExecutor = strategyExecutor();

        OrderUpdate cahUpdate = new OrderUpdate(
            STRATEGY_ID,
            LocalDateTime.now(),
            OrderStatus.FILLED,
            new Order("CAH", 20, OrderType.SELL, market(64d))
        );
        strategyExecutor.orderStatusUpdate(cahUpdate);
        verify(positionService).closePosition(STRATEGY_ID, "CAH", cahUpdate.toAllSharesTrade());
    }

    private List<Signal> sellSignals() {
        return List.of(
            sell("CAH").quantity(20).at(SignalExecution.MARKET),
            sell("JNJ").quantity(20).at(SignalExecution.MARKET));
    }

    private List<Signal> buySignals() {
        return List.of(
            buy("KO").quantity(20).at(SignalExecution.MARKET),
            buy("T").quantity(20).at(SignalExecution.MARKET));
    }

    private SimpleBuyCloseAllStrategyExecutor strategyExecutor() {
        return new SimpleBuyCloseAllStrategyExecutor(
            positionService,
            marketDataService
        );
    }

    private ExecParams strategyParams() {
        return new ExecParams(
            STRATEGY_ID,
            List.of("T", "KO", "CAH"),
            100,
            ChronoUnit.DAYS,
            null,null,null
        );
    }

}