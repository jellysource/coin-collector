package com.jellysource.coin.executor.application.marketdata;

import com.jellysource.coin.strategy.domain.TickerData;
import javaslang.collection.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.when;

public class MarketDataServiceImplTest {

    @Mock
    OHLCDataRepository ohlcDataRepository;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getData() throws Exception {
        LocalDateTime from = january2000(1);
        LocalDateTime to = january2000(3);
        when(ohlcDataRepository.getData(argThat(narrowerHasSameDates(from, to)))
        ).thenReturn(
            List.of(
                ohlc("KO", 41f, 44f, 41f, 43f, 1),
                ohlc("KO", 42f, 44f, 41f, 43.2f, 2),
                ohlc("KO", 40f, 42f, 38f, 41f, 3),
                ohlc("CAH", 65f, 66f, 63f, 64f, 1),
                ohlc("CAH", 53f, 54f, 50f, 52f, 2),
                ohlc("CAH", 50f, 51f, 49f, 51f, 3)
            )
        );

        MarketDataServiceImpl symbolDataService = new MarketDataServiceImpl(
            ohlcDataRepository);

        List<TickerData> symbolData = symbolDataService.getData(from, to);
        List<TickerData> colaData = symbolData.filter(data -> "KO".equals(data.getTicker()));
        assertThat(symbolData).hasSize(2);
        assertThat(colaData.get(0).getBars()).hasSize(3);
    }

    private ArgumentMatcher<OHLCRequestNarrower> narrowerHasSameDates(
        LocalDateTime from,
        LocalDateTime to) {
        return narrower ->
            from.equals(narrower.getFrom()) &&
            to.equals(narrower.getTo());
    }

    private LocalDateTime january2000(Integer dayOfMonth) {
        return LocalDateTime.of(2000,1, dayOfMonth, 1, 1);
    }

    private OHLCRow ohlc(
        String ticker,
        Float open,
        Float high,
        Float low,
        Float close,
        Integer dayOfMonth) {

        return new OHLCRow(
            ticker,
            january2000(dayOfMonth),
            BigDecimal.valueOf(open),
            BigDecimal.valueOf(high),
            BigDecimal.valueOf(low),
            BigDecimal.valueOf(close));
    }

}