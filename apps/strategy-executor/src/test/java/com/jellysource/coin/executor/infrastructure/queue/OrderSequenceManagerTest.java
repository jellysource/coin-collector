package com.jellysource.coin.executor.infrastructure.queue;

import com.jellysource.coin.executor.application.ordersignal.OrderSignal;
import javaslang.collection.List;
import org.junit.Test;

import static com.jellysource.coin.executor.infrastructure.queue.OrderSequenceManager.from;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class OrderSequenceManagerTest {

  @Test
  public void managerSequence() throws Exception {
    OrderSequence sequence = from(null,
        List.of(orderSignal("SELL-A"), orderSignal("SELL-B")),
        List.of(orderSignal("SELL-C"), orderSignal("SELL-D")),
        List.of(orderSignal("BUY-A"), orderSignal("BUY-B"))
    ).getOrderSequence();

    assertEquals(sequence.getSignals().get(0).getId(), "SELL-A");
    assertEquals(sequence.getSignals().get(1).getId(), "SELL-B");
    assertEquals(sequence.getNext().getSignals().get(0).getId(), "SELL-C");
    assertEquals(sequence.getNext().getSignals().get(1).getId(), "SELL-D");
    assertEquals(sequence.getNext().getNext().getSignals().get(0).getId(), "BUY-A");
    assertEquals(sequence.getNext().getNext().getSignals().get(1).getId(), "BUY-B");
    assertNull(sequence.getNext().getNext().getNext());
  }

  private OrderSignal orderSignal(String id) {
    return new OrderSignal(id,
        null,
        null,
        null,
        null,
        null,
        null,
        null);
  }

}