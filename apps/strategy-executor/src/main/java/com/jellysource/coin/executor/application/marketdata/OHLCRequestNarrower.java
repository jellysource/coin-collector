package com.jellysource.coin.executor.application.marketdata;

import javaslang.collection.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.Wither;

import java.time.LocalDateTime;

@Getter
@Wither
@AllArgsConstructor
public class OHLCRequestNarrower {

    LocalDateTime from;
    LocalDateTime to;
    Boolean historicDataOnly;
    List<String> tickers;
    Integer limit;

    public static OHLCRequestNarrower between(LocalDateTime from, LocalDateTime to) {
        return new OHLCRequestNarrower(
            from,
            to,
            false,
            null,
            null
        );
    }
}
