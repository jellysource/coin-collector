package com.jellysource.coin.executor.domain;

public enum OrderType {
  BUY, SELL
}
