package com.jellysource.coin.executor.infrastructure;

import com.jellysource.broker.api.protocol.Order;
import com.jellysource.broker.client.BrokerClient;
import com.jellysource.coin.executor.ApplicationConfig.BrokerConf.JmsConfig;
import com.jellysource.coin.executor.application.ordersignal.OrderSignal;
import com.jellysource.coin.executor.infrastructure.queue.AmqpOrderEventClient;
import com.jellysource.coin.executor.infrastructure.queue.OrderEventObserver;
import javaslang.collection.List;
import lombok.AllArgsConstructor;
import lombok.val;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@AllArgsConstructor
public class Broker {
  private static final Logger LOG = LoggerFactory.getLogger(Broker.class);

  private BrokerClient brokerClient;
  private JmsConfig orderResponseChannelConf;
  private List<OrderEventObserver> orderEventObservers;

  public void submitOrder(OrderSignal orderSignal, OrderEventObserver onEventAction) {
    prepareOrderEventsClient(orderSignal, onEventAction);
    callBrokerAPI(toBrokerOrder(orderSignal));
  }

  private void callBrokerAPI(Order order) {
    brokerClient.placeOrder(order)
        .onSuccess(done ->
            LOG.info(
                "Order for {} submitted via broker API",
                order.symbol()))
        .onFailure(failure ->
            LOG.info(
                "Problem submitting {} via broker API: {}",
                order.symbol(),
                failure.getMessage())
        );
  }

  private void prepareOrderEventsClient(OrderSignal signal, OrderEventObserver onEventAction) {
    try {
      val amqpConsumer = new AmqpOrderEventClient(
          signal,
          orderEventObservers.append(onEventAction));

      amqpConsumer.connect(
          orderResponseChannelConf.getUri(),
          orderResponseChannelConf.getExchange());
    } catch (Exception ex) {
      LOG.error("Could not connect consumer for signal: {}", signal.toString());
    }
  }

  private com.jellysource.broker.api.protocol.Order toBrokerOrder(OrderSignal orderSignal) {
    //TODO: Add support also for LIMIT and STOP orders
    return orderSignal.isBuy()
        ? toBuyBrokerOrder(orderSignal)
        : toSellBrokerOrder(orderSignal);
  }

  private com.jellysource.broker.api.protocol.Order toSellBrokerOrder(OrderSignal orderSignal) {
    return com.jellysource.broker.api.protocol.Order
        .sell(orderSignal.getQuantity(), orderSignal.getTicker())
        .market()
        .withID(orderSignal.getId());
  }

  private com.jellysource.broker.api.protocol.Order toBuyBrokerOrder(OrderSignal orderSignal) {
    return com.jellysource.broker.api.protocol.Order
        .buy(orderSignal.getQuantity(), orderSignal.getTicker())
        .market()
        .withID(orderSignal.getId());
  }


}
