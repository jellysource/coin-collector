package com.jellysource.coin.executor.application.ordersignal;

import lombok.Value;
import lombok.experimental.Wither;

import java.math.BigDecimal;

@Value
@Wither
public class OrderSignal {
  String id;
  String strategy;
  String ticker;
  Integer quantity;
  OrderSignalType orderSignalType;
  OrderSignalExecution execution;
  OrderSignalState signalState;
  BigDecimal price;

  public boolean isBuy() {
    return OrderSignalType.BUY == orderSignalType;
  }

  public boolean isSell() {
    return OrderSignalType.SELL == orderSignalType;
  }
}
