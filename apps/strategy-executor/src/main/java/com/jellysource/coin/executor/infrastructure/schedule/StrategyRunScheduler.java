package com.jellysource.coin.executor.infrastructure.schedule;

import com.coreoz.wisp.Scheduler;
import com.jellysource.coin.executor.application.ordersignal.*;
import com.jellysource.coin.executor.domain.Order;
import com.jellysource.coin.executor.domain.OrderExecution;
import com.jellysource.coin.executor.domain.OrderType;
import com.jellysource.coin.executor.domain.StrategyExecutor;
import com.jellysource.coin.executor.infrastructure.OrderSignalProcessor;
import com.jellysource.coin.strategy.domain.Strategy;
import javaslang.API;
import javaslang.collection.List;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

import static com.jellysource.coin.executor.infrastructure.schedule.CronSchedule.parseQuartzCron;
import static java.time.LocalDateTime.now;
import static javaslang.API.$;
import static javaslang.API.Case;

@AllArgsConstructor
public class StrategyRunScheduler {
  private static final Logger LOG = LoggerFactory.getLogger(StrategyRunScheduler.class);
  private static Scheduler scheduler = new Scheduler();

  private StrategyExecutor strategyExecutor;
  private OrderSignalRepository signalRepository;
  private List<OrderSignalProcessor> signalObservers;

  public void schedule(Strategy strategy, String strategyId, ZonedCronSchedule schedule) {
    scheduler.schedule(
        () -> notifyObservers(strategyRun(strategy, strategyId)),
        parseQuartzCron(schedule));
    LOG.info("Scheduled {} for cron: {} to run in timezone {}",
        strategyId, schedule.getCronExpression(), schedule.getZoneId());
  }

  private void notifyObservers(List<OrderSignal> orderSignals) {
    signalObservers.forEach(observer -> observer.onSignalsPublished(orderSignals));
  }

  private List<OrderSignal> strategyRun(Strategy strategy, String strategyId) {
    List<Order> orders = strategyExecutor.executeStrategy(strategy, now());
    List<OrderSignal> orderSignals = newOrderSignals(strategyId, orders);
    signalRepository.newOrderSignals(orderSignals);
    return orderSignals;
  }

  private List<OrderSignal> newOrderSignals(String strategyId, List<Order> orders) {
    return orders.map(order ->
        new OrderSignal(
            UUID.randomUUID().toString(),
            strategyId,
            order.getSymbol(),
            order.getQuantity(),
            toOrderSignalType(order.getOrderType()),
            toOrderSignalExecution(order.getOrderExecution()),
            OrderSignalState.CREATED,
            order.getOrderExecution().getPrice()
        )
    );
  }

  private OrderSignalType toOrderSignalType(OrderType orderType) {
    return API.Match(orderType).of(
        Case($(OrderType.BUY), OrderSignalType.BUY),
        Case($(OrderType.SELL), OrderSignalType.SELL)
    );
  }

  private OrderSignalExecution toOrderSignalExecution(OrderExecution orderExecution) {
    return API.Match(orderExecution.getType()).of(
        Case($(OrderExecution.OrderExecutionType.MARKET), OrderSignalExecution.MARKET),
        Case($(OrderExecution.OrderExecutionType.LIMIT), OrderSignalExecution.LIMIT),
        Case($(OrderExecution.OrderExecutionType.STOP), OrderSignalExecution.STOP)
    );
  }

}
