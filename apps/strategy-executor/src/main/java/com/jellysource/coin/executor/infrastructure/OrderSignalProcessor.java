package com.jellysource.coin.executor.infrastructure;

import com.jellysource.coin.executor.application.ordersignal.OrderSignal;
import javaslang.collection.List;

public interface OrderSignalProcessor {

  void onSignalsPublished(List<OrderSignal> newOrderSignals);
}
