package com.jellysource.coin.executor.application.position;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class LongShortPosition {

    @NonNull
    String executorId;
    @NonNull
    String ticker;
    @NonNull
    Integer quantity;
    @NonNull
    BigDecimal price;
    @NonNull
    PositionType type;
    @NonNull
    LocalDateTime created;
    LocalDateTime closed;
    BigDecimal closedPrice;

    public Boolean isOpen() {
        return  !isClosed();
    }

    public Boolean isClosed() {
        return  closed != null;
    }

    public enum PositionType {

        LONG("L"),
        SHORT("S");

        String value;

        PositionType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public static PositionType fromString(String from) {
            for(PositionType type : PositionType.values()) {
                if(type.value.equals(from)) {
                    return type;
                }
            }
            return null;
        }
    }
}
