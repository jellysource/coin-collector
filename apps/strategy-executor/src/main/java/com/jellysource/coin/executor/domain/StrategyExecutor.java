package com.jellysource.coin.executor.domain;

import com.jellysource.coin.strategy.domain.Strategy;
import javaslang.collection.List;
import javaslang.control.Option;

import java.time.LocalDateTime;

public interface StrategyExecutor {

  /**
   * @param now represents date-time until strategy should load market data
   *            If null is passed, current date-time is used.
   * @return list of orders
   */
  List<Order> executeStrategy(Strategy strategy, LocalDateTime now);

  /**
   * @param orderUpdate should update state of current positions that are
   *                    about to be executed. If pending orders exists
   * @return nothing if everything went well, problem otherwise
   */
  Option<OrderUpdateProblem> orderStatusUpdate(OrderUpdate orderUpdate);
}
