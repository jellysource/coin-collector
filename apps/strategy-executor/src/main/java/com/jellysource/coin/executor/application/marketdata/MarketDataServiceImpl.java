package com.jellysource.coin.executor.application.marketdata;

import com.jellysource.coin.executor.domain.MarketDataService;
import com.jellysource.coin.strategy.domain.BarData;
import com.jellysource.coin.strategy.domain.TickerData;
import javaslang.collection.List;
import javaslang.collection.Seq;
import lombok.AllArgsConstructor;

import java.time.LocalDateTime;

import static com.jellysource.coin.executor.application.marketdata.OHLCRequestNarrower.between;
import static javaslang.collection.List.of;

@AllArgsConstructor
public class MarketDataServiceImpl implements MarketDataService {

    private OHLCDataRepository ohlcDataRepository;

    @Override
    public List<TickerData> getData(LocalDateTime from, LocalDateTime to, String... tickers) {
        List<OHLCRow> data = ohlcDataRepository.getData(
            between(from, to).withTickers(of(tickers))
        );

        Seq<TickerData> mappedData = data
            .groupBy(OHLCRow::getTicker)
            .map(group -> new TickerData(
                group._1,
                toBarData(group._2)
            ));

        return List.ofAll(mappedData);
    }

    @Override
    public List<LocalDateTime> getTradingDates(LocalDateTime from, LocalDateTime to) {
        return ohlcDataRepository.getUniqueTradingDates(from, to);
    }

    private List<BarData> toBarData(List<OHLCRow> ohlcRows) {
        return ohlcRows.map(ohlc -> new BarData(
            ohlc.getDateTime(),
            ohlc.getOpen(),
            ohlc.getHigh(),
            ohlc.getLow(),
            ohlc.getClose(),
            0,
            ohlc.getClose()
        ));
    }

}
