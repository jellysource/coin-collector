package com.jellysource.coin.executor.application;

import com.jellysource.coin.executor.domain.MarketDataService;
import com.jellysource.coin.executor.domain.Order;
import com.jellysource.coin.executor.domain.OrderExecution;
import com.jellysource.coin.executor.domain.OrderStatus;
import com.jellysource.coin.executor.domain.OrderType;
import com.jellysource.coin.executor.domain.OrderUpdate;
import com.jellysource.coin.executor.domain.OrderUpdateProblem;
import com.jellysource.coin.executor.domain.PositionService;
import com.jellysource.coin.executor.domain.StrategyExecutor;
import com.jellysource.coin.strategy.domain.DataFeed;
import com.jellysource.coin.strategy.domain.ExecParams;
import com.jellysource.coin.strategy.domain.Signal;
import com.jellysource.coin.strategy.domain.SignalExecution;
import com.jellysource.coin.strategy.domain.SignalType;
import com.jellysource.coin.strategy.domain.Strategy;
import com.jellysource.coin.strategy.domain.StrategyContext;
import javaslang.API;
import javaslang.collection.List;
import javaslang.control.Option;
import lombok.AllArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static com.jellysource.coin.executor.domain.OrderUpdateProblem.problemWithMessageOnly;
import static javaslang.API.$;
import static javaslang.API.Case;

@AllArgsConstructor
public class SimpleBuyCloseAllStrategyExecutor implements StrategyExecutor {

  PositionService positionService;
  MarketDataService marketDataService;

  @Override
  public List<Order> executeStrategy(Strategy strategy, LocalDateTime now) {
    LocalDateTime executionDate = Option.of(now).getOrElse(LocalDateTime.now());
    ExecParams execParams = strategy.getExecParams();

    List<Signal> result = strategy.process(
        new StrategyContext(
            executionDate,
            positionService.getAllPositions(execParams.getStrategyId())),
        prepareDataFeed(execParams, executionDate)
    );

    return toOrders(result);
  }

  private List<Order> toOrders(List<Signal> signals) {
    return signals.map(signal ->
        new Order(
            signal.getSymbol(),
            signal.getQuantity(),
            toOrderType(signal.getType()),
            toOrderExecution(signal.getSignalExecution(), signal.getPrice())
        )
    );
  }

  private OrderType toOrderType(SignalType signalType) {
    return SignalType.BUY.equals(signalType)
        ? OrderType.BUY
        : OrderType.SELL;
  }

  private OrderExecution toOrderExecution(SignalExecution execution, BigDecimal price) {
    return API.Match(execution).of(
        Case($(SignalExecution.MARKET), OrderExecution.market()),
        Case($(SignalExecution.LIMIT), OrderExecution.limit(price.doubleValue())),
        Case($(SignalExecution.STOP), OrderExecution.stop(price.doubleValue()))
    );
  }

  DataFeed prepareDataFeed(ExecParams execParams, LocalDateTime now) {
    return new DataFeed(
        marketDataService.getData(
            now.minus(execParams.getUnitsBack(), execParams.getFrequency()),
            now,
            execParams.getTickers())
    );
  }

  @Override
  public Option<OrderUpdateProblem> orderStatusUpdate(OrderUpdate orderUpdate) {
    if (!OrderStatus.FILLED.equals(orderUpdate.getOrderStatus())) {
      return Option.of(problemWithMessageOnly("Order status problem: order not filled"));
    }
    return updateState(orderUpdate);
  }

  private Option<OrderUpdateProblem> updateState(OrderUpdate orderUpdate) {
    Order filledOrder = orderUpdate.getOrder();
    if (filledOrder.isSell()) {
      positionService.closePosition(
          orderUpdate.getIdentifier(),
          filledOrder.getSymbol(),
          orderUpdate.toAllSharesTrade());
    } else {
      positionService.newLongTrade(
          orderUpdate.getIdentifier(),
          filledOrder.getSymbol(),
          orderUpdate.toTrade());
    }
    return Option.none();
  }
}
