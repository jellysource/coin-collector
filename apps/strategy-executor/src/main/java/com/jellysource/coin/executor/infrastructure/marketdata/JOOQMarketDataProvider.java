package com.jellysource.coin.executor.infrastructure.marketdata;

import com.jellysource.coin.db.Tables;
import com.jellysource.coin.db.tables.MarketData;
import com.jellysource.coin.executor.application.marketdata.OHLCDataRepository;
import com.jellysource.coin.executor.application.marketdata.OHLCRequestNarrower;
import com.jellysource.coin.executor.application.marketdata.OHLCRow;
import javaslang.collection.List;
import lombok.AllArgsConstructor;
import lombok.val;
import org.jooq.DSLContext;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import static com.jellysource.coin.executor.application.utils.DateTimeHelper.toLocalDateTime;
import static java.sql.Timestamp.valueOf;

@AllArgsConstructor
public class JOOQMarketDataProvider implements OHLCDataRepository {

  private DSLContext jooq;
  private final MarketData MARKET_DATA = Tables.MARKET_DATA;

  @Override
  public List<OHLCRow> getData(OHLCRequestNarrower dataSpec) {
    val query = jooq.selectFrom(MARKET_DATA);

    if (dataSpec.getTickers() != null && dataSpec.getTickers().nonEmpty()) {
      query.where(MARKET_DATA.TICKER.in(dataSpec.getTickers().toJavaList()));
    }

    if (dataSpec.getFrom() != null) {
      Timestamp from = valueOf(dataSpec.getFrom());
      query.where(MARKET_DATA.DATE.greaterOrEqual(from));
    }

    if (dataSpec.getTo() != null) {
      Timestamp to = valueOf(dataSpec.getTo());
      query.where(MARKET_DATA.DATE.lessOrEqual(to));
    }

    if (dataSpec.getLimit() != null) {
      query.limit(dataSpec.getLimit());
    }

    val result = query
        .orderBy(MARKET_DATA.TICKER.asc(), MARKET_DATA.DATE.desc())
        .fetch();

    Iterable<OHLCRow> ohlcResults = result.map(record -> new OHLCRow(
        record.getTicker(),
        toLocalDateTime(record.getDate()),
        record.getClose(),
        record.getClose(),
        record.getClose(),
        record.getClose()
    ));

    return List.ofAll(
        ohlcResults
    );
  }

  @Override
  public List<LocalDateTime> getUniqueTradingDates(LocalDateTime from, LocalDateTime to) {
    val query = jooq
        .selectDistinct(MARKET_DATA.DATE)
        .from(MARKET_DATA);
    Timestamp fromTimestamp = valueOf(from);
    Timestamp toTimestamp = valueOf(to);

    Iterable<LocalDateTime> uniqueDates = query
        .where(MARKET_DATA.DATE.between(fromTimestamp, toTimestamp))
        .orderBy(MARKET_DATA.DATE.asc())
        .fetch()
        .map(record -> toLocalDateTime(record.value1()));

    return List.ofAll(
        uniqueDates
    );
  }
}
