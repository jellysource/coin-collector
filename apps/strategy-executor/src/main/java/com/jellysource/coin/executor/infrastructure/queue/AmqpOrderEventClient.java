package com.jellysource.coin.executor.infrastructure.queue;

import com.jellysource.broker.api.protocol.OrderEvent;
import com.jellysource.broker.api.protocol.OrderFilled;
import com.jellysource.coin.executor.application.ordersignal.OrderSignal;
import com.rabbitmq.client.*;
import javaslang.collection.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

import static com.jellysource.broker.api.protocol.json.JsonSerDe.deserializeOrderEvent;

public class AmqpOrderEventClient {
  private static Logger LOG = LoggerFactory.getLogger(AmqpOrderEventClient.class);
  private static final String ALL_EVENTS = "#";

  private OrderSignal orderSignal;
  private List<OrderEventObserver> observers = List.empty();
  private QueueConsumer consumer;

  public AmqpOrderEventClient(
      OrderSignal orderSignal,
      List<OrderEventObserver> eventObservers) {
    this.orderSignal = orderSignal;
    this.observers = this.observers.appendAll(eventObservers);
  }

  public void connect(String uri, String exchange) throws Exception{
    Channel channel = openChannel(uri);
    channel.exchangeDeclare(exchange, "topic", true);

    String queueName = createQueueName(orderSignal);
    channel.queueDeclare(queueName, false, false, false, null);
    channel.queueBind(queueName, exchange, bindingKey(orderSignal.getId()));

    consumer = new QueueConsumer(channel);
    channel.basicConsume(queueName, true, consumer);
  }

  public void destroy() throws Exception {
    consumer.getChannel().close();
    consumer.getChannel().getConnection().close();
  }

  private void onDelivery(OrderEvent orderEvent) {
    observers.forEach(observer -> observer.onOrderEvent(orderEvent));
  }

  private String createQueueName(OrderSignal orderSignal) {
    return orderSignal.getStrategy()
        .toLowerCase()
        .replaceAll(" ","")
        .concat("." + orderSignal.getId());
  }

  private String bindingKey(String orderId) {
    return String.format("%s.%s", orderId, ALL_EVENTS);
  }

  private Channel openChannel(String uri) throws Exception {
    ConnectionFactory factory = new ConnectionFactory();
    factory.setUri(uri);

    Connection connection = factory.newConnection();
    return connection.createChannel();
  }

  private class QueueConsumer extends DefaultConsumer {

    QueueConsumer(Channel channel) {
      super(channel);
    }

    @Override
    public void handleDelivery(
        String consumerTag,
        Envelope envelope,
        AMQP.BasicProperties properties,
        byte[] body) throws IOException {
      super.handleDelivery(consumerTag, envelope, properties, body);

      OrderEvent event = deserializeOrderEvent(body);
      LOG.info("Event id {} in {} strategy queue consumed: {} ",
          event.id(),
          orderSignal.getStrategy(),
          event.toString());

      onDelivery(event);
      if(event instanceof OrderFilled) {
        autoDestruct();
      }
    }

    private void autoDestruct() {
      try {
        destroy();
      } catch(Exception ex) {
        LOG.error("Could not close channel: {}", ex.getMessage());
      }
    }
  }
}
