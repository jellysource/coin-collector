package com.jellysource.coin.executor.domain;

import com.jellysource.coin.strategy.domain.AllSharesTrade;
import com.jellysource.coin.strategy.domain.Trade;
import lombok.Value;

import java.time.LocalDateTime;

@Value
public class OrderUpdate {

    String identifier;
    LocalDateTime timestamp;
    OrderStatus orderStatus;
    Order order;

    public Trade toTrade() {
        return new Trade(timestamp, order.getQuantity(), order.getOrderExecution().getPrice());
    }

    public AllSharesTrade toAllSharesTrade() {
        return new AllSharesTrade(timestamp, order.getOrderExecution().getPrice());
    }
}
