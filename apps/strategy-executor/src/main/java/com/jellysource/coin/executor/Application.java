package com.jellysource.coin.executor;

import com.jellysource.broker.client.BrokerClient;
import com.jellysource.broker.client.BrokerClientConfig;
import com.jellysource.coin.executor.ApplicationConfig.BrokerConf;
import com.jellysource.coin.executor.ApplicationConfig.DatabaseConf;
import com.jellysource.coin.executor.ApplicationConfig.StrategyConf;
import com.jellysource.coin.executor.application.SimpleBuyCloseAllStrategyExecutor;
import com.jellysource.coin.executor.application.marketdata.MarketDataServiceImpl;
import com.jellysource.coin.executor.application.ordersignal.OrderSignalRepository;
import com.jellysource.coin.executor.application.position.PositionServiceImpl;
import com.jellysource.coin.executor.infrastructure.Broker;
import com.jellysource.coin.executor.infrastructure.OrderDealer;
import com.jellysource.coin.executor.infrastructure.marketdata.JOOQMarketDataProvider;
import com.jellysource.coin.executor.infrastructure.ordersignal.JOOQOrderSignalProvider;
import com.jellysource.coin.executor.infrastructure.position.JOOQMarketPositionProvider;
import com.jellysource.coin.executor.infrastructure.queue.OrderEventCollector;
import com.jellysource.coin.executor.infrastructure.queue.OrderEventObserver;
import com.jellysource.coin.executor.infrastructure.schedule.StrategyRunScheduler;
import com.jellysource.coin.executor.infrastructure.schedule.ZonedCronSchedule;
import com.jellysource.coin.strategy.domain.ExecParams;
import com.jellysource.coin.strategy.domain.Strategy;
import com.zaxxer.hikari.HikariDataSource;
import javaslang.collection.List;
import javaslang.control.Try;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.temporal.ChronoUnit;

public class Application {

  private static final Logger LOG = LoggerFactory.getLogger(Application.class);
  private static ApplicationConfig properties;

  public static void main(String[] args) {
    try {
      properties = new ApplicationConfig();
      bootstrap();
      LOG.info("Up and running");
      keepAlive();
    } catch (Exception ex) {
      LOG.error("Error in main:", ex);
      System.exit(1);
    }
  }

  private static void keepAlive() throws InterruptedException {
    synchronized (Application.class) {
      Application.class.wait();
    }
  }

  private static void bootstrap() throws Exception {
    DSLContext jooq = createJOOQContext();
    MarketDataServiceImpl marketDataService = marketDataService(jooq);
    PositionServiceImpl positionService = positionService(jooq);
    OrderSignalRepository orderSignalRepository = orderSignalRepository(jooq);

    OrderEventCollector orderEventCollector = orderEventCollector(
        orderSignalRepository,
        positionService
    );
    OrderDealer orderDealer = new OrderDealer(
        broker(orderEventCollector)
    );

    SimpleBuyCloseAllStrategyExecutor strategyExecutor = strategyExecutor(
        positionService,
        marketDataService
    );
    StrategyRunScheduler strategyRunScheduler = new StrategyRunScheduler(
        strategyExecutor,
        orderSignalRepository,
        List.of(orderDealer)
    );

    List<StrategyConf> strategyConfs = List.ofAll(properties.getStrategyConf());
    strategyConfs
        .forEach(conf ->
            getStrategy(
                conf.getClassName(),
                toExecParams(conf)
            ).andThen(strategy ->
                strategyRunScheduler.schedule(
                    strategy,
                    conf.getIdentifier(),
                    ZonedCronSchedule.from(conf.getSchedule())
                )
            )
        );
  }

  private static ExecParams toExecParams(StrategyConf conf) {
    return new ExecParams(
        conf.getIdentifier(),
        List.ofAll(conf.getTickers()),
        conf.getUnitsBack(),
        ChronoUnit.DAYS,
        conf.getMaxSlots(),
        conf.getMinimalSlotValue(),
        conf.getMaxNewSymbolsPerDay()
    );
  }

  private static Try<Strategy> getStrategy(String className, ExecParams params) {
    return Try.of(() -> {
      Class<?> strategy = Class.forName(className);
      return (Strategy) strategy
          .getConstructors()[0]
          .newInstance(params);
    });
  }

  private static DSLContext createJOOQContext() throws Exception {
    DatabaseConf dbConf = properties.getDatabaseConf();

    HikariDataSource ds = new HikariDataSource();
    ds.setDriverClassName(dbConf.driver);
    ds.setJdbcUrl(dbConf.url);
    ds.setUsername(dbConf.username);
    ds.setPassword(dbConf.password);

    return DSL.using(ds, SQLDialect.valueOf(dbConf.sqlDialect));
  }

  static Broker broker(OrderEventObserver eventObserver) {
    BrokerConf brokerConf = properties.getBrokerConf();
    return new Broker(
        new BrokerClient(new BrokerClientConfig(brokerConf.getRest().getUri())),
        brokerConf.getJms(),
        List.of(eventObserver)
    );
  }

  static OrderEventCollector orderEventCollector(
      OrderSignalRepository signalRepository,
      PositionServiceImpl positionService) {
    return new OrderEventCollector(signalRepository, positionService);
  }

  static OrderSignalRepository orderSignalRepository(DSLContext jooq) {
    return new JOOQOrderSignalProvider(jooq);
  }

  static PositionServiceImpl positionService(DSLContext jooq) {
    return new PositionServiceImpl(new JOOQMarketPositionProvider(jooq));
  }

  static MarketDataServiceImpl marketDataService(DSLContext jooq) {
    return new MarketDataServiceImpl(new JOOQMarketDataProvider(jooq));
  }


  private static SimpleBuyCloseAllStrategyExecutor strategyExecutor(
      PositionServiceImpl positionService,
      MarketDataServiceImpl marketDataService) {
    return new SimpleBuyCloseAllStrategyExecutor(
        positionService,
        marketDataService
    );
  }
}
