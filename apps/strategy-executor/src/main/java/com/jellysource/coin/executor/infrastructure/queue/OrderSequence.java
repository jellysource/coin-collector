package com.jellysource.coin.executor.infrastructure.queue;

import com.jellysource.coin.executor.application.ordersignal.OrderSignal;
import com.jellysource.coin.executor.application.ordersignal.OrderSignalState;
import javaslang.collection.List;
import lombok.Data;

@Data
public class OrderSequence {
  List<OrderSignal> signals;
  OrderSequence next;

  public OrderSequence(List<OrderSignal> signals) {
    this.signals = signals;
  }

  public Boolean isEmpty() {
    return (signals == null || signals.isEmpty()) && (next == null || next.isEmpty());
  }

  public void changeSignalState(String id, OrderSignalState signalState) {
    signals
        .find(signal -> id.equals(signal.getId()))
        .peek(signal -> signals = signals.replace(signal, signal.withSignalState(signalState)));
  }

  public Boolean hasOnlyFilledSignals() {
    return signals.foldLeft(true,
        (acc, signal) -> acc && OrderSignalState.FILLED == signal.getSignalState());
  }
}
