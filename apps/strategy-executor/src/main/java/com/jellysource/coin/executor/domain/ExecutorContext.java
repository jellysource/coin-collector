package com.jellysource.coin.executor.domain;

import com.jellysource.coin.strategy.domain.Strategy;
import javaslang.control.Option;
import lombok.Value;

import java.time.LocalDateTime;
import java.time.temporal.TemporalUnit;

@Value
public class ExecutorContext {

    String executorId;
    Strategy strategy;
    Integer barCount;
    TemporalUnit frequency;
    Option<LocalDateTime> today;
}
