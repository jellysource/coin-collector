package com.jellysource.coin.executor.application.ordersignal;

public enum OrderSignalExecution {
  MARKET("M"),
  LIMIT("L"),
  STOP("S");

  String acronym;

  public String getAcronym() {
    return acronym;
  }

  OrderSignalExecution(String acronym) {
    this.acronym = acronym;
  }

  public static OrderSignalExecution fromString(String from) {
    for(OrderSignalExecution signalExecution : values()) {
      if(signalExecution.acronym.equals(from)) {
        return signalExecution;
      }
    }
    return null;
  }
}
