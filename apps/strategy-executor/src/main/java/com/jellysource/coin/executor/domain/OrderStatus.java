package com.jellysource.coin.executor.domain;

public enum OrderStatus {
    PLACED,
    FILLED,
    CENCELLED
}
