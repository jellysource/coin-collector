package com.jellysource.coin.executor.application.ordersignal;

public enum  OrderSignalState {
  CREATED("C"),
  PLACED("P"),
  FILLED("F");


  String acronym;

  public String getAcronym() {
    return acronym;
  }

  OrderSignalState(String acronym) {
    this.acronym = acronym;
  }

  public static OrderSignalState fromString(String from) {
    for(OrderSignalState signalState : values()) {
      if(signalState.acronym.equals(from)) {
        return signalState;
      }
    }
    return null;
  }
}
