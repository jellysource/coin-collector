package com.jellysource.coin.executor.application.marketdata;

import lombok.Value;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Value
public class OHLCRow {

  String ticker;
  LocalDateTime dateTime;
  BigDecimal open;
  BigDecimal high;
  BigDecimal low;
  BigDecimal close;
}
