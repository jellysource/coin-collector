package com.jellysource.coin.executor.domain;

import lombok.Value;

@Value
public class OrderUpdateProblem {

  Exception exception;
  String optionalMessage;

  public static OrderUpdateProblem problemWithMessageOnly(String message) {
    return new OrderUpdateProblem(
        null,
        message
    );
  }
}
