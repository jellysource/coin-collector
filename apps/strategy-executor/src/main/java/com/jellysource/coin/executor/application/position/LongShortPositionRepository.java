package com.jellysource.coin.executor.application.position;


import javaslang.collection.List;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public interface LongShortPositionRepository {

  List<LongShortPosition> getLongPositions(String executorId, Boolean includeClosed);

  void saveNewPosition(LongShortPosition longShortPosition);

  void closeAllPositions(
      String strategyId,
      String ticker,
      LocalDateTime dateTime,
      BigDecimal closePrice
  );
}
