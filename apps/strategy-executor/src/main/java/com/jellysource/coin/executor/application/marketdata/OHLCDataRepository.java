package com.jellysource.coin.executor.application.marketdata;

import javaslang.collection.List;

import java.time.LocalDateTime;

public interface OHLCDataRepository {

    List<OHLCRow> getData(OHLCRequestNarrower dataSpec);
    List<LocalDateTime> getUniqueTradingDates(LocalDateTime from, LocalDateTime to);
}
