package com.jellysource.coin.executor.infrastructure;

import com.jellysource.coin.executor.application.ordersignal.OrderSignal;
import com.jellysource.coin.executor.infrastructure.queue.OrderSequenceManager;
import javaslang.collection.List;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.jellysource.coin.executor.infrastructure.queue.OrderSequenceManager.from;

@AllArgsConstructor
public class OrderDealer implements OrderSignalProcessor {
  private static final Logger LOG = LoggerFactory.getLogger(OrderDealer.class);

  private Broker broker;

  @Override
  public void onSignalsPublished(List<OrderSignal> orderSignals) {
    List<OrderSignal> sellSignals = orderSignals.filter(OrderSignal::isSell);
    List<OrderSignal> buySignals = orderSignals.filter(OrderSignal::isBuy);

    if(sellSignals.nonEmpty()) {
      submitSignalsToBroker(from(this, sellSignals, buySignals));
    } else if (buySignals.nonEmpty()) {
      submitSignalsToBroker(from(this, buySignals));
    } else {
      LOG.info("No signals to process");
    }
  }

  private void submitSignalsToBroker(OrderSequenceManager sequenceManager) {
    sequenceManager
        .getOrderSequence()
        .getSignals()
        .forEach(signal -> broker.submitOrder(signal, sequenceManager));
  }
}
