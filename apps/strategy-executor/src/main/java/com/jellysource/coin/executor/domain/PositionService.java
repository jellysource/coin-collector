package com.jellysource.coin.executor.domain;

import com.jellysource.coin.strategy.domain.AllSharesTrade;
import com.jellysource.coin.strategy.domain.Position;
import com.jellysource.coin.strategy.domain.Trade;
import javaslang.collection.List;

public interface PositionService {

    /**
     * @param executorId
     * @return all positions associated with identifier
     */
    List<Position> getAllPositions(String executorId);

    /**
     * @param ticker
     * @param trade
     */
    void newLongTrade(String strategyId, String ticker, Trade trade);

    /**
     * @param ticker
     */
    void closePosition(String strategyId, String ticker, AllSharesTrade trade);
}
