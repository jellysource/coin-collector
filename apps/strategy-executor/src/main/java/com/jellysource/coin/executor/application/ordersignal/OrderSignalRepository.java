package com.jellysource.coin.executor.application.ordersignal;

import javaslang.collection.List;

import java.math.BigDecimal;

public interface OrderSignalRepository {

  OrderSignal get(String orderId);

  List<OrderSignal> listOrderSingals(
      String executorId,
      OrderSignalType type,
      OrderSignalState signalState
  );

  void newOrderSignals(List<OrderSignal> orderSignal);

  void orderSignalPlaced(String id);

  void orderSignalFilled(String id, BigDecimal price);
}
