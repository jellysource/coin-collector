package com.jellysource.coin.executor.application.utils;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class DateTimeHelper {

  public static LocalDateTime toLocalDateTime(Timestamp timestamp) {
    return timestamp != null
        ? timestamp.toLocalDateTime()
        : null;
  }
}
