package com.jellysource.coin.executor.infrastructure.queue;

import com.jellysource.broker.api.protocol.OrderEvent;
import com.jellysource.broker.api.protocol.OrderFilled;
import com.jellysource.broker.api.protocol.OrderPlaced;
import com.jellysource.coin.executor.application.ordersignal.OrderSignal;
import com.jellysource.coin.executor.application.ordersignal.OrderSignalRepository;
import com.jellysource.coin.executor.application.position.PositionServiceImpl;
import com.jellysource.coin.strategy.domain.AllSharesTrade;
import com.jellysource.coin.strategy.domain.Trade;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static javaslang.API.*;
import static javaslang.Predicates.instanceOf;

@AllArgsConstructor
public class OrderEventCollector implements OrderEventObserver {
  private static Logger LOG = LoggerFactory.getLogger(OrderEventCollector.class);

  OrderSignalRepository orderSignalRepository;
  PositionServiceImpl positionService;

  @Override
  public void onOrderEvent(OrderEvent event) {
    LOG.info("Event id {} consumed: {} ", event.id(), event.toString());
    Match(event).of(
        Case($(instanceOf(OrderPlaced.class)), placed -> run(() -> orderPlaced(placed))),
        Case($(instanceOf(OrderFilled.class)), filled -> run(() -> orderFilled(filled))),
        Case($(), o -> run(() -> LOG.info("Ignoring event " + o.toString())))
    );
  }

  void orderPlaced(OrderPlaced event) {
    OrderSignal orderSignal = orderSignalRepository.get(event.id());
    LOG.trace(
        "Changed order state of {} to PLACED for strategy {}",
        orderSignal.getId(),
        orderSignal.getStrategy());
    orderSignalRepository.orderSignalPlaced(orderSignal.getId());
  }

  void orderFilled(OrderFilled filledEvent) {
    OrderSignal orderSignal = orderSignalRepository.get(filledEvent.id());

    orderSignalRepository.orderSignalFilled(filledEvent.id(), filledEvent.price().bigDecimal());
    updatePositions(filledEvent, orderSignal);
  }

  private void updatePositions(OrderFilled filledEvent, OrderSignal signal) {
    Match(signal.isBuy()).of(
        Case($(true), o -> run(() -> buySignalFilled(filledEvent, signal))),
        Case($(false), o -> run(() -> sellSignalFilled(filledEvent, signal)))
    );
  }

  private void buySignalFilled(OrderFilled filledEvent, OrderSignal orderSignal) {
    positionService.newLongTrade(
        orderSignal.getStrategy(),
        orderSignal.getTicker(),
        new Trade(
            filledEvent.time().toLocalDateTime(),
            orderSignal.getQuantity(),
            filledEvent.price().bigDecimal()));
  }

  private void sellSignalFilled(OrderFilled filledEvent, OrderSignal orderSignal) {
    positionService.closePosition(
        orderSignal.getStrategy(),
        orderSignal.getTicker(),
        new AllSharesTrade(
            filledEvent.time().toLocalDateTime(),
            filledEvent.price().bigDecimal()));
  }
}
