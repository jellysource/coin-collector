package com.jellysource.coin.executor.application.ordersignal;

public enum OrderSignalType {
  BUY("B"),
  SELL("S");

  String acronym;

  public String getAcronym() {
    return acronym;
  }

  OrderSignalType(String acronym) {
    this.acronym = acronym;
  }

  public static OrderSignalType fromString(String from) {
    for(OrderSignalType signalType : values()) {
      if(signalType.acronym.equals(from)) {
        return signalType;
      }
    }
    return null;
  }
}
