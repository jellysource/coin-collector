package com.jellysource.coin.executor.application.position;

import com.jellysource.coin.executor.domain.PositionService;
import com.jellysource.coin.strategy.domain.AllSharesTrade;
import com.jellysource.coin.strategy.domain.Position;
import com.jellysource.coin.strategy.domain.Trade;
import javaslang.collection.List;
import javaslang.collection.Seq;
import lombok.AllArgsConstructor;
import lombok.val;

@AllArgsConstructor
public class PositionServiceImpl implements PositionService {

    LongShortPositionRepository longShortPositionRepository;

    @Override
    public List<Position> getAllPositions(String executorId) {
        val longShortPositions = longShortPositionRepository
            .getLongPositions(executorId, false);

        Seq<Position> stockPositions = longShortPositions
            .groupBy(LongShortPosition::getTicker)
            .map(group -> new Position(
                group._1,
                toTradeList(group._2)
            ));

        return List.ofAll(stockPositions);
    }

    private List<Trade> toTradeList(List<LongShortPosition> longShortPositions) {
        return longShortPositions
            .map(longShortPosition -> new Trade(
                longShortPosition.getCreated(),
                longShortPosition.getQuantity(),
                longShortPosition.getPrice()))
            .sortBy(Trade::getDateTime);
    }

    @Override
    public void newLongTrade(String strategyId, String ticker, Trade trade) {
        longShortPositionRepository.saveNewPosition(new LongShortPosition(
            strategyId,
            ticker,
            trade.getShares(),
            trade.getPrice(),
            LongShortPosition.PositionType.LONG,
            trade.getDateTime()
        ));
    }

    @Override
    public void closePosition(String strategyId, String ticker, AllSharesTrade trade) {
        longShortPositionRepository.closeAllPositions(
            strategyId,
            ticker,
            trade.getDateTime(),
            trade.getPrice()
        );
    }
}
