package com.jellysource.coin.executor.infrastructure.schedule;

import com.jellysource.coin.executor.ApplicationConfig;
import lombok.Value;

import java.time.ZoneId;

@Value
public class ZonedCronSchedule {

  ZoneId zoneId;
  String cronExpression;

  public static ZonedCronSchedule from(ApplicationConfig.StrategyConf.StrategySchedule schedule) {
    return new ZonedCronSchedule(
        ZoneId.of(schedule.getTimezone()),
        schedule.getCron());
  }
}
