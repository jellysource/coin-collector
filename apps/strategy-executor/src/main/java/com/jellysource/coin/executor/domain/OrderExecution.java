package com.jellysource.coin.executor.domain;

import lombok.Value;

import java.math.BigDecimal;

import static com.jellysource.coin.executor.domain.OrderExecution.OrderExecutionType.*;
import static java.math.BigDecimal.ZERO;
import static java.math.BigDecimal.valueOf;

@Value
public class OrderExecution {

  public static enum OrderExecutionType {
    MARKET, LIMIT, STOP
  }

  BigDecimal price;
  OrderExecutionType type;

  public static OrderExecution market() {
    return new OrderExecution(ZERO, MARKET);
  }

  public static OrderExecution market(Double price) {
    return new OrderExecution(valueOf(price), MARKET);
  }

  public static OrderExecution limit(Double price) {
    return new OrderExecution(valueOf(price), LIMIT);
  }

  public static OrderExecution stop(Double price) {
    return new OrderExecution(valueOf(price), STOP);
  }
}
