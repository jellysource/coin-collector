package com.jellysource.coin.executor.infrastructure.queue;

import com.jellysource.broker.api.protocol.OrderEvent;

public interface OrderEventObserver {

  void onOrderEvent(OrderEvent orderEvent);
}
