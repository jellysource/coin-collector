package com.jellysource.coin.executor.infrastructure.queue;

import com.jellysource.broker.api.protocol.OrderEvent;
import com.jellysource.broker.api.protocol.OrderFilled;
import com.jellysource.broker.api.protocol.OrderPlaced;
import com.jellysource.coin.executor.application.ordersignal.OrderSignal;
import com.jellysource.coin.executor.application.ordersignal.OrderSignalState;
import com.jellysource.coin.executor.infrastructure.OrderDealer;
import javaslang.collection.List;
import lombok.Value;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static javaslang.API.*;
import static javaslang.Predicates.instanceOf;

@Value
public class OrderSequenceManager implements OrderEventObserver {
  private static final Logger LOG = LoggerFactory.getLogger(OrderSequenceManager.class);

  OrderDealer orderDealer;
  OrderSequence orderSequence;

  @Override
  public void onOrderEvent(OrderEvent event) {
    LOG.info("Event id {} consumed: {} ", event.id(), event.toString());
    Match(event).of(
        Case($(instanceOf(OrderPlaced.class)), o -> run(() -> orderPlaced(event.id()))),
        Case($(instanceOf(OrderFilled.class)), o -> run(() -> orderFilled(event.id()))),
        Case($(), o -> run(() -> LOG.info("Ignoring event " + o.toString())))
    );
  }

  private void orderPlaced(String id) {
    orderSequence.changeSignalState(id, OrderSignalState.PLACED);
  }

  private void orderFilled(String id) {
    orderSequence.changeSignalState(id, OrderSignalState.FILLED);
    if(orderSequence.hasOnlyFilledSignals() && nextSequenceIsNotEmpty(orderSequence)) {
      orderDealer.onSignalsPublished(orderSequence.getNext().getSignals());
    } else{
      LOG.info("Order sequence finished");
    }
  }

  private Boolean nextSequenceIsNotEmpty(OrderSequence sequence) {
    return sequence.getNext() != null && !sequence.getNext().isEmpty();
  }

  public static OrderSequenceManager from(OrderDealer orderDealer, List<OrderSignal>... orderSequence) {
    return new OrderSequenceManager(
        orderDealer,
        constructSequence(List.of(orderSequence).map(OrderSequence::new))
    );
  }

  private static OrderSequence constructSequence(List<OrderSequence> sequences) {
    return sequences.nonEmpty()
      ? sequences.reduceRight(
        (earlier, later) -> {
          earlier.setNext(later);
          return earlier;
        })
      : new OrderSequence(List.empty());
  }
}
