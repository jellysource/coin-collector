package com.jellysource.coin.executor.application;

import javaslang.collection.List;
import lombok.Value;

import java.time.temporal.TemporalUnit;

@Value
public class ExecutorProps {
  String executorId;
  Integer unitsBack;
  TemporalUnit frequency;
  List<String> tickers;
}
