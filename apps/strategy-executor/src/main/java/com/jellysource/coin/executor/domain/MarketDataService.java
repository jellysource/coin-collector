package com.jellysource.coin.executor.domain;

import com.jellysource.coin.strategy.domain.TickerData;
import javaslang.collection.List;

import java.time.LocalDateTime;

public interface MarketDataService {

  List<TickerData> getData(LocalDateTime from, LocalDateTime to, String... tickers);

  /**
   * @param from
   * @param to
   * @return unique trading dates between specified two
   */
  List<LocalDateTime> getTradingDates(LocalDateTime from, LocalDateTime to);


  default List<TickerData> getData(LocalDateTime from, LocalDateTime to, List<String> tickers) {
    return getData(from, to, tickers.toJavaArray(String.class));
  }
}
