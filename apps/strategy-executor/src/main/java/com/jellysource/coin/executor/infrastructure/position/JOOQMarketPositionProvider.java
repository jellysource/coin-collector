package com.jellysource.coin.executor.infrastructure.position;

import com.jellysource.coin.db.Tables;
import com.jellysource.coin.db.tables.MarketPosition;
import com.jellysource.coin.executor.application.position.LongShortPosition;
import com.jellysource.coin.executor.application.position.LongShortPositionRepository;
import javaslang.collection.List;
import lombok.AllArgsConstructor;
import lombok.val;
import org.jooq.DSLContext;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static com.jellysource.coin.executor.application.position.LongShortPosition.PositionType.LONG;
import static com.jellysource.coin.executor.application.position.LongShortPosition.PositionType.fromString;
import static com.jellysource.coin.executor.application.utils.DateTimeHelper.toLocalDateTime;
import static java.sql.Timestamp.valueOf;
import static java.util.UUID.randomUUID;

@AllArgsConstructor
public class JOOQMarketPositionProvider implements LongShortPositionRepository {

  private DSLContext jooq;
  private final MarketPosition MARKET_POSITION = Tables.MARKET_POSITION;

  @Override
  public List<LongShortPosition> getLongPositions(String executorId, Boolean includeClosed) {
    val query = jooq
        .selectFrom(MARKET_POSITION)
        .where(MARKET_POSITION.EXECUTOR_ID.eq(executorId))
        .and(MARKET_POSITION.TYPE.eq(LONG.getValue()));

    if (!includeClosed) {
      query.and(MARKET_POSITION.CLOSED.isNull());
    }

    val result = query.fetch();

    Iterable<LongShortPosition> positions = result.map(marketPosition -> new LongShortPosition(
        marketPosition.getExecutorId(),
        marketPosition.getTicker(),
        marketPosition.getQuantity(),
        marketPosition.getPrice(),
        fromString(marketPosition.getType()),
        toLocalDateTime(marketPosition.getCreated()),
        toLocalDateTime(marketPosition.getClosed()),
        marketPosition.getClosePrice()
    ));

    return List.ofAll(
        positions
    );
  }

  @Override
  public void saveNewPosition(LongShortPosition longShortPosition) {
    jooq.insertInto(MARKET_POSITION)
        .columns(
            MARKET_POSITION.ID,
            MARKET_POSITION.EXECUTOR_ID,
            MARKET_POSITION.TICKER,
            MARKET_POSITION.QUANTITY,
            MARKET_POSITION.PRICE,
            MARKET_POSITION.TYPE,
            MARKET_POSITION.CREATED)
        .values(
            randomUUID().toString(),
            longShortPosition.getExecutorId(),
            longShortPosition.getTicker(),
            longShortPosition.getQuantity(),
            longShortPosition.getPrice(),
            longShortPosition.getType().getValue(),
            valueOf(longShortPosition.getCreated())
        ).execute();
  }

  @Override
  public void closeAllPositions(
      String strategyId,
      String ticker,
      LocalDateTime now,
      BigDecimal closePrice) {
    jooq.update(MARKET_POSITION)
        .set(MARKET_POSITION.CLOSED, valueOf(now))
        .set(MARKET_POSITION.CLOSE_PRICE, closePrice)
        .where(MARKET_POSITION.EXECUTOR_ID.eq(strategyId))
        .and(MARKET_POSITION.TICKER.eq(ticker))
        .and(MARKET_POSITION.CLOSED.isNull())
        .execute();
  }
}
