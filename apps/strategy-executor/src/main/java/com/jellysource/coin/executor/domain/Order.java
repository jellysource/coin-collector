package com.jellysource.coin.executor.domain;

import lombok.Value;

@Value
public class Order {

  String symbol;
  Integer quantity;
  OrderType orderType;
  OrderExecution orderExecution;

  public boolean isSell() {
    return OrderType.SELL.equals(orderType);
  }

  public boolean isBuy() {
    return OrderType.BUY.equals(orderType);
  }

}
