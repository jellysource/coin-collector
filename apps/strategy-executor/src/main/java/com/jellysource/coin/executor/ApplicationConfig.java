package com.jellysource.coin.executor;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigBeanFactory;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigObject;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

public class ApplicationConfig {

  private Config properties;

  public ApplicationConfig() {
    properties = ConfigFactory.load();
  }

  public BrokerConf getBrokerConf() {
    return ConfigBeanFactory.create(
        properties.getConfig("broker"),
        BrokerConf.class);
  }

  public DatabaseConf getDatabaseConf() {
    return ConfigBeanFactory.create(
        properties.getConfig("database"),
        DatabaseConf.class
    );
  }

  public List<StrategyConf> getStrategyConf() {
    return properties.getList("strategy")
        .stream()
        .map(strategyConf -> ConfigBeanFactory.create(
            ((ConfigObject)strategyConf).toConfig(),
            StrategyConf.class))
        .collect(Collectors.toList());
  }

  @Data
  public static class BrokerConf {
    JmsConfig jms;
    RestConfig rest;

    @Data
    public static class JmsConfig {
      String uri;
      String exchange;
      String binding;
    }

    @Data
    public static class RestConfig {
      String uri;
    }
  }

  @Data
  public static class DatabaseConf {
    String driver;
    String sqlDialect;
    String url;
    String username;
    String password;
  }

  @Data
  public static class StrategyConf {
    String identifier;
    String className;
    List<String> tickers;
    Integer unitsBack;
    StrategySchedule schedule;
    Integer maxSlots;
    Integer minimalSlotValue;
    Integer maxNewSymbolsPerDay;

    @Data
    public static class StrategySchedule {
      String cron;
      String timezone;
    }
  }
}
