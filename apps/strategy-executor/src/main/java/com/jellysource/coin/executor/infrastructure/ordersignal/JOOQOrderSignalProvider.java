package com.jellysource.coin.executor.infrastructure.ordersignal;

import com.jellysource.coin.db.Tables;
import com.jellysource.coin.db.tables.MarketSignal;
import com.jellysource.coin.db.tables.records.MarketSignalRecord;
import com.jellysource.coin.executor.application.ordersignal.*;
import javaslang.collection.List;
import lombok.AllArgsConstructor;
import lombok.val;
import org.jooq.DSLContext;

import java.math.BigDecimal;

@AllArgsConstructor
public class JOOQOrderSignalProvider implements OrderSignalRepository {

  private DSLContext jooq;
  private final MarketSignal MARKET_SIGNAL = Tables.MARKET_SIGNAL;

  @Override
  public OrderSignal get(String signalId) {
    val result = jooq
        .selectFrom(MARKET_SIGNAL)
        .where(MARKET_SIGNAL.SIGNAL_ID.eq(signalId))
        .fetch();

    Iterable<OrderSignal> list = result.map(record -> new OrderSignal(
        record.getSignalId(),
        record.getStrategy(),
        record.getTicker(),
        record.getQuantity(),
        OrderSignalType.fromString(record.getSignalType()),
        OrderSignalExecution.fromString(record.getExecution()),
        OrderSignalState.fromString(record.getSignalType()),
        record.getPrice()
    ));

    return list.iterator().hasNext()
        ? list.iterator().next()
        : null;
  }

  @Override
  public List<OrderSignal> listOrderSingals(String strategy, OrderSignalType type, OrderSignalState signalState) {
    val result = jooq
        .selectFrom(MARKET_SIGNAL)
        .where(
            MARKET_SIGNAL.STRATEGY.eq(strategy),
            MARKET_SIGNAL.SIGNAL_TYPE.eq(type.getAcronym()),
            MARKET_SIGNAL.SIGNAL_STATE.eq(signalState.getAcronym()))
        .fetch();

    Iterable<OrderSignal> signals = result.map(record -> new OrderSignal(
        record.getSignalId(),
        record.getStrategy(),
        record.getTicker(),
        record.getQuantity(),
        OrderSignalType.fromString(record.getSignalType()),
        OrderSignalExecution.fromString(record.getExecution()),
        OrderSignalState.fromString(record.getSignalType()),
        record.getPrice()
    ));

    return List.ofAll(
        signals
    );
  }

  @Override
  public void newOrderSignals(List<OrderSignal> orderSignals) {
    List<MarketSignalRecord> records = orderSignals.map(orderSignal ->
        new MarketSignalRecord(
            orderSignal.getId(),
            orderSignal.getStrategy(),
            orderSignal.getTicker(),
            orderSignal.getQuantity(),
            orderSignal.getOrderSignalType().getAcronym(),
            orderSignal.getSignalState().getAcronym(),
            orderSignal.getExecution().getAcronym(),
            null
        )
    );

    jooq.batchInsert(records.toJavaList()).execute();
  }

  @Override
  public void orderSignalPlaced(String id) {
    jooq.update(MARKET_SIGNAL)
        .set(MARKET_SIGNAL.SIGNAL_STATE, OrderSignalState.PLACED.getAcronym())
        .where(MARKET_SIGNAL.SIGNAL_ID.eq(id))
        .execute();
  }

  @Override
  public void orderSignalFilled(String id, BigDecimal price) {
    jooq.update(MARKET_SIGNAL)
        .set(MARKET_SIGNAL.SIGNAL_STATE, OrderSignalState.FILLED.getAcronym())
        .set(MARKET_SIGNAL.PRICE, price)
        .where(MARKET_SIGNAL.SIGNAL_ID.eq(id))
        .execute();
  }
}
