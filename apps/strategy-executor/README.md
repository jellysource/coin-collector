# Coin collector - Strategy executor

Strategy executor is application which can run trading strategies.

## How to build for HSQLDB

    ./gradlew clean buildRpm

## How to build for PostgreSQL

    ./gradlew clean buildRpm -Pdb=local-postgres

You need to have locally runnig PostgreSQL database with database **quotecenter** with user **quotecenter** without password
 
>If you do not have local PostgreSQL instance you can use docker image
>
>`docker run -it -e POSTGRES_DB=quotecenter -e POSTGRES_USER=quotecenter -p 5432:5432 postgres:9.6-alpine`

## How to run for HSQLDB

    ./gradlew run

## How to run for PostgreSQL

    ./gradlew run -Pdb=local-postgres

## How to publish

    ./gradlew bintrayUpload -Pdb=local-postgres -PbintrayUser=YOUR_USER -PbintrayKey=YOUR_KEY

You need replace YOUR_USER and YOUR_KEY with bintray credentials.

## Deployment

Resulting package is `RPM` which can be installed on some machine. It contains strategy executor application.

### How to install

    sudo yum install strategy-executor

### Where it is installed

    /opt/strategy-executor
        ├── bin
        ├── conf -> /etc/strategy-executor
        └── log  -> /var/log/coin-strategy-executor/

### How to start

    sudo service strategy-executor start

### How to restart

    sudo service strategy-executor restart

### How to check status

    sudo service strategy-executor status

### How to configure

Configuration file can be found on path `/etc/strategy-executor/executor.conf`. It is written in [HCON](https://github.com/lightbend/config/blob/master/HOCON.md) format.

| Property name                 | Default value | Description                               |
|-------------------------------|---------------|-------------------------------------------|
| database.sqlDialect           | POSTGRES      | [sql dialect](https://www.jooq.org/doc/3.11/manual/sql-building/dsl-context/sql-dialects/) used by [jOOQ](https://www.jooq.org) framework |
| database.driver               | org.postgresql.Driver | JDBC driver class name (`org.hsqldb.jdbc.JDBCDriver` or `org.postgresql.Driver`) |
| database.url                  | jdbc:postgresql://127.0.0.1/quotecenter | JDCB url to target database |
| database.username             | quotecenter   | user with read / write privileges to target database |
| database.password             | quotecenter   | password for database user                |
| broker.jms.uri                | amqp://guest:guest@127.0.0.1:5672 | connection URI to message broker implementing [AMQP protocol](https://www.amqp.org/) where order events are published |
| broker.jms.exchange           | coin.order.event | name of exchange where order events are published |
| broker.jms.binding            | #             | not used |
| broker.rest.uri               | localhost:9001 | host and port of [Broker server](../broker-server/README.md) REST API |
| strategy                      |               | array of strategy blocks (see below) |

### How to configure strategy

Currently there is only one strategy called **Mario Bros**.

>**TODO** add description of Mario Bros (Ninety) strategy

| Property name                 | Default value | Description                              |
|-------------------------------|---------------|------------------------------------------|
| identifier                    | Mario Bros    | id of strategy                           |
| className                     | com.jellysource.coin.strategy.application.strategy.NinetyStrategy | name of class implementing strategy |
| tickers                       | all symbols from [S&P 100](https://en.wikipedia.org/wiki/S%26P_100) | array of symbols which can be traded     |
| unitsBack                     | 300           | **TODO**                                 |
| schedule.cron                 | 0 50 10 ? * * * | [cron](https://en.wikipedia.org/wiki/Cron) expression defining when strategy should be triggered | 
| schedule.timezone             | America/New_York | timezone which should be used for resolving cron expression |
| maxSlots                      | 20            | how many slots can be used for positions (**TODO** clarify) |
| minimalSlotValue              | 5000          | how big is one slot (**TODO** clarify) |
| maxNewSymbolsPerDay           | 1             | how many buys of new assets can be done per day |
