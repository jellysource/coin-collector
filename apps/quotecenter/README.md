# Coin collector - Quotecenter

Quotecenter is application responsible for collecting and providing realtime and historical prices of trading assets.

## How to build for HSQLDB

    ./gradlew clean buildRpm

## How to build for PostgreSQL

    ./gradlew clean buildRpm -Pdb=local-postgres

You need to have locally runnig PostgreSQL database with database **quotecenter** with user **quotecenter** without password
 
>If you do not have local PostgreSQL instance you can use docker image
>
>`docker run -it -e POSTGRES_DB=quotecenter -e POSTGRES_USER=quotecenter -p 5432:5432 postgres:9.6-alpine`

## How to run for HSQLDB

    ./gradlew bootRun --args='--spring.profiles.active=local'

## How to run for PostgreSQL

    ./gradlew bootRun -Pdb=local-postgres

## How to publish

    ./gradlew bintrayUpload -Pdb=local-postgres -PbintrayUser=YOUR_USER -PbintrayKey=YOUR_KEY

You need replace YOUR_USER and YOUR_KEY with bintray credentials.

## Deployment

Resulting package is `RPM` which can be installed on some machine. It contains quotecenter application.

### How to install

    sudo yum install quotecenter

### Where it is installed

    /opt/quotecenter
        ├── bin
        ├── conf -> /etc/quotecenter
        └── log  -> /var/log/coin-quotecenter/

### How to start

    sudo service quotecenter start

### How to restart

    sudo service quotecenter restart

### How to check status

    sudo service quotecenter status

### How to configure

Configuration file can be found on path `/etc/quotecenter/quotecenter.properties`. It is written in [.properties](https://en.wikipedia.org/wiki/.properties) file format.

| Property name                 | Default value | Description                               |
|-------------------------------|---------------|-------------------------------------------|
| spring.jooq.sql-dialect       | Postgres      | [sql dialect](https://www.jooq.org/doc/3.11/manual/sql-building/dsl-context/sql-dialects/) used by [jOOQ](https://www.jooq.org) framework |
| spring.jooq.schema-name       | PUBLIC        | name of database schema                   |
| spring.datasource.driverClassName | org.postgresql.Driver | JDBC driver class name (`org.hsqldb.jdbc.JDBCDriver` or `org.postgresql.Driver`) |
| spring.datasource.url         | jdbc:postgresql://127.0.0.1/quotecenter | JDCB url to target database |
| spring.datasource.username    | quotecenter   | user with read / write privileges to target database |
| spring.datasource.password    | quotecenter   | password for database user                |
| logging.config                |               | path to logging configuration file, we use [logback](https://logback.qos.ch/) |
