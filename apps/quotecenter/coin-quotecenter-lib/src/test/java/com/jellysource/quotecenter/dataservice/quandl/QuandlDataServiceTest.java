package com.jellysource.quotecenter.dataservice.quandl;

import com.jimmoores.quandl.classic.ClassicQuandlSession;
import org.junit.Before;
import org.junit.Test;

import static com.jimmoores.quandl.SessionOptions.Builder.withAuthToken;
import static com.jimmoores.quandl.classic.ClassicQuandlSession.create;

public class QuandlDataServiceTest {

  private ClassicQuandlSession quandlSession;

  @Before
  public void setup() {
    quandlSession = create(
        withAuthToken("Zq-ZZ5e3yt4N6wP-z2hy").build()
    );
  }

  @Test
  public void downloadHistoricData() {
    QuandlDataService dataService = new QuandlDataService(null, quandlSession);

    //TODO:
    /*dataService.downloadHistoricData(
        Arrays.asList("T"),
        LocalDate.of(2018, 1, 1),
        LocalDate.of(2018, 1, 10)
    );*/

    /*System.out.println(
        quandlSession
            .getDataSet(DataSetRequest.Builder.of("WIKI/T")
                .withStartDate(LocalDate.of(2018, 4, 1))
                .withEndDate(LocalDate.of(2018, 4, 30))
                .build())
            .toPrettyPrintedString()
    );*/

  }

}