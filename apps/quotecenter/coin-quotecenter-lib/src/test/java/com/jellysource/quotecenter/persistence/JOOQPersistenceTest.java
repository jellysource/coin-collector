package com.jellysource.quotecenter.persistence;

import com.jellysource.coin.db.Tables;
import com.jellysource.coin.db.tables.MarketData;
import com.jellysource.coin.db.tables.records.MarketDataRecord;
import com.jellysource.coin.db.test.TestDbRule;
import com.jellysource.quotecenter.persistence.entity.DailyData;
import org.jooq.DSLContext;
import org.jooq.Result;
import org.jooq.SQLDialect;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.*;

public class JOOQPersistenceTest {

    private static final String TICKER = "AAPL";
    private static final LocalDateTime now = LocalDate.now().atStartOfDay();
    private static final MarketData MARKET_DATA = Tables.MARKET_DATA;

    @Rule
    public TestDbRule testDbRule = new TestDbRule();
    private Persistence persistence;

    @Before
    public void setup() {
        persistence = new JOOQPersistence(getJooq());
    }

    @Before
    @After
    public void cleanup(){
        getJooq().delete(MARKET_DATA).execute();
    }

    @Test
    public void persistSingleDataRecordTest(){
        DailyData dailyClose = DailyData.builder()
                .ticker(TICKER)
                .close(BigDecimal.valueOf(999))
                .modified(Timestamp.valueOf(now))
                .date(Timestamp.valueOf(now))
                .realtimeClose(true)
                .build();

        persistence.persistSingleDataRecord(dailyClose);
        Result<MarketDataRecord> fetchedResults = getJooq().selectFrom(MARKET_DATA)
                .where(MARKET_DATA.TICKER.eq(TICKER))
                .and(MARKET_DATA.CLOSE.eq(BigDecimal.valueOf(999)))
                .and(MARKET_DATA.MODIFIED.eq(Timestamp.valueOf(now)))
                .and(MARKET_DATA.DATE.eq(Timestamp.valueOf(now)))
                .and(MARKET_DATA.REALTIMECLOSE.eq(true))
                .fetch();
        assertEquals(1, fetchedResults.size());
    }

    @Test
    public void batchInsertTest(){
        LocalDateTime now = LocalDateTime.now();
        List<DailyData> batch =
                IntStream.range(0, 100)
                        .mapToObj(i ->
                                DailyData.builder()
                                        .ticker(TICKER)
                                        .date(Timestamp.valueOf(now.minusMinutes(i)))
                                        .build())
                .collect(Collectors.toList());

        persistence.batchInsert(batch);
        Result<MarketDataRecord> fetchedResults = getJooq().selectFrom(MARKET_DATA).fetch();
        assertEquals(100, fetchedResults.size());
    }

    @Test
    public void findLatestNonRealtimeCloseTest(){
        DailyData realtimeClose = DailyData.builder().ticker(TICKER)
                .date(Timestamp.valueOf(now)).realtimeClose(true).build();

        Timestamp latestNonRealtimeClose = persistence.findLatestNonRealtimeClose(TICKER);
        assertNull(latestNonRealtimeClose);

        DailyData close1 = DailyData.builder().ticker(TICKER)
                .date(Timestamp.valueOf(now.minusDays(1))).build();
        DailyData close2 = DailyData.builder().ticker(TICKER)
                .date(Timestamp.valueOf(now.minusDays(2))).build();

        persistence.batchInsert(asList(realtimeClose,close1, close2));
        latestNonRealtimeClose = persistence.findLatestNonRealtimeClose(TICKER);
        assertEquals(Timestamp.valueOf(now.minusDays(1)), latestNonRealtimeClose);
    }

    @Test
    public void findEarliestCloseTest(){
        DailyData realtimeClose = DailyData.builder().ticker(TICKER)
                .date(Timestamp.valueOf(now)).build();
        DailyData close1 = DailyData.builder().ticker(TICKER)
                .date(Timestamp.valueOf(now.minusDays(1))).build();
        DailyData close2 = DailyData.builder().ticker(TICKER)
                .date(Timestamp.valueOf(now.minusDays(2))).build();

        persistence.batchInsert(asList(realtimeClose,close1, close2));
        Timestamp earliestClose = persistence.findEarliestClose(TICKER);
        assertEquals(Timestamp.valueOf(now.minusDays(2)), earliestClose);
    }

    @Test
    public void persistUniqueDailyClosesTest(){
        DailyData realtimeClose = DailyData.builder()
                .ticker(TICKER)
                .date(Timestamp.valueOf(now.minusDays(1)))
                .realtimeClose(true)
                .modified(Timestamp.valueOf(now.minusDays(1)))
                .build();
        DailyData previousClose = DailyData.builder()
                .ticker(TICKER)
                .date(Timestamp.valueOf(now.minusDays(2)))
                .modified(Timestamp.valueOf(now.minusDays(2)))
                .build();
        persistence.batchInsert(asList(realtimeClose,previousClose));

        List<DailyData> simulatedDownloadedData = asList(
                DailyData.builder().ticker(TICKER).date(Timestamp.valueOf(now.minusDays(1)))
                        .modified(Timestamp.valueOf(now)).build(),
                DailyData.builder().ticker(TICKER).date(Timestamp.valueOf(now.minusDays(2)))
                        .modified(Timestamp.valueOf(now)).build(),
                DailyData.builder().ticker(TICKER).date(Timestamp.valueOf(now.minusDays(3)))
                        .modified(Timestamp.valueOf(now)).build()
        );
        persistence.persistUniqueDailyCloses(simulatedDownloadedData);

        Result<MarketDataRecord> fetchedResults = getJooq().selectFrom(MARKET_DATA).orderBy(MARKET_DATA.DATE.asc()).fetch();
        assertEquals(3, fetchedResults.size());

        // where in insert into .. onDuplicateKeyUpdate does not work for HSQLDB
        LocalDateTime notModified = getJooq().dialect() == SQLDialect.HSQLDB ? now : now.minusDays(2);

        List<Timestamp> modifiedTimestamps = fetchedResults.getValues(MARKET_DATA.MODIFIED);
        assertEquals(Timestamp.valueOf(now), modifiedTimestamps.get(0));
        assertEquals(Timestamp.valueOf(notModified), modifiedTimestamps.get(1));
        assertEquals(Timestamp.valueOf(now), modifiedTimestamps.get(2));

        MarketDataRecord overwrittenRealtimeClose = fetchedResults.get(2);
        assertFalse(overwrittenRealtimeClose.getRealtimeclose());
    }

    @Test
    public void insertRealtimeClosesTest(){
        List<DailyData> historicalCloses = asList(
            data(true, 0, now.minusDays(1)),
            data(false, 10, now.minusDays(2))
        );
        persistence.batchInsert(historicalCloses);

        List<DailyData> simulatedDownloadedData = singletonList(
            data(true,1, now)
        );
        persistence.persistRealtimeCloses(simulatedDownloadedData);

        Result<MarketDataRecord> fetchedResults = getJooq()
            .selectFrom(MARKET_DATA)
            .orderBy(MARKET_DATA.DATE.desc())
            .fetch();

        assertSame(fetchedResults, concat(simulatedDownloadedData, historicalCloses));
    }

    @Test
    public void updateRealtimeClosesTest(){
        DailyData realtimeClose = data(true, 0, now);
        DailyData previousClose = data(false, 1, now.minusDays(1));
        DailyData prevPrevClose = data(false, 10, now.minusDays(2));
        persistence.batchInsert(asList(realtimeClose, previousClose, prevPrevClose));

        List<DailyData> simulatedDownloadedData = singletonList(
            data(true,10, now)
        );
        persistence.persistRealtimeCloses(simulatedDownloadedData);

        Result<MarketDataRecord> fetchedResults = getJooq()
            .selectFrom(MARKET_DATA)
            .orderBy(MARKET_DATA.DATE.desc())
            .fetch();

        assertSame(fetchedResults, concat(simulatedDownloadedData, asList(previousClose, prevPrevClose)));
    }

    private DailyData data(Boolean realtime, Integer close, LocalDateTime dateTime) {
        return DailyData.builder().ticker(TICKER).date(Timestamp.valueOf(dateTime))
            .modified(Timestamp.valueOf(dateTime)).close(BigDecimal.valueOf(close))
            .realtimeClose(realtime).build();
    }

    private void assertSame(Result<MarketDataRecord> fetchedResults, List<DailyData> dailyData) {
        assertEquals(fetchedResults.size(), dailyData.size());
        IntStream.range(0, fetchedResults.size()).forEach(index -> {
            assertEquals(fetchedResults.get(index).getModified(), dailyData.get(index).getModified());
            assertEquals(fetchedResults.get(index).getRealtimeclose(), dailyData.get(index).isRealtimeClose());
            assertEquals(fetchedResults.get(index).getClose().intValue(), dailyData.get(index).getClose().intValue());
        });
    }

    private <T> List<T> concat(List<T> head, List<T> tail) {
        List<T> result = new ArrayList<>(head);
        result.addAll(tail);
        return result;
    }

    private DSLContext getJooq() {
        return testDbRule.getJooqContext();
    }
}
