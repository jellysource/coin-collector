package com.jellysource.quotecenter.dataservice.quandl;

public enum QuandlResultColumn {
	OPEN(1),
	HIGH(2),
	LOW(3),
	CLOSE(4),
	DATE(0);
	
	int ordinalValue;
	
	QuandlResultColumn(int ordinalValue) {
		this.ordinalValue = ordinalValue;
	}
	
	public int getColumnNumber(){
		return this.ordinalValue;
	}

}
