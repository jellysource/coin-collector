package com.jellysource.quotecenter.dataservice.quandl;

import com.jellysource.quotecenter.Pair;
import com.jellysource.quotecenter.dataservice.DataService;
import com.jellysource.quotecenter.dataservice.HistoricDataService;
import com.jellysource.quotecenter.persistence.Persistence;
import com.jellysource.quotecenter.persistence.entity.DailyData;
import com.jimmoores.quandl.DataSetRequest;
import com.jimmoores.quandl.Row;
import com.jimmoores.quandl.TabularResult;
import com.jimmoores.quandl.classic.ClassicQuandlSession;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.subjects.ReplaySubject;
import io.reactivex.subjects.Subject;
import org.threeten.bp.LocalDate;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.StreamSupport;

import static com.jellysource.quotecenter.dataservice.quandl.DateUtils.toQuandlDate;
import static org.threeten.bp.LocalDate.of;

/**
 * Created by admin on 7/9/2017.
 */

public class QuandlDataService implements DataService, HistoricDataService {

    private Persistence persistenceService;
    private ClassicQuandlSession quandlSession;

    public QuandlDataService(Persistence persistenceService, ClassicQuandlSession quandlSession) {
        this.persistenceService = persistenceService;
        this.quandlSession = quandlSession;
    }

    @Override
    public Subject<List<DailyData>> downloadHistoricData(List<String> tickers) {
        Subject<List<DailyData>> dataRecordSubject = ReplaySubject.create();

        downloadDataFromQuandl(tickers)
            .subscribe(list -> Observable
                .concat(
                    IntStream.range(0, Math.min(list.size(), tickers.size()))
                        .mapToObj(i ->
                            Observable
                                .fromIterable(list.get(i))
                                .take(20)
                                .map(row -> convertRowToDataRecord(row, tickers.get(i)))
                        ).collect(Collectors.toList()))
                .toList()
                .subscribe(dataRecordSubject::onNext));
        return dataRecordSubject;
    }

    private DailyData convertRowToDataRecord(Row row, String ticker) {
        LocalDate localDate = row.getLocalDate(QuandlResultColumn.DATE.getColumnNumber());
        LocalDateTime localDateTime = LocalDateTime.of(localDate.getYear(), localDate.getMonthValue(),
                localDate.getDayOfMonth(), 22, 15);

        return DailyData.builder()
                .ticker(ticker)
                .open(BigDecimal.valueOf(row.getDouble(QuandlResultColumn.OPEN.getColumnNumber())))
                .high(BigDecimal.valueOf(row.getDouble(QuandlResultColumn.HIGH.getColumnNumber())))
                .low(BigDecimal.valueOf(row.getDouble(QuandlResultColumn.LOW.getColumnNumber())))
                .close(BigDecimal.valueOf(row.getDouble(QuandlResultColumn.CLOSE.getColumnNumber())))
                .date(Timestamp.valueOf(localDateTime))
                .modified(Timestamp.valueOf(LocalDateTime.now()))
                .build();
    }

    private Maybe<List<TabularResult>> downloadDataFromQuandl(List<String> tickers) {
        long barsBackWithWeekendsAndHolidays = 252;
        Timestamp barsBackTimestamp = Timestamp.valueOf(LocalDateTime.now().minusDays(barsBackWithWeekendsAndHolidays));

        return Maybe.just(tickers.stream().map(ticker -> {
            DataSetRequest.Builder builder = DataSetRequest.Builder.of("EOD/" + ticker);
            Maybe.fromCallable(() -> persistenceService.findLatestNonRealtimeClose(ticker))
                    .subscribe(latestTimestamp -> {
                                if (latestTimestamp.compareTo(barsBackTimestamp) >= 0) {
                                    LocalDateTime time = LocalDateTime.ofInstant(latestTimestamp.toInstant(), ZoneId.systemDefault());
                                    builder.withStartDate(of(time.getYear(), time.getMonthValue(), time.getDayOfMonth()).plusDays(1))
                                            .withEndDate(LocalDate.now().minusDays(1));
                                } else {
                                    builder.withStartDate(LocalDate.now().minusDays(barsBackWithWeekendsAndHolidays))
                                            .withEndDate(LocalDate.now().minusDays(1));
                                }
                            },
                            err -> {
                            },
                            () -> {
                                builder.withStartDate(LocalDate.now().minusDays(barsBackWithWeekendsAndHolidays))
                                        .withEndDate(LocalDate.now().minusDays(1));
                            });
            return quandlSession.getDataSet(builder.build());
        }).collect(Collectors.toList()));
    }

    @Override
    public Subject<Map<String, List<DailyData>>> downloadHistoricData(
        List<String> tickers,
        java.time.LocalDate from,
        java.time.LocalDate to) {

        Subject<Map<String, List<DailyData>>> dataRecordSubject = ReplaySubject.create();
        downloadDataFromQuandl(tickers, from, to)
            .subscribe(dataRecordSubject::onNext);
        return dataRecordSubject;
    }

    private Maybe<Map<String, List<DailyData>>> downloadDataFromQuandl(
        List<String> tickers,
        java.time.LocalDate from,
        java.time.LocalDate to) {

        return Maybe.just(tickers.stream().map(ticker -> {
            DataSetRequest.Builder builder = DataSetRequest.Builder.of("WIKI/" + ticker);
            builder
                .withStartDate(toQuandlDate(from))
                .withEndDate(toQuandlDate(to));
            return Pair.of(ticker, toDailyData(ticker, quandlSession.getDataSet(builder.build())));
        }).collect(Collectors.toMap(Pair::getKey, Pair::getValue)));
    }

    private List<DailyData> toDailyData(String ticker, TabularResult tabularResult) {
        return StreamSupport
            .stream(tabularResult.spliterator(), false)
            .map(row -> convertRowToDataRecord(row, ticker))
            .collect(Collectors.toList());
    }

    @Override
    public Subject<DailyData> downloadRealTimeClose(String ticker) {
        //will not implement for the time being, Quandl will not be used as a production data source
        return ReplaySubject.create();
    }

    @Override
    public Subject<List<DailyData>> downloadRealTimeCloses(List<String> tickers) {
        return null;
    }
}
