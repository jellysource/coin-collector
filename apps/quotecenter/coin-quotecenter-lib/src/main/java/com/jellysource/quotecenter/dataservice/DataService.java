package com.jellysource.quotecenter.dataservice;

import com.jellysource.quotecenter.persistence.entity.DailyData;
import io.reactivex.subjects.Subject;

import java.util.List;

public interface DataService {

    Subject<List<DailyData>> downloadHistoricData(List<String> tickers);
    Subject<DailyData> downloadRealTimeClose(String ticker);
    Subject<List<DailyData>> downloadRealTimeCloses(List<String> tickers);


}
