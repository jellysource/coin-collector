package com.jellysource.quotecenter.dataservice;

import com.jellysource.quotecenter.persistence.entity.DailyData;
import io.reactivex.subjects.Subject;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

public interface HistoricDataService {

  Subject<Map<String, List<DailyData>>> downloadHistoricData(
      List<String> tickers,
      LocalDate from,
      LocalDate to);
}
