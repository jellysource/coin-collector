package com.jellysource.quotecenter.persistence.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.math.BigDecimal;
import java.sql.Timestamp;

@Getter
@Builder(toBuilder = true)
@ToString
public class DailyData {
    private Long id;
    private String ticker;
    private BigDecimal open;
    private BigDecimal high;
    private BigDecimal low;
    private BigDecimal close;
    @Builder.Default
    private boolean realtimeClose = false;
    private Timestamp date;
    private Timestamp modified;
}
