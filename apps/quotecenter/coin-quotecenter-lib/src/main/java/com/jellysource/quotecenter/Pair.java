package com.jellysource.quotecenter;

import lombok.Value;

import java.util.Map;

@Value
public class Pair<L, R> implements Map.Entry<L, R> {
    L left;
    R right;

    public static <L, R> Pair<L, R> of(L left, R right) {
        return new Pair<>(left, right);
    }

    @Override
    public L getKey() {
        return getLeft();
    }

    @Override
    public R getValue() {
        return getRight();
    }

    @Override
    public R setValue(R value) {
        throw new UnsupportedOperationException();
    }
}