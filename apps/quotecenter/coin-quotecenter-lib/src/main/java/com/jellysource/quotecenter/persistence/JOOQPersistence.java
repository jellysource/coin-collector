package com.jellysource.quotecenter.persistence;

import com.jellysource.coin.db.Tables;
import com.jellysource.coin.db.tables.MarketData;
import com.jellysource.coin.db.tables.records.MarketDataRecord;
import com.jellysource.quotecenter.persistence.entity.DailyData;
import org.jooq.DSLContext;
import org.jooq.InsertOnDuplicateStep;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

import static org.jooq.impl.DSL.max;
import static org.jooq.impl.DSL.min;

public class JOOQPersistence implements Persistence {

    private DSLContext jooq;
    private MarketData DATA = Tables.MARKET_DATA;

    public JOOQPersistence(DSLContext jooq) {
        this.jooq = jooq;
    }

    @Override
    public Timestamp findLatestNonRealtimeClose(String ticker) {
        return (Timestamp) jooq
                .select(max(DATA.DATE))
                .from(DATA)
                .where(DATA.TICKER.eq(ticker))
                .and(DATA.REALTIMECLOSE.eq(Boolean.FALSE))
                .fetch()
                .getValue(0, 0);
    }

    @Override
    public Timestamp findEarliestClose(String ticker) {
        return (Timestamp) jooq
                .select(min(DATA.DATE))
                .from(DATA)
                .where(DATA.TICKER.eq(ticker))
                .fetch()
                .getValue(0, 0);
    }

    @Override
    public void batchInsert(List<DailyData> list) {
        List<MarketDataRecord> convertedList = list.stream()
                .map(dailyData -> {
                    MarketDataRecord dataRecord = new MarketDataRecord(
                                    dailyData.getTicker(),
                                    dailyData.getOpen(),
                                    dailyData.getHigh(),
                                    dailyData.getLow(),
                                    dailyData.getClose(),
                                    dailyData.isRealtimeClose(),
                                    dailyData.getDate(),
                                    dailyData.getModified());
                            dataRecord.changed(true);
                            return dataRecord;
                        }
                ).collect(Collectors.toList());
        jooq.batchInsert(convertedList).execute();
    }

    @Override
    public void persistUniqueDailyCloses(List<DailyData> list) {
        list.forEach(dailyData -> insertValuesIntoColumns(dailyData)
                .onDuplicateKeyUpdate()
                .set(DATA.REALTIMECLOSE, false)
                .set(DATA.CLOSE, dailyData.getClose())
                .set(DATA.MODIFIED, dailyData.getModified())
                .where(DATA.REALTIMECLOSE)
                .execute());
    }

    @Override
    public void persistRealtimeCloses(List<DailyData> list) {
        list.forEach(dailyData -> insertValuesIntoColumns(dailyData)
                .onDuplicateKeyUpdate()
                .set(DATA.CLOSE, dailyData.getClose())
                .set(DATA.MODIFIED, dailyData.getModified())
                .where(DATA.REALTIMECLOSE)
                .execute());
    }

    @Override
    public void persistSingleDataRecord(DailyData dailyData) {
        insertValuesIntoColumns(dailyData).execute();
    }

    private InsertOnDuplicateStep<MarketDataRecord> insertValuesIntoColumns(DailyData dailyData) {
        return jooq
                .insertInto(DATA)
                .columns(
                        DATA.TICKER,
                        DATA.OPEN,
                        DATA.HIGH,
                        DATA.LOW,
                        DATA.CLOSE,
                        DATA.DATE,
                        DATA.MODIFIED,
                        DATA.REALTIMECLOSE)
                .values(
                        dailyData.getTicker(),
                        dailyData.getOpen(),
                        dailyData.getHigh(),
                        dailyData.getLow(),
                        dailyData.getClose(),
                        dailyData.getDate(),
                        dailyData.getModified(),
                        dailyData.isRealtimeClose());
    }

}