package com.jellysource.quotecenter.persistence;

import com.jellysource.quotecenter.persistence.entity.DailyData;

import java.sql.Timestamp;
import java.util.List;

public interface Persistence {

    Timestamp findLatestNonRealtimeClose(String ticker);

    Timestamp findEarliestClose(String ticker);

    void persistUniqueDailyCloses(List<DailyData> list);

    void persistSingleDataRecord(DailyData dailyData);

    void persistRealtimeCloses(List<DailyData> list);

    void batchInsert(List<DailyData> list);

}
