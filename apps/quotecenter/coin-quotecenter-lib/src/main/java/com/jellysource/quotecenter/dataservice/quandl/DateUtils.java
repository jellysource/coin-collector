package com.jellysource.quotecenter.dataservice.quandl;

import java.time.LocalDate;

public class DateUtils {

  public static org.threeten.bp.LocalDate toQuandlDate(LocalDate date) {
    return org.threeten.bp.LocalDate.of(
        date.getYear(),
        date.getMonthValue(),
        date.getDayOfMonth());
  }
}
