package com.jellysource.quotecenter.dataservice.IEX;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.jellysource.quotecenter.Pair;
import com.jellysource.quotecenter.dataservice.DataService;
import com.jellysource.quotecenter.persistence.Persistence;
import com.jellysource.quotecenter.persistence.entity.DailyData;
import io.reactivex.subjects.ReplaySubject;
import io.reactivex.subjects.Subject;
import lombok.SneakyThrows;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.*;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class IEXDataService implements DataService {

    private Persistence persistenceService;
    private ObjectMapper objectMapper;
    private RetryTemplate retryTemplate;
    private RestTemplate restTemplate;

    private LocalDate today = LocalDate.now();

    public IEXDataService(Persistence persistenceService, ObjectMapper objectMapper, RetryTemplate retryTemplate, RestTemplate restTemplate) {
        this.persistenceService = persistenceService;
        this.objectMapper = objectMapper;
        this.retryTemplate = retryTemplate;
        this.restTemplate = restTemplate;
    }

    @Override
    @SneakyThrows
    public Subject<List<DailyData>> downloadHistoricData(List<String> tickers) {
        Map<String, Pair<Timestamp, Timestamp>> latestTimestamps = tickers.stream()
                .collect(Collectors.toMap(Function.identity(),
                        ticker -> Pair.of(persistenceService.findEarliestClose(ticker),
                                persistenceService.findLatestNonRealtimeClose(ticker))));
        List<String> noHistoryYet = latestTimestamps.entrySet().stream()
                .filter(entry -> entry.getValue().getLeft() == null &&
                        entry.getValue().getRight() == null)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
        Map<String, Pair<Timestamp, Timestamp>> someHistoryExists = latestTimestamps.entrySet().stream()
                .filter(entry -> entry.getValue().getLeft() != null &&
                        entry.getValue().getRight() != null)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        List<String> notEnoughHistory = someHistoryExists.entrySet().stream()
                .filter(entry -> entry.getValue().getLeft()
                        .toLocalDateTime()
                        .toLocalDate()
                        .compareTo(LocalDate.now().minusYears(1)) > 0)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
        Map<String, Timestamp> enoughHistory = someHistoryExists.entrySet().stream()
                .filter(entry -> entry.getValue().getLeft()
                        .toLocalDateTime()
                        .toLocalDate()
                        .compareTo(LocalDate.now().minusYears(1)) <= 0)
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().getRight()));

        Subject<List<DailyData>> subject = ReplaySubject.create();

        Optional.of(noHistoryYet).filter(list -> list.size() > 0)
                .ifPresent(list -> subject.onNext(downloadTickers(list, "1y")));
        Optional.of(notEnoughHistory).filter(list -> list.size() > 0)
                .ifPresent(list -> subject.onNext(downloadTickers(list, "1y")));
        Optional.of(enoughHistory)
                .filter(map -> map.size() > 0)
                .ifPresent(map -> map.entrySet()
                        .stream()
                        .collect(Collectors.groupingBy(this::determineIntervalLength,
                                Collectors.mapping(Map.Entry::getKey, Collectors.toList())))
                        .forEach((interval, tickersList) -> subject.onNext(downloadTickers(tickersList, interval))));
        return subject;
    }

    @Override
    @SneakyThrows
    public Subject<DailyData> downloadRealTimeClose(String ticker) {
        StringBuilder sb = new StringBuilder();
        sb.append("https://api.iextrading.com/1.0/stock/");
        sb.append(ticker);
        sb.append("/quote?filter=latestPrice");

        String responseJSON = makeHTTPRequestWithRetry(sb);

        IEXRealtimeClose realtimeClose = objectMapper.readValue(responseJSON, IEXRealtimeClose.class);
        DailyData dailyClose = DailyData.builder()
                .ticker(ticker)
                .date(Timestamp.valueOf(LocalDate.now().atStartOfDay()))
                .close(BigDecimal.valueOf(realtimeClose.getLatestPrice()))
                .realtimeClose(true)
                .modified(Timestamp.valueOf(LocalDateTime.now()))
                .build();
        Subject<DailyData> subject = ReplaySubject.create();
        subject.onNext(dailyClose);
        return subject;
    }

    @Override
    @SneakyThrows
    public Subject<List<DailyData>> downloadRealTimeCloses(List<String> tickers) {

        StringBuilder sb = composeURL(tickers);
        sb.append("&types=quote");
        sb.append("&filter=latestPrice,latestTime");

        String responseJSON = makeHTTPRequestWithRetry(sb);

        JavaType javaType = TypeFactory.defaultInstance().constructType(IEXRealtimeClose.class);
        JavaType stringType = TypeFactory.defaultInstance().constructType(String.class);
        javaType = TypeFactory.defaultInstance().constructMapType(HashMap.class, stringType, javaType);
        javaType = TypeFactory.defaultInstance().constructMapType(HashMap.class, stringType, javaType);
        Map<String, Object> json = objectMapper.readValue(responseJSON, javaType);

        List<DailyData> result = json.entrySet().stream().map(entry -> {
            IEXRealtimeClose realtimeClose = ((Map<String, IEXRealtimeClose>) entry.getValue()).get("quote");
            LocalDateTime date;
            Pattern pattern = Pattern.compile("\\d{1,2}:\\d{1,2}:\\d{1,2}\\s(AM|PM)");

            if (pattern.matcher(realtimeClose.getLatestTime()).matches()) {
                date = LocalDate.now().atStartOfDay();
            } else {
                String[] dateString = realtimeClose.getLatestTime().replace(",", "").split("\\s");
                date = LocalDate.of(Integer.parseInt(dateString[2]),
                        Month.valueOf(dateString[0].toUpperCase()),
                        Integer.parseInt(dateString[1])).atStartOfDay();
            }

            return DailyData.builder()
                    .ticker(entry.getKey())
                    .date(Timestamp.valueOf(date))
                    .close(BigDecimal.valueOf(realtimeClose.getLatestPrice()))
                    .realtimeClose(true)
                    .modified(Timestamp.valueOf(LocalDateTime.now()))
                    .build();
        }).collect(Collectors.toList());

        Subject<List<DailyData>> subject = ReplaySubject.create();
        subject.onNext(result);
        return subject;
    }

    @SneakyThrows
    private List<DailyData> downloadTickers(List<String> tickers, String timeInterval) {
        StringBuilder sb = composeURL(tickers);
        sb.append("&types=chart&range=");
        sb.append(timeInterval);
        sb.append("&filter=date,close");

        String responseJSON = makeHTTPRequestWithRetry(sb);

        List<DailyData> result = new ArrayList<>();

        JavaType javaType = TypeFactory.defaultInstance().constructCollectionType(ArrayList.class, IEXDailyCLose.class);
        JavaType stringType = TypeFactory.defaultInstance().constructType(String.class);
        javaType = TypeFactory.defaultInstance().constructMapType(HashMap.class, stringType, javaType);
        javaType = TypeFactory.defaultInstance().constructMapType(HashMap.class, stringType, javaType);
        Map<String, Object> json = objectMapper.readValue(responseJSON, javaType);

        json.forEach((ticker, values) -> {
            List<IEXDailyCLose> dailyCloses = ((Map<String, List>) values).get("chart");
            dailyCloses.forEach(iexDailyCLose -> {
                DailyData dailyClose = DailyData.builder()
                        .ticker(ticker)
                        .date(Timestamp.valueOf(iexDailyCLose.getDate().atStartOfDay()))
                        .close(BigDecimal.valueOf(iexDailyCLose.getClose()))
                        .modified(Timestamp.valueOf(LocalDateTime.now()))
                        .build();
                result.add(dailyClose);
            });
        });

        return result;
    }

    private String makeHTTPRequestWithRetry(StringBuilder sb) {
        return retryTemplate.execute(retryContext -> {
            ResponseEntity<String> response = restTemplate.getForEntity(sb.toString(), String.class);
            if (!response.getStatusCode().equals(HttpStatus.OK)) {
                throw new RuntimeException("IEX server unreachable.");
            }
            return response.getBody();
        });
    }

    private StringBuilder composeURL(List<String> tickers) {
        StringBuilder sb = new StringBuilder();
        sb.append("https://api.iextrading.com/1.0/stock/market/batch?symbols=");

        IntStream.range(0, tickers.size()).forEach(i -> {
            sb.append(tickers.get(i));
            if (i != tickers.size() - 1)
                sb.append(",");
        });

        return sb;
    }

    private String determineIntervalLength(Map.Entry<String, Timestamp> entry) {
        LocalDate latestDate = entry
                .getValue().toLocalDateTime().toLocalDate();
        if (today.minusMonths(1).compareTo(latestDate) <= 0) {
            return "1m";
        } else if (today.minusMonths(3).compareTo(latestDate) <= 0
                && today.minusMonths(1).compareTo(latestDate) > 0) {
            return "3m";
        } else if (today.minusMonths(6).compareTo(latestDate) <= 0
                && today.minusMonths(3).compareTo(latestDate) > 0) {
            return "6m";
        } else {
            return "1y";
        }
    }
}