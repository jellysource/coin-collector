package com.jellysource.quotecenter.dataservice.IEX;

import lombok.Getter;

import java.time.LocalDate;

@Getter
public class IEXRealtimeClose {
    private double latestPrice;
    private String latestTime;
}
