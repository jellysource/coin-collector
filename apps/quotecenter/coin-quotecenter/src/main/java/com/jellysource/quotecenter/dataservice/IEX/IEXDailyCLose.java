package com.jellysource.quotecenter.dataservice.IEX;

import lombok.Getter;

import java.time.LocalDate;

@Getter
public class IEXDailyCLose {

    private LocalDate date;
    private double close;
}
