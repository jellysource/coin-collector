package com.jellysource.quotecenter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuotecenterApplication {

	public static void main(String[] args) {
		SpringApplication.run(QuotecenterApplication.class, args);
	}

}
