package com.jellysource.quotecenter.scheduled;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.jellysource.quotecenter.dataservice.DataService;
import com.jellysource.quotecenter.persistence.Persistence;
import io.reactivex.subjects.ReplaySubject;
import io.reactivex.subjects.Subject;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Component
public class ScheduledTasks {

    private static final Logger LOG = LoggerFactory.getLogger(ScheduledTasks.class);

    @Autowired
    DataService dataService;

    @Autowired
    private Persistence persistenceService;

    @Autowired
    ObjectMapper objectMapper;

    //second, minute, hour, day, month, weekday
    //@Scheduled(cron = "0 0 10 * * ?") // for production deployment
    @Scheduled(fixedRate = 50000) //only for development
    public void downloadHistoryAndPersist() {
        LOG.info("Downloading history");
        readTickersFromFile()
                .map(dataService::downloadHistoricData)
                .flatMap(list -> list)
                .subscribe(persistenceService::persistUniqueDailyCloses);
    }

    //@Scheduled(cron = "0 0 22 * * ?") // for production deployment
    @Scheduled(fixedRate = 50000) //only for development
    public void downloadRealtimeAndPersist() {
        LOG.info("Downloading realtime data");
        readTickersFromFile()
                .map(dataService::downloadRealTimeCloses)
                .flatMap(list -> list)
                .subscribe(persistenceService::persistRealtimeCloses);
    }

    @SneakyThrows
    private Subject<List<String>> readTickersFromFile() {
        Subject<List<String>> replaySubject = ReplaySubject.create();
        ClassLoader classLoader = getClass().getClassLoader();
        InputStream tickersFile = classLoader.getResourceAsStream("tickers.json");
        JavaType javaType = TypeFactory.defaultInstance().constructCollectionType(ArrayList.class, String.class);
        List<String> parsedTickers = objectMapper.readValue(tickersFile, javaType);
        replaySubject.onNext(parsedTickers);
        return replaySubject;
    }
}
