package com.jellysource.quotecenter.dataservice.IEX;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jellysource.quotecenter.Pair;
import com.jellysource.quotecenter.persistence.Persistence;
import com.jellysource.quotecenter.persistence.entity.DailyData;
import io.reactivex.observers.TestObserver;
import io.reactivex.subjects.Subject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.powermock.reflect.Whitebox;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.powermock.api.mockito.PowerMockito.doReturn;


@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(SpringRunner.class)
@PrepareForTest(IEXDataService.class)
@SpringBootTest
public class IEXDataServiceTest {

    //https://stackoverflow.com/questions/34067956/how-to-use-injectmocks-along-with-autowired-annotation-in-junit

    @MockBean
    private Persistence persistenceService;

    @MockBean
    private DataSource dataSource;

    @Mock
    private RetryTemplate retryTemplate;

    @Autowired
    @InjectMocks
    IEXDataService iexDataService;

    @Autowired
    private ApplicationContext applicationContext;

    @Test
    public void downloadHistoricDataTest() throws Exception {
        //no history
        Mockito.doReturn(null)
                .when(persistenceService).findLatestNonRealtimeClose("AAPL");
        Mockito.doReturn(null)
                .when(persistenceService).findEarliestClose("AAPL");
        //not enough history
        Mockito.doReturn(Timestamp.valueOf(LocalDate.now().atStartOfDay()))
                .when(persistenceService).findLatestNonRealtimeClose("CSCO");
        Mockito.doReturn(Timestamp.valueOf(LocalDate.now().minusMonths(1).atStartOfDay()))
                .when(persistenceService).findEarliestClose("CSCO");
        //enouugh history
        Mockito.doReturn(Timestamp.valueOf(LocalDate.now().atStartOfDay()))
                .when(persistenceService).findLatestNonRealtimeClose("KO");
        Mockito.doReturn(Timestamp.valueOf(LocalDate.now().minusYears(1).atStartOfDay()))
                .when(persistenceService).findEarliestClose("KO");

        IEXDataService mockService = PowerMockito.spy(new IEXDataService(persistenceService, null,
                null, null));
        List<DailyData> downloadTickersReturnList = new ArrayList<>();
        downloadTickersReturnList.add(DailyData.builder().build());
        doReturn(downloadTickersReturnList).when(mockService, "downloadTickers",
                Mockito.any(List.class), Mockito.any(String.class));

        List<String> tickers = Arrays.asList("AAPL", "CSCO", "KO");
        Subject<List<DailyData>> subject = mockService.downloadHistoricData(tickers);

        TestObserver<List<DailyData>> testObserver = new TestObserver<>();
        subject.subscribe(testObserver);

        testObserver.awaitCount(3);
        testObserver.assertNoErrors();
        testObserver.assertSubscribed();
        testObserver.assertNotTerminated();
        testObserver.assertValueCount(3);
    }

    @Test
    public void downloadRealTimeClosesAfterCloseTest() throws Exception {
        ObjectMapper objectMapper = applicationContext.getBean(ObjectMapper.class);
        IEXDataService mockService = PowerMockito.spy(new IEXDataService(null, objectMapper,
                null, null));
        String returnJson =
                "{\"AAPL\":{\"quote\":{\"latestPrice\":178.02,\"latestTime\":\"March 16, 2018\"}}," +
                "\"CSCO\":{\"quote\":{\"latestPrice\":45.01,\"latestTime\":\"March 16, 2018\"}}," +
                "\"KO\":{\"quote\":{\"latestPrice\":43.46,\"latestTime\":\"March 16, 2018\"}}}";
        doReturn(returnJson).when(mockService, "makeHTTPRequestWithRetry", Mockito.any(StringBuilder.class));

        List<String> tickers = Arrays.asList("AAPL", "CSCO", "KO");
        Subject<List<DailyData>> subject = mockService.downloadRealTimeCloses(tickers);

        TestObserver<List<DailyData>> testObserver = new TestObserver<>();
        subject.subscribe(testObserver);

        testObserver.awaitCount(1);
        testObserver.assertNoErrors();
        testObserver.assertSubscribed();
        testObserver.assertNotTerminated();
        testObserver.assertValueCount(1);
        List<DailyData> actual = testObserver.values().stream().flatMap(List::stream).collect(Collectors.toList());
        DailyData AAPL = actual.get(0);
        DailyData CSCO = actual.get(1);
        DailyData KO = actual.get(2);

        assertEquals(BigDecimal.valueOf(178.02), AAPL.getClose());
        assertEquals(BigDecimal.valueOf(45.01), CSCO.getClose());
        assertEquals(BigDecimal.valueOf(43.46), KO.getClose());
        assertEquals(Timestamp.valueOf(LocalDate.of(2018,3,16).atStartOfDay()), AAPL.getDate());
        assertEquals(Timestamp.valueOf(LocalDate.of(2018,3,16).atStartOfDay()), CSCO.getDate());
        assertEquals(Timestamp.valueOf(LocalDate.of(2018,3,16).atStartOfDay()), KO.getDate());
    }

    @Test
    public void downloadRealTimeClosesBeforeCloseTest() throws Exception {
        ObjectMapper objectMapper = applicationContext.getBean(ObjectMapper.class);
        IEXDataService mockService = PowerMockito.spy(new IEXDataService(null, objectMapper,
                null, null));
        String returnJson =
                "{\"AAPL\":{\"quote\":{\"latestPrice\":178.02,\"latestTime\":\"15:05:00 PM\"}}," +
                        "\"CSCO\":{\"quote\":{\"latestPrice\":45.01,\"latestTime\":\"22:05:00 AM\"}}," +
                        "\"KO\":{\"quote\":{\"latestPrice\":43.46,\"latestTime\":\"12:34:00 PM\"}}}";
        doReturn(returnJson).when(mockService, "makeHTTPRequestWithRetry", Mockito.any(StringBuilder.class));

        List<String> tickers = Arrays.asList("AAPL", "CSCO", "KO");
        Subject<List<DailyData>> subject = mockService.downloadRealTimeCloses(tickers);

        TestObserver<List<DailyData>> testObserver = new TestObserver<>();
        subject.subscribe(testObserver);

        testObserver.awaitCount(1);
        testObserver.assertNoErrors();
        testObserver.assertSubscribed();
        testObserver.assertNotTerminated();
        testObserver.assertValueCount(1);
        List<DailyData> actual = testObserver.values().stream().flatMap(List::stream).collect(Collectors.toList());
        DailyData AAPL = actual.get(0);
        DailyData CSCO = actual.get(1);
        DailyData KO = actual.get(2);

        assertEquals(BigDecimal.valueOf(178.02), AAPL.getClose());
        assertEquals(BigDecimal.valueOf(45.01), CSCO.getClose());
        assertEquals(BigDecimal.valueOf(43.46), KO.getClose());
        assertEquals(Timestamp.valueOf(LocalDate.now().atStartOfDay()), AAPL.getDate());
        assertEquals(Timestamp.valueOf(LocalDate.now().atStartOfDay()), CSCO.getDate());
        assertEquals(Timestamp.valueOf(LocalDate.now().atStartOfDay()), KO.getDate());
    }


    @Test
    public void composeURLTest() throws Exception {
        List<String> tickers = Arrays.asList("AAPL", "CSCO", "KO");
        StringBuilder sb = Whitebox.invokeMethod(iexDataService, "composeURL", tickers);
        assertEquals("https://api.iextrading.com/1.0/stock/market/batch?symbols=AAPL,CSCO,KO", sb.toString());
    }

    @Test
    public void determineIntervalLengthTest() {
        List<Pair<String, Timestamp>> list = new ArrayList<>();
        list.add(Pair.of("", Timestamp.valueOf(LocalDate.now().atStartOfDay())));
        list.add(Pair.of("", Timestamp.valueOf(LocalDate.now().minusDays(5).atStartOfDay())));
        list.add(Pair.of("", Timestamp.valueOf(LocalDate.now().minusMonths(1).atStartOfDay())));
        list.add(Pair.of("", Timestamp.valueOf(LocalDate.now().minusMonths(2).atStartOfDay())));
        list.add(Pair.of("", Timestamp.valueOf(LocalDate.now().minusMonths(3).atStartOfDay())));
        list.add(Pair.of("", Timestamp.valueOf(LocalDate.now().minusMonths(4).atStartOfDay())));
        list.add(Pair.of("", Timestamp.valueOf(LocalDate.now().minusMonths(7).atStartOfDay())));

        List<String> actual = list.stream()
                .map(entry -> {
                    try {
                        return Whitebox.invokeMethod(iexDataService, "determineIntervalLength", entry);
                    } catch (Exception e) {
                        e.printStackTrace();
                        return "";
                    }
                }).collect(Collectors.toList());

        List<String> expected = Arrays.asList("1m", "1m", "1m", "3m", "3m", "6m", "1y");
        assertEquals(expected, actual);
    }

    @Test
    public void downloadTickersOfflineTest() throws Exception {

        //sample URL :
        // https://api.iextrading.com/1.0/stock/market/batch?symbols=AAPL&types=chart&range=1m&filter=date,close

        ObjectMapper objectMapper = applicationContext.getBean(ObjectMapper.class);
        IEXDataService mockService = PowerMockito.spy(new IEXDataService(null, objectMapper,
                null, null));
        String returnJson = "{\"AAPL\":{\"chart\":[" +
                "{\"date\":\"2018-02-13\",\"close\":164.34}," +
                "{\"date\":\"2018-02-14\",\"close\":167.37}," +
                "{\"date\":\"2018-02-15\",\"close\":172.99}]}}";
        doReturn(returnJson).when(mockService, "makeHTTPRequestWithRetry", Mockito.any(StringBuilder.class));

        List<DailyData> result = Whitebox.invokeMethod(mockService, "downloadTickers", Arrays.asList("AAPL"), "1m");
        PowerMockito.verifyPrivate(mockService).invoke("downloadTickers",Arrays.asList("AAPL"), "1m");
        assertEquals(3, result.size());
    }


}
