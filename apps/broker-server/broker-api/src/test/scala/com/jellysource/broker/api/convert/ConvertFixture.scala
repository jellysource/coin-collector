package com.jellysource.broker.api.convert

import java.time.{LocalDateTime, OffsetDateTime, ZoneId, ZoneOffset}

import com.jellysource.broker.domain.{order => domain}
import com.jellysource.broker.api.protocol

trait ConvertFixture {

  lazy val FB = "FB"
  lazy val ID = "id"
  lazy val domainData = domain.OrderData(symbol = FB, quantity = 10, domain.Sell, execution = domain.Limit(200))
  lazy val protocolOrder = protocol.Order(id = ID, symbol = FB, quantity = 10, protocol.Sell, execution = protocol.Limit(200))
  lazy val now: LocalDateTime = LocalDateTime.now()
  lazy val nowAtOffset: OffsetDateTime = now.atZone(ZoneId.of("Europe/Bratislava")).toOffsetDateTime

  lazy val domainOrderCreated = domain.OrderCreated(id = ID, time = now, order = domainData)
  lazy val protocolOrderCreated = protocol.OrderCreated(
    id = ID,
    time = nowAtOffset,
    symbol = protocolOrder.symbol,
    quantity = protocolOrder.quantity,
    action = protocolOrder.action,
    execution = protocolOrder.execution
  )

  lazy val domainOrderPlaced = domain.OrderPlaced(id = ID, time = now)
  lazy val protocolOrderPlaced = protocol.OrderPlaced(ID, time = nowAtOffset)

  lazy val domainOrderFilledPartially = domain.OrderFilledPartially(id = ID, time = now, quantity = 10, price = 100)
  lazy val protocolOrderFilledPartially = protocol.OrderFilledPartially(
    id = ID,
    time = nowAtOffset,
    quantity = 10,
    price = 100
  )

  lazy val domainOrderFilled = domain.OrderFilled(id = ID, time = now, avgPrice = 100.01)
  lazy val protocolOrderFilled = protocol.OrderFilled(id = ID, time = nowAtOffset, price = 100.01)

  lazy val domainOrderCancelled = domain.OrderCancelled(id = ID, time = now)
  lazy val protocolOrderCancelled = protocol.OrderCancelled(id = ID, time = nowAtOffset)

}
