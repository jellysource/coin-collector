package com.jellysource.broker.api.publish
import java.time.OffsetDateTime

import com.jellysource.broker.api.protocol.OrderCancelled
import com.jellysource.broker.api.protocol.json.JsonSerDe
import org.scalatest.{FlatSpec, Matchers}

class OrderEventToMessageSpec extends FlatSpec with Matchers {

  "Order event to message" should "create amqp message with key in format {id}.{type}" in {
    OrderEventToMessage(event).routingKey shouldBe Some("1.cancelled")
  }

  it should "create amqp message containing event as json in bytes" in {
    JsonSerDe.deserializeOrderEvent(OrderEventToMessage(event).bytes.toArray) shouldBe event
  }

  it should "create not immediate amqp message" in {
    OrderEventToMessage(event).immediate shouldBe false
  }

  it should "create not mandatory amqp message" in {
    OrderEventToMessage(event).mandatory shouldBe false
  }

  private val event = OrderCancelled("1", OffsetDateTime.now())

}
