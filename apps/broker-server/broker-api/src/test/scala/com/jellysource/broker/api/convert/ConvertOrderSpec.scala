package com.jellysource.broker.api.convert

import org.scalatest.{FlatSpec, Matchers}

class ConvertOrderSpec extends FlatSpec with Matchers with ConvertFixture {

  "Convert order data" should "convert domain data to protocol data" in {
    ConvertOrder.toProtocol(ID, domainData) shouldBe protocolOrder
  }

  it should "convert protocol data to domain data" in {
    ConvertOrder.toDomain(protocolOrder) shouldBe domainData
  }

}
