package com.jellysource.broker.api.convert

import org.scalatest.{FlatSpec, Matchers}

import com.jellysource.broker.domain.{order => domain}
import com.jellysource.broker.api.protocol

class ConvertOrderCommandSpec extends FlatSpec with Matchers with ConvertFixture {

  "Convert oder command" should "convert protocol place order to domain" in {
    ConvertOrderCommand.toDomain(protocol.PlaceOrder(protocolOrder)) shouldBe domain.PlaceOrder(ID, domainData)
  }

  it should "convert protocol cancel order to domain" in {
    ConvertOrderCommand.toDomain(protocol.CancelOrder(ID)) shouldBe domain.CancelOrder(ID)
  }

}
