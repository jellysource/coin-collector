package com.jellysource.broker.api

import java.time.LocalDateTime

import akka.actor.ActorRef
import akka.testkit.{TestActorRef, TestProbe}
import com.jellysource.akka.support.BaseActorSpecWithFixture
import com.jellysource.akka.support.actors.Replier
import com.jellysource.broker.api.convert.ConvertOrderEvent
import com.jellysource.broker.api.protocol.OrderEvent
import com.jellysource.broker.domain.{order => domain}

class BrokerApiSpec extends BaseActorSpecWithFixture {

  "Broker api" should "forward command to broker" in { fixture => import fixture._
    api ! protocol.CancelOrder("id")
    broker.expectMsg(domain.CancelOrder("id"))
  }

  it should "forward events from broker to command sender" in { fixture => import fixture._
    apiWithEcho ! protocol.CancelOrder("id")
    expectMsg(orderCancelled)
  }

  it should "publish events in system event stream" in { fixture => import fixture._
    subscribeForOrderEvents()
    apiWithEcho.tell(protocol.CancelOrder("id"), ActorRef.noSender)
    expectMsg(orderCancelled)
  }

  class BrokerApiFixture extends AkkaFixture {
    lazy val api = TestActorRef(BrokerApi.props(broker.testActor))
    lazy val broker = TestProbe("broker")
    lazy val apiWithEcho = TestActorRef(BrokerApi.props(brokerWithEcho))
    lazy val brokerWithEcho = TestActorRef(Replier.props(domain.OrderCancelled("id", time = time)))
    lazy val time: LocalDateTime = LocalDateTime.now()
    lazy val orderCancelled = protocol.OrderCancelled(id = "id", time = time.atZone(ConvertOrderEvent.timeZoneId).toOffsetDateTime)
    lazy val subscriber = TestProbe("subscriber")
    def subscribeForOrderEvents(): Unit = system.eventStream.subscribe(self, classOf[OrderEvent])
  }

  override protected type Fixture = BrokerApiFixture
  override def createAkkaFixture() = new BrokerApiFixture
}
