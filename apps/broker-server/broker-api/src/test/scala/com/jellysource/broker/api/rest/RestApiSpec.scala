package com.jellysource.broker.api.rest

import akka.http.scaladsl.model.{ContentTypes, HttpEntity, StatusCodes}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.testkit.TestProbe
import com.jellysource.broker.api.protocol.{Buy, Market, Order, PlaceOrder}
import org.scalatest.{FlatSpec, Matchers}


class RestApiSpec extends FlatSpec with Matchers with ScalatestRouteTest {

  "Rest api" should "place order on POST json to /orders" in {
    Post("/orders", json(order)) ~> restApi.route ~> check {
      status shouldBe StatusCodes.Accepted
      api.expectMsg(placeOrder)
    }
  }

  lazy val restApi = new RestApi(api.testActor)

  lazy val api = TestProbe("api")

  lazy val order: String =
    """{
      |  "id": "1",
      |  "symbol": "T",
      |  "quantity": 10,
      |  "action": "BUY",
      |  "execution": {
      |    "type": "MARKET"
      |  }
      |}""".stripMargin

  lazy val placeOrder = PlaceOrder(Order(
    id = "1",
    symbol = "T",
    quantity = 10,
    action = Buy,
    execution = Market
  ))

  private def json(content: String) =
    HttpEntity(contentType = ContentTypes.`application/json`, bytes = content.getBytes)
}
