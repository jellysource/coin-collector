package com.jellysource.broker.api.convert

import org.scalatest.{FlatSpec, Matchers}

import com.jellysource.broker.domain.{order => domain}
import com.jellysource.broker.api.protocol

class ConvertOrderEventSpec extends FlatSpec with Matchers with ConvertFixture {

  "Convert order event" should "convert domain order created to protocol" in {
    ConvertOrderEvent.toProtocol(domainOrderCreated) shouldBe protocolOrderCreated
  }

  it should "convert domain order placed to protocol" in {
    ConvertOrderEvent.toProtocol(domainOrderPlaced) shouldBe protocolOrderPlaced
  }

  it should "convert domain order filled partially to protocol" in {
    ConvertOrderEvent.toProtocol(domainOrderFilledPartially) shouldBe protocolOrderFilledPartially
  }

  it should "convert domain order filled to protocol" in {
    ConvertOrderEvent.toProtocol(domainOrderFilled) shouldBe protocolOrderFilled
  }

  it should "convert domain order cancelled to protocol" in {
    ConvertOrderEvent.toProtocol(domainOrderCancelled) shouldBe protocolOrderCancelled
  }

}
