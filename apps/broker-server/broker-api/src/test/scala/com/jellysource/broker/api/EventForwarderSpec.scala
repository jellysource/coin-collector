package com.jellysource.broker.api

import java.time.{LocalDateTime, OffsetDateTime}

import akka.testkit.{TestActorRef, TestProbe}
import com.jellysource.akka.support.BaseActorSpecWithFixture
import com.jellysource.broker.api.convert.ConvertOrderEvent
import com.jellysource.broker.domain.{order => domain}

class EventForwarderSpec extends BaseActorSpecWithFixture {

  "Event forwarder" should "convert domain events to protocol and forward to all targets" in { fixture => import fixture._
    checkForwarderForwardsToAllTargets(inEvt, outEvt)
  }

  it should "be killed when order cancelled received" in { fixture => import fixture._
    checkForwarderDiesAfterForwarding(inCancelled)
  }

  it should "forward oreder cancelled before it dies" in { fixture => import fixture._
    checkForwarderForwardsToAllTargets(inCancelled, outCancelled)
  }

  it should "be killed when order filled received" in { fixture => import fixture._
    checkForwarderDiesAfterForwarding(inFilled)
  }

  it should "forward oreder filled before it dies" in { fixture => import fixture._
    checkForwarderForwardsToAllTargets(inFilled, outFilled)
  }

  class EventForwarderFixture extends AkkaFixture {
    lazy val forwarder = TestActorRef(EventForwarder.props(target1.testActor, target2.testActor))
    lazy val target1 = TestProbe("target1")
    lazy val target2 = TestProbe("target2")
    lazy val inEvt = domain.OrderPlaced(id = "id", time = atNow)
    lazy val outEvt = protocol.OrderPlaced(id = "id", time = atNowAtOffset)
    lazy val inCancelled = domain.OrderCancelled("id", time = atNow)
    lazy val outCancelled = protocol.OrderCancelled(id = "id", time = atNowAtOffset)
    lazy val inFilled = domain.OrderFilled(id = "id", time = atNow, avgPrice = 100)
    lazy val outFilled = protocol.OrderFilled(id = "id", time = atNowAtOffset, price = 100)
    lazy val atNow: LocalDateTime = LocalDateTime.now()
    lazy val atNowAtOffset: OffsetDateTime = atNow.atZone(ConvertOrderEvent.timeZoneId).toOffsetDateTime

    def checkForwarderForwardsToAllTargets(in: domain.OrderEvent, out: protocol.OrderEvent): Unit = {
      forwarder ! in
      target1.expectMsg(out)
      target2.expectMsg(out)
    }

    def checkForwarderDiesAfterForwarding(in: domain.OrderEvent): Unit = {
      watch(forwarder)
      forwarder ! in
      expectTerminated(forwarder)
    }
  }

  override protected type Fixture = EventForwarderFixture
  override def createAkkaFixture() = new EventForwarderFixture
}
