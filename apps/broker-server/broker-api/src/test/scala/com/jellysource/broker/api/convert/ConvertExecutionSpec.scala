package com.jellysource.broker.api.convert

import org.scalatest.{FlatSpec, Matchers}

import com.jellysource.broker.domain.{order => domain}
import com.jellysource.broker.api.protocol

class ConvertExecutionSpec extends FlatSpec with Matchers {

  "Convert execution" should "convert domain market to protocol market" in {
    ConvertExecution.toProtocol(domain.Market) shouldBe protocol.Market
  }

  it should "convert domain limit to protocol limit" in {
    ConvertExecution.toProtocol(domain.Limit(10.01)) shouldBe protocol.Limit(10.01)
  }

  it should "convert domain stop to protocol stop" in {
    ConvertExecution.toProtocol(domain.Stop(10.01)) shouldBe protocol.Stop(10.01)
  }

  it should "convert protocol market to domain market" in {
    ConvertExecution.toDomain(protocol.Market) shouldBe domain.Market
  }

  it should "convert protocol limit to domain limit" in {
    ConvertExecution.toDomain(protocol.Limit(10.01)) shouldBe domain.Limit(10.01)
  }

  it should "convert protocol stop to domain stop" in {
    ConvertExecution.toDomain(protocol.Stop(10.01)) shouldBe domain.Stop(10.01)
  }
  
}
