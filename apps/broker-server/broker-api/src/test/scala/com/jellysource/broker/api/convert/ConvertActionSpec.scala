package com.jellysource.broker.api.convert

import org.scalatest.{FlatSpec, Matchers}

import com.jellysource.broker.domain.{order => domain}
import com.jellysource.broker.api.protocol

class ConvertActionSpec extends FlatSpec with Matchers {

  "Convet action" should "convert domain buy to protocol buy" in {
    ConvertAction.toProtocol(domain.Buy) shouldBe protocol.Buy
  }

  it should "convert domain sell to protocol sell" in {
    ConvertAction.toProtocol(domain.Sell) shouldBe protocol.Sell
  }

  it should "convert protocol buy to domain buy" in {
    ConvertAction.toDomain(protocol.Buy) shouldBe domain.Buy
  }

  it should "convert protocol sell to domain sell" in {
    ConvertAction.toDomain(protocol.Sell) shouldBe domain.Sell
  }
}
