package com.jellysource.broker.api.convert

import com.jellysource.broker.domain.{order => domain}
import com.jellysource.broker.api.protocol

object ConvertOrderCommand {

  def toDomain(src: protocol.OrderCommand): domain.OrderCommand = {
    src match {
      case protocol.PlaceOrder(order) =>
        domain.PlaceOrder(
          id = order.id,
          order = ConvertOrder.toDomain(order)
        )
      case protocol.CancelOrder(id) =>
        domain.CancelOrder(id = id)
    }
  }

}
