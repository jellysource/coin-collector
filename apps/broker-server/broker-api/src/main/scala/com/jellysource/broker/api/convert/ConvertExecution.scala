package com.jellysource.broker.api.convert

import com.jellysource.broker.domain.{order => domain}
import com.jellysource.broker.api.protocol

object ConvertExecution {

  def toDomain(src: protocol.Execution): domain.Execution = src match {
    case protocol.Market => domain.Market
    case protocol.Limit(l) => domain.Limit(l)
    case protocol.Stop(l) => domain.Stop(l)
  }

  def toProtocol(src: domain.Execution): protocol.Execution = src match {
    case domain.Market => protocol.Market
    case domain.Limit(l) => protocol.Limit(l)
    case domain.Stop(l) => protocol.Stop(l)
  }

}
