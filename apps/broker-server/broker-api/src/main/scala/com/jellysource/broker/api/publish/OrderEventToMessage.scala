package com.jellysource.broker.api.publish
import akka.stream.alpakka.amqp.OutgoingMessage
import akka.util.ByteString
import com.jellysource.broker.api.protocol.OrderEvent
import com.jellysource.broker.api.protocol.json.JsonSerDe

private object OrderEventToMessage extends (OrderEvent => OutgoingMessage) {

  override def apply(event: OrderEvent): OutgoingMessage = {
    OutgoingMessage(
      bytes = ByteString(JsonSerDe.serializeOrderEvent(event)),
      immediate = false, // if true, when there are no active consumers on all the queues sent message back
      mandatory = false, // if true, when no queue exists for this message sent back
      routingKey = Some(s"${event.id}.${event.typeName.toLowerCase}")
    )
  }

}
