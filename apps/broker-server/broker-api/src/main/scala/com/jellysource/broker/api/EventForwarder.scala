package com.jellysource.broker.api

import akka.actor.{Actor, ActorLogging, ActorRef, PoisonPill, Props}
import com.jellysource.broker.api.convert.ConvertOrderEvent
import com.jellysource.broker.domain.{order => domain}

object EventForwarder {
  def props(targets: ActorRef*) = Props(new EventForwarder(targets))
}

private class EventForwarder(targets: Seq[ActorRef]) extends Actor with ActorLogging{

  override def receive: Receive = {

    case evt: domain.OrderEvent =>
      forward(evt)
      killOnFinalEvent(evt)

    case other =>
      log.warning(s"Cannot forward unknown event: $other")
  }

  private def forward(evt: domain.OrderEvent): Unit =
    targets.foreach {
      _ ! ConvertOrderEvent.toProtocol(evt)
    }

  private def killOnFinalEvent(evt: domain.OrderEvent): Unit =
    evt match {
      case _: domain.OrderCancelled | _: domain.OrderFilled =>
        self ! PoisonPill
      case _ =>
        // do nothing, not a final event
    }
}
