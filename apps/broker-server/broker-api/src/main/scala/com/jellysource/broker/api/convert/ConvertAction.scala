package com.jellysource.broker.api.convert

import com.jellysource.broker.domain.{order => domain}
import com.jellysource.broker.api.protocol

object ConvertAction {

  def toDomain(src: protocol.Action): domain.Action = src match {
    case protocol.Sell => domain.Sell
    case protocol.Buy  => domain.Buy
  }

  def toProtocol(src: domain.Action): protocol.Action = src match {
    case domain.Sell => protocol.Sell
    case domain.Buy  => protocol.Buy
  }

}
