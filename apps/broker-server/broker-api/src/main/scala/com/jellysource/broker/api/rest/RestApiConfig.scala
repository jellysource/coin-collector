package com.jellysource.broker.api.rest

case class RestApiConfig(
  host: String,
  port: Int
)
