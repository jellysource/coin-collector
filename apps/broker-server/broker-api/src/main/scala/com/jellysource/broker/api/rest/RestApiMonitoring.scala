package com.jellysource.broker.api.rest
import io.prometheus.client.Counter

private object RestApiMonitoring {

  val orderRequestCounter: Counter =
    Counter.build()
      .name("restapi_order_requests_total")
      .help("Counts all order requests made")
      .labelNames("type", "action")
      .register()

}
