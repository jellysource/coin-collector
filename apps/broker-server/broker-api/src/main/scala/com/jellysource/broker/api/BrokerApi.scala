package com.jellysource.broker.api

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import com.jellysource.broker.api.convert.ConvertOrderCommand

object BrokerApi {
  def props(broker: ActorRef) = Props(new BrokerApi(broker))
}

class BrokerApi(broker: ActorRef) extends Actor with ActorLogging {

  override def receive: Receive = {

    case cmd : protocol.OrderCommand =>
      broker.tell(ConvertOrderCommand.toDomain(cmd), context.actorOf(EventForwarder.props(sender(), self)))

    case evt : protocol.OrderEvent =>
      context.system.eventStream.publish(evt)

    case other =>
      log.warning(s"Unknown command received by API: $other")

  }

}
