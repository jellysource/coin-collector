package com.jellysource.broker.api.publish
import io.prometheus.client.Counter

private object PublishMonitoring {

  val orderEventCounter: Counter =
    Counter.build()
      .name("publish_order_events_total")
      .help("Counts all order events published to message broker")
      .labelNames("type")
      .register()

}
