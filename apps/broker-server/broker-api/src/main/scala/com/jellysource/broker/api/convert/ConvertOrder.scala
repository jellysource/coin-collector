package com.jellysource.broker.api.convert

import com.jellysource.broker.domain.{order => domain}
import com.jellysource.broker.api.protocol

object ConvertOrder {

  def toProtocol(id: String, src: domain.OrderData): protocol.Order = {
    protocol.Order(
      id = id,
      symbol = src.symbol,
      quantity =  src.quantity,
      action = ConvertAction.toProtocol(src.action),
      execution = ConvertExecution.toProtocol(src.execution)
    )
  }

  def toDomain(src: protocol.Order): domain.OrderData = {
    domain.OrderData(
      symbol = src.symbol,
      quantity =  src.quantity,
      action = ConvertAction.toDomain(src.action),
      execution = ConvertExecution.toDomain(src.execution)
    )
  }
}
