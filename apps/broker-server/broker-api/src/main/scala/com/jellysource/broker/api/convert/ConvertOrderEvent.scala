package com.jellysource.broker.api.convert

import java.time.{LocalDateTime, OffsetDateTime, ZoneId, ZoneOffset}

import com.jellysource.broker.domain.{order => domain}
import com.jellysource.broker.api.protocol


object ConvertOrderEvent {

  // todo make zoneId configurable ?
  val timeZoneId: ZoneId = ZoneId.of("Europe/Bratislava")

  def toProtocol(src: domain.OrderEvent): protocol.OrderEvent = {
    src match {
      case domain.OrderCreated(id, time, order) =>
        protocol.OrderCreated(
          id = id,
          time = convertTime(time),
          symbol = order.symbol,
          quantity = order.quantity,
          action = ConvertAction.toProtocol(order.action),
          execution = ConvertExecution.toProtocol(order.execution)
        )
      case domain.OrderPlaced(id, time) =>
        protocol.OrderPlaced(
          id = id,
          time = convertTime(time)
        )
      case domain.OrderFilledPartially(id, time, quantity, price) =>
        protocol.OrderFilledPartially(
          id = id,
          time = convertTime(time),
          quantity = quantity,
          price = price
        )
      case domain.OrderFilled(id, time, price) =>
        protocol.OrderFilled(
          id = id,
          time = convertTime(time),
          price = price
        )
      case domain.OrderCancelled(id, time) =>
        protocol.OrderCancelled(
          id = id,
          time = convertTime(time)
        )
    }
  }

  private def convertTime(time: LocalDateTime): OffsetDateTime =
    time.atZone(timeZoneId).toOffsetDateTime

}
