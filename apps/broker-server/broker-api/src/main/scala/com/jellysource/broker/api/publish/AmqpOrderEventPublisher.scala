package com.jellysource.broker.api.publish

import akka.actor.{ActorRef, ActorSystem}
import akka.stream.alpakka.amqp.scaladsl.AmqpSink
import akka.stream.alpakka.amqp.{AmqpSinkSettings, AmqpUriConnectionProvider, ExchangeDeclaration}
import akka.stream.scaladsl.{Keep, Source}
import akka.stream.{Materializer, OverflowStrategy}
import com.jellysource.broker.api.protocol.OrderEvent
import com.typesafe.scalalogging.LazyLogging

class AmqpOrderEventPublisher(config: AmqpOrderEventPublisherConfig) extends LazyLogging {
  
  private lazy val exchangeDeclaration =
    ExchangeDeclaration(
      name = config.exchange,
      exchangeType = "topic",
      durable = true
    )

  private lazy  val connectionProvider = AmqpUriConnectionProvider(config.uri)

  private lazy val amqpSink =
    AmqpSink(
      AmqpSinkSettings(connectionProvider)
        .withExchange(config.exchange)
        .withDeclarations(exchangeDeclaration)
    )

  def run(implicit materializer: Materializer): ActorRef =
    Source.actorRef[OrderEvent](100, OverflowStrategy.dropTail)
      .map { e =>
        val msg = OrderEventToMessage(e)
        PublishMonitoring.orderEventCounter.labels(e.typeName.toLowerCase).inc()
        logger.debug(s"Publishing event $e as message $msg")
        msg
      }
      .toMat(amqpSink)(Keep.left)
      .run()

  def runAndSubscribeToSystemEventStream(implicit actorSystem: ActorSystem, materializer: Materializer): Unit = {
    val subscriber = run(materializer)
    actorSystem.eventStream.subscribe(subscriber, classOf[OrderEvent])
  }

}
