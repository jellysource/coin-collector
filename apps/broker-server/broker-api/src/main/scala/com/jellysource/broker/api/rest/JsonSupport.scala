package com.jellysource.broker.api.rest

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import com.jellysource.broker.api.protocol.json.format.OrderFormat

trait JsonSupport extends SprayJsonSupport with OrderFormat
