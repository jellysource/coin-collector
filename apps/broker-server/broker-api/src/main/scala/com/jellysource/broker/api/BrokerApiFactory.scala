package com.jellysource.broker.api

import akka.actor.{ActorRef, ActorSystem}
import akka.http.scaladsl.Http
import akka.stream.Materializer
import com.jellysource.broker.api.publish.{AmqpOrderEventPublisher, AmqpOrderEventPublisherConfig}
import com.jellysource.broker.api.rest.{RestApi, RestApiConfig}
import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.Future

class BrokerApiFactory(
  restApiConfig: RestApiConfig,
  publishConfig: AmqpOrderEventPublisherConfig
) extends LazyLogging {

  def run(broker: ActorRef)(implicit actorSystem: ActorSystem, materializer: Materializer): Future[Http.ServerBinding] = {
    val publisher = new AmqpOrderEventPublisher(publishConfig)
    val api = actorSystem.actorOf(BrokerApi.props(broker), "api")
    val rest = new RestApi(api)

    val restBinding = rest.start(restApiConfig)
    publisher.runAndSubscribeToSystemEventStream

    restBinding
  }

}
