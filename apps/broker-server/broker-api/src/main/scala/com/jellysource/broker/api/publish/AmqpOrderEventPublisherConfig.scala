package com.jellysource.broker.api.publish

case class AmqpOrderEventPublisherConfig(uri: String, exchange: String)
