package com.jellysource.broker.api.rest

import akka.actor.{ActorRef, ActorSystem}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives
import akka.stream.Materializer
import com.jellysource.broker.api.protocol.{Order, PlaceOrder}
import com.typesafe.scalalogging.LazyLogging
import io.prometheus.client.CollectorRegistry
import PrometheusMarshallers._

import scala.concurrent.Future

class RestApi(brokerApi: ActorRef)
  extends Directives
    with JsonSupport
    with LazyLogging {

  private[rest] val route =
    path("orders") {
      post {
        entity(as[Order]) { order =>
          RestApiMonitoring.orderRequestCounter.labels("create", order.action.toString.toLowerCase).inc()
          logger.info(s"Post order request received: $order")
          brokerApi ! PlaceOrder(order)
          complete(StatusCodes.Accepted)
        }
      }
    } ~
    path("metrics"){
      get {
        complete(CollectorRegistry.defaultRegistry)
      }
    }

  def start(config: RestApiConfig)(implicit actorSystem: ActorSystem, materializer: Materializer): Future[Http.ServerBinding] = {
    Http().bindAndHandle(route, config.host, config.port)
  }

}