package com.jellysource.broker.domain.order

import java.time.LocalDateTime

trait OrderFixture {

  lazy val createdOrder = CreatedOrder(id, buyIBM)

  lazy val placedOrder = PlacedOrder(id, buyIBM, placedAt)

  lazy val partiallyFilledOrderFirst =
    PartiallyFilledOrder(id, buyIBM, placedAt, OrderFills.create(fillFirst))

  lazy val partiallyFilledOrderSecond =
    PartiallyFilledOrder(id, buyIBM, placedAt, OrderFills.create(fillFirst, fillSecond))

  lazy val filledOrderOnOnce =
    FilledOrder(
      id,
      buyIBM,
      placedAt,
      OrderFills.create(Fill(buyIBM.quantity, priceFirst, filledAtFirstTime))
    )

  lazy val filledOrderOnThreeTimes =
    FilledOrder(id, buyIBM, placedAt, OrderFills.create(fillFirst, fillSecond, fillThird))

  lazy val id = "id-order"
  lazy val account = "id-account"
  lazy val buyIBM = OrderData("IBM", 15, Buy, Market)
  lazy val placedAt: LocalDateTime = LocalDateTime.now()
  lazy val quantityOneThird: Int = buyIBM.quantity / 3

  lazy val priceFirst = 10
  lazy val filledAtFirstTime: LocalDateTime = LocalDateTime.now()
  lazy val fillFirst = Fill(quantityOneThird, priceFirst, filledAtFirstTime)

  lazy val priceSecond = 11
  lazy val filledAtSecondTime: LocalDateTime = filledAtFirstTime.plusSeconds(1)
  lazy val fillSecond = Fill(quantityOneThird, priceSecond, filledAtSecondTime)

  lazy val priceThird = 12
  lazy val filledAtThirdTime: LocalDateTime = filledAtSecondTime.plusSeconds(1)
  lazy val fillThird = Fill(quantityOneThird, priceThird, filledAtThirdTime)

}
