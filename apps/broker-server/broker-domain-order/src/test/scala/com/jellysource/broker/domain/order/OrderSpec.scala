package com.jellysource.broker.domain.order

import org.scalatest.{FlatSpec, Matchers}

class OrderSpec extends FlatSpec with Matchers with OrderFixture {

  "Order" can "be created" in {
    Order.create(id, buyIBM) shouldBe createdOrder
  }

  "Created order" can "be placed" in {
    createdOrder.place(placedAt) shouldBe placedOrder
  }

  "Placed order" can "be filled" in {
    placedOrder.fill(buyIBM.quantity, priceFirst, filledAtFirstTime) shouldBe filledOrderOnOnce
  }

  "Placed order" can "be partially filled" in {
    placedOrder.fill(buyIBM.quantity / 3, priceFirst, filledAtFirstTime) shouldBe partiallyFilledOrderFirst
  }

  "Partially filled order" can "be partially filled again" in {
    partiallyFilledOrderFirst.fill(buyIBM.quantity / 3, priceSecond, filledAtSecondTime) shouldBe partiallyFilledOrderSecond
  }

  "Partially filled order" can "be fully filled" in {
    partiallyFilledOrderSecond.fill(buyIBM.quantity / 3, priceThird, filledAtThirdTime) shouldBe filledOrderOnThreeTimes
  }

}
