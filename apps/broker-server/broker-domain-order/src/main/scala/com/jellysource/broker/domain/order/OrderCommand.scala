package com.jellysource.broker.domain.order

sealed trait OrderCommand

case class PlaceOrder(id: OrderId, order: OrderData) extends OrderCommand
case class CancelOrder(id: OrderId) extends OrderCommand
