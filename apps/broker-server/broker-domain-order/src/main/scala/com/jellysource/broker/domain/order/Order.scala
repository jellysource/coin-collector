package com.jellysource.broker.domain.order

import java.time.{LocalDateTime, ZoneOffset}

sealed trait Order {
  def id: OrderId
  def data: OrderData
}
sealed trait FillableOrder extends Order {
  def fill(quantity: Int, price: Price, at: LocalDateTime): MaybeFilledOrder
}
sealed trait MaybeFilledOrder extends Order {
  def fills: OrderFills
  lazy val remaining: Int = data.quantity - fills.quantity
  lazy val isFullyFilled: Boolean = remaining == 0
}

case class OrderFills(fills: List[Fill]) {
  lazy val lastAt: LocalDateTime = fills.map(_.at).maxBy(_.toInstant(ZoneOffset.UTC).toEpochMilli)
  lazy val avgPrice: Price = fills.map(_.price).sum / fills.length
  lazy val quantity: Int = fills.map(_.quantity).sum
  lazy val count: Int = fills.length
  def add(fill: Fill): OrderFills = OrderFills(fills :+ fill)
}
object OrderFills {
  def create(fills: Fill*) = OrderFills(fills.toList)
  val empty = OrderFills(List.empty)
}

case class Fill(quantity: Int, price: Price, at: LocalDateTime)

case class CreatedOrder private (id: OrderId, data: OrderData) extends Order {
  def place(at: LocalDateTime): PlacedOrder =
    PlacedOrder(id, data, at)
}

case class PlacedOrder private (
  id: OrderId,
  data: OrderData,
  placedAt: LocalDateTime
) extends FillableOrder {

  override def fill(quantity: Int, price: Price, at: LocalDateTime): MaybeFilledOrder =
    if (quantity == data.quantity) {
      FilledOrder(
        id,
        data,
        placedAt,
        OrderFills.create(Fill(quantity, price, at))
      )
    } else {
      PartiallyFilledOrder(
        id,
        data,
        placedAt,
        OrderFills.create(Fill(quantity, price, at))
      )
    }

}

case class PartiallyFilledOrder private (
  id: String,
  data: OrderData,
  placedAt: LocalDateTime,
  fills: OrderFills
) extends MaybeFilledOrder
  with FillableOrder {

  override def fill(quantity: Int, price: Price, at: LocalDateTime): MaybeFilledOrder =
    if (fills.quantity + quantity == data.quantity) {
      FilledOrder(
        id,
        data,
        placedAt,
        fills add Fill(quantity, price, at)
      )
    } else {
      PartiallyFilledOrder(
        id,
        data,
        placedAt,
        fills add Fill(quantity, price, at)
      )
    }
}

case class FilledOrder private (
  id: String,
  data: OrderData,
  placedAt: LocalDateTime,
  fills: OrderFills
) extends MaybeFilledOrder

object Order {
  def create(id: OrderId, data: OrderData): CreatedOrder =
    CreatedOrder(id, data)
}