package com.jellysource.broker.domain

package object order {

  type OrderId = String
  type Symbol = String
  type Exchange = String
  type Price = BigDecimal

}
