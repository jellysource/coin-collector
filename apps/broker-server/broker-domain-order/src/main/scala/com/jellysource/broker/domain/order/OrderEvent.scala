package com.jellysource.broker.domain.order

import java.time.LocalDateTime

sealed trait OrderEvent {
  def id: OrderId
  def time: LocalDateTime
}

case class OrderCreated(
  id: OrderId,
  time: LocalDateTime,
  order: OrderData
) extends OrderEvent

case class OrderPlaced(
  id: OrderId,
  time: LocalDateTime
) extends OrderEvent

case class OrderFilled(
  id: OrderId,
  time: LocalDateTime,
  avgPrice: Price
) extends OrderEvent

case class OrderFilledPartially(
  id: OrderId,
  time: LocalDateTime,
  quantity: Int,
  price: Price
) extends OrderEvent

case class OrderCancelled(
  id: OrderId,
  time: LocalDateTime
) extends OrderEvent
