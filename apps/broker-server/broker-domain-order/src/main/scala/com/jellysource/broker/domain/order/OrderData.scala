package com.jellysource.broker.domain.order

case class OrderData(
  symbol: Symbol,
  quantity: Int,
  action: Action,
  execution: Execution
)

sealed trait Action
case object Sell extends Action
case object Buy extends Action

sealed trait Execution
case object Market extends Execution
case class Limit(price: Price) extends Execution
case class Stop(price: Price) extends Execution
