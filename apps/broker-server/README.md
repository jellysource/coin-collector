# Coin collector - Broker Server

Broker module provides `REST API` for creating trading orders requests to external broker service.

Currently only [IB Trader Workstation](https://www.interactivebrokers.co.uk/en/home.php) (TWS) from interactive brokers is supported.

## How to build

    ./gradlew clean build


## How to run

There are two additional applications which need to be started before broker server (for local setup).

### TWS

You need to have installation of TWS.
Check [official page](https://www.interactivebrokers.com/en/index.php?f=14099#tws-software) for details. 

In TWS you need to change some settings under `File -> Global configuration -> API -> Settings`:
* check **in** `Enable ActiveX and socket clients`
* check **out** `Read-Only API`

> Alternatively you can install [IB gateway](https://www.interactivebrokers.com/en/index.php?f=16457) 
  which is lighter version without GUI. 

### RabbitMQ

You need to  have running [rabbitMQ](https://www.rabbitmq.com/) server.

Easiest way is to use docker image and just run:

    docker run -it -p 5672:5672 rabbitmq:3.7.14-alpine

### Broker server

    ./gradlew run -Dtws.account-id=TWS_ACCOUNT_ID -Dtws.connection.port=TWS_PORT

Where:
- **TWS_ACCOUNT_ID** is you TWS account id and you can get it on [account management page](https://www.interactivebrokers.co.uk/AccountManagement/AmAuthentication?&loginType=2&SERVICE=AM.LOGIN) under `Settings -> Account Settings`
- **TWS_PORT** is socket port used to connect to TWS and can be found in TWS (IB Gateway) in `API -> Settings`

## How to publish

    ./gradlew bintrayUpload -PbintrayUser=YOUR_USER -PbintrayKey=YOUR_KEY


You need replace YOUR_USER and YOUR_KEY with bintray credentials.

## Deployment

Resulting package is `RPM` which can be installed on some machine. It contains broker server application.

### How to install

    sudo yum install broker-server

### Where it is installed

    /opt/broker-server
        ├── bin
        ├── conf -> /etc/broker-server
        ├── lib
        └── log  -> /var/log/broker-server

### How to start

    sudo service broker-server start

### How to restart

    sudo service broker-server restart

### How to check status

    sudo service broker-server status

### How to configure

Configuration file can be found on path `/etc/broker-server/broker.conf`. It is written in [HCON](https://github.com/lightbend/config/blob/master/HOCON.md) format.

| Property name                 | Default value | Description                                   |
|-------------------------------|---------------|-----------------------------------------------|
| tws.connection.host           | localhost     | host name where IB Trader Workstation is running |
| tws.connection.port           | 7497          | port which is open from IB Trader Workstation, can be seend / cahnged under global configuration `API -> Settings -> Socket port` |
| tws.connection.client-id      | 0             | id of client, if there are multiple connected clients need to be unique |
| tws.account-id                |               | id of interacitve brokers trading account used for all orders |
| tws.primary-exchange-mapping  |               | mapping of symbols to primary exchange which will be used for tws contract <br/>e.g. `{`<br/>`    CSCO = BATS`<br/>`    MSFT = ISLAND`<br/>`}` |
| api.rest.host                 | localhost     | host name where REST API will be bind         |
| api.rest.port                 | 9001          | port on which REST API will be listening      |
| api.publish.amqp.host         | localhost     | host where messaging broker supporting [AMQP protocol](https://www.amqp.org/) is running |
| api.publish.amqp.port         | 5672          | port on which messaging broker is listening   |
| api.publish.amqp.user         | guest         | username used to connect to messaging broker  |
| api.publish.amqp.password     | guest         | password userd to connect to messageing broker |
| api.publish.amqp.uri          | amqp://${ api.publish.amqp.user }:${ api.publish.amqp.password }@${ api.publish.amqp.host }:${ api.publish.amqp.port } | full connection URI to message broker, some additional connection parameters can be added here |
| api.publish.amqp.exchange     | coin.order.event | name of exchange where ored events will be published |