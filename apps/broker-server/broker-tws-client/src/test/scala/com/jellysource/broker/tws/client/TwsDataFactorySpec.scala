package com.jellysource.broker.tws.client
import com.ib.contracts.StkContract
import org.scalatest.{FlatSpec, Matchers}

class TwsDataFactorySpec extends FlatSpec with Matchers {

  "Tws data factory" should "produce stock contract with no primary exchange when no defined for symbol" in {
    factory.contract("FB") shouldBe new StkContract("FB")
  }

  it should "produce stock contract with primary exchange when defined for symbol" in {
    factory.contract("CSCO") shouldBe contractWithPrimaryExchange
  }

  private val factory = TwsDataFactory(primaryExchangeMapping = Map("CSCO" -> "BATS"))

  private val contractWithPrimaryExchange = {
    val contract = new StkContract("CSCO")
    contract.primaryExch("BATS")
    contract
  }
}
