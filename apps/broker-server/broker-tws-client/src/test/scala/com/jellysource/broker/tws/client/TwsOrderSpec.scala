package com.jellysource.broker.tws.client

import java.time.LocalDateTime

import akka.testkit.{TestActorRef, TestProbe}
import com.ib.client.OrderStatus.{Cancelled, Filled, Submitted}
import com.ib.client.Types.Action
import com.ib.client.{OrderType, Order => IbOrder}
import com.ib.contracts.StkContract
import com.jellysource.akka.support.BaseActorSpecWithFixture
import com.jellysource.broker.domain.order._
import com.jellysource.broker.tws.client.api.OrderHandler.OrderStatusChanged
import com.jellysource.broker.tws.client.api.TwsApi

class TwsOrderSpec extends BaseActorSpecWithFixture {

  "Tws Order" should "be placed via api" in { fixture => import fixture._
    twsOrderCreated ! PlaceOrder(orderId, orderData)
    apiProbe.expectMsg(TwsApi.PlaceOrModifyOrder(contract, ibOrder))
  }

  it should "rise OrderPlaced event when it was submitted" in { fixture => import fixture._
    twsOrderCreated ! OrderStatusChanged(status = Submitted, remaining = 10)
    orderProbe.expectMsgClass(classOf[OrderPlaced])
  }

  it should "rise OrderFilled event when it was fully filled" in { fixture => import fixture._
    twsOrderPlaced ! OrderStatusChanged(status = Filled, filled = 10, avgFillPrice = 100.0)
    orderProbe.expectMsgClass(classOf[OrderFilled])
  }

  it should "rise OrderPartiallyFilled event when it was partially filled" in { fixture => import fixture._
    twsOrderPlaced ! OrderStatusChanged(status = Submitted, filled = 5, remaining = 5, avgFillPrice = 100.0)
    orderProbe.expectMsgClass(classOf[OrderFilledPartially])
  }

  it should "rise OrderPartiallyFilled event followed by OrderFilled  when it was fully filled after partially filled before" in { fixture => import fixture._
    twsOrderFilledPartially ! OrderStatusChanged(status = Filled, filled = 10, avgFillPrice = 100.0)
    orderProbe.expectMsgClass(classOf[OrderFilledPartially])
    orderProbe.expectMsgClass(classOf[OrderFilled])
  }

  it should "not rise OrderFilled event again when filled status reporter after order fully filled" in { fixture => import fixture._
    twsOrderFilled ! OrderStatusChanged(status = Filled, filled = 10, avgFillPrice = 100.0)
    orderProbe.expectNoMessage()
  }

  it should "rise OrderCancelled event and die when cancelled" in { fixture => import fixture._
    twsOrderCreated ! OrderStatusChanged(status = Cancelled)
    val cancelled = orderProbe.expectMsgType[OrderCancelled]
    cancelled.id shouldBe orderId
  }

  class OrderManagerFixture extends AkkaFixture {
    def twsOrderCreated: TestActorRef[Nothing] = twsTestOrder(orderCreated)
    def twsOrderPlaced: TestActorRef[Nothing] = twsTestOrder(orderPlaced)
    def twsOrderFilledPartially: TestActorRef[Nothing] = twsTestOrder(orderFilledPartially)
    def twsOrderFilled: TestActorRef[Nothing] = twsTestOrder(orderFilled)
    lazy val apiProbe = TestProbe("order")
    lazy val orderProbe = TestProbe("order")
    lazy val orderId = "order-id"
    lazy val accountId = "account-id"
    lazy val orderData = OrderData("FB", 10, Sell, Market)
    lazy val contract = new StkContract("FB")
    lazy val orderCreated: CreatedOrder = Order.create(orderId, orderData)
    lazy val orderPlaced: PlacedOrder = orderCreated.place(LocalDateTime.now())
    lazy val orderFilledPartially: PartiallyFilledOrder = orderPlaced.fill(5, 100.0, LocalDateTime.now()).asInstanceOf[PartiallyFilledOrder]
    lazy val orderFilled: FilledOrder = orderPlaced.fill(10, 100.0, LocalDateTime.now()).asInstanceOf[FilledOrder]
    lazy val ibOrder: IbOrder = {
      val o = new IbOrder()
      o.orderId(0)
      o.account(accountId)
      o.action(Action.SELL)
      o.orderType(OrderType.MKT)
      o.totalQuantity(10)
      o
    }

    def twsTestOrder(o: Order) = TestActorRef(
      TwsOrder.props(
        accountId,
        TwsDataFactory(Map.empty),
        o,
        apiProbe.testActor,
        orderProbe.testActor
      )
    )

  }

  override protected type Fixture = OrderManagerFixture
  override def createAkkaFixture() = new OrderManagerFixture
}
