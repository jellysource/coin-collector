package com.jellysource.broker.tws.client.api

import akka.testkit.TestProbe
import com.ib.client.OrderStatus
import com.jellysource.akka.support.BaseActorSpecWithFixture
import com.jellysource.broker.tws.client.api.OrderHandler.OrderStatusChanged

class OrderHandlerSpec extends BaseActorSpecWithFixture {

  override protected type Fixture = OrderHandlerFixture
  override def createAkkaFixture() = new OrderHandlerFixture

  "Order handler" should "report order status change to order" in { fixture =>
    import fixture._
    orderHandler.orderStatus(
      orderStatusChanged.status,
      orderStatusChanged.filled,
      orderStatusChanged.remaining,
      orderStatusChanged.avgFillPrice,
      orderStatusChanged.permId,
      orderStatusChanged.parentId,
      orderStatusChanged.lastFillPrice,
      orderStatusChanged.clientId,
      orderStatusChanged.whyHeld
    )
    order.expectMsg(orderStatusChanged)
  }

// cannot create Order state instance !
//  it should "reprot order state change to order" in { fixture =>
//    import fixture._
//    orderHandler.orderState(orderState)
//    order.expectMsg(OrderStateChanged(orderState))
//  }

  it should "report an error to order" in { fixture => import fixture._
    orderHandler.handle(errorCode, errorMessage)
    order.expectMsg(OrderHandler.Error(errorCode, errorMessage))
  }


  class OrderHandlerFixture extends AkkaFixture {
    lazy val orderHandler = new OrderHandler(order.testActor)
    lazy val order = TestProbe("order")
    lazy val orderStatusChanged = OrderStatusChanged(
      status = OrderStatus.Submitted,
      filled = 0,
      remaining = 1,
      avgFillPrice = 2,
      permId = 3,
      parentId = 4,
      lastFillPrice = 5,
      clientId = 6,
      whyHeld = "held"
    )
    // constructor inaccessible !
    //    lazy val orderState =
    //      new OrderState(
    //        "status",
    //        "init-margin",
    //        "maint-margin",
    //        "load",
    //        1.0,
    //        0.1,
    //        1.5,
    //        "curr",
    //        "warn"
    //      )
    lazy val errorCode = 10
    lazy val errorMessage = "error"
  }
}
