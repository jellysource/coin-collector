package com.jellysource.broker.tws.client

case class Connection(
  host: String,
  port: Int,
  clientId: Int
)
