package com.jellysource.broker.tws.client

import com.ib.client.{Contract, Order, OrderType}
import com.ib.client.Types.Action
import com.ib.contracts.StkContract
import com.jellysource.broker.domain.order.{Buy, Limit, Market, OrderData, Sell, Stop, Symbol, Exchange}

private case class TwsDataFactory(primaryExchangeMapping: Map[Symbol, Exchange] = Map.empty) {

  def contract(symbol: Symbol): Contract = {
    val contract = new StkContract(symbol)
    // Set primary exchange for configured symbols
    // From doc:
    // For certain smart-routed stock contracts that have the same symbol, currency and exchange,
    // you would also need to specify the primary exchange attribute to uniquely define the contract.
    // This should be defined as the native exchange of a contract, and is good practice for all stocks.
    // for smart routing, see: https://www.interactivebrokers.com/en/index.php?f=1685
    primaryExchangeMapping.get(symbol).foreach { exchange =>
      contract.primaryExch(exchange)
    }
    contract
  }


  def order(accountId: String, orderData: OrderData): Order = {
    import orderData._
    val order = new Order()
    order.orderId(0) // 0 will be replaced by current id within controller
    order.account(accountId)
    order.action(action match {
      case Sell => Action.SELL
      case Buy  => Action.BUY
    })
    execution match {
      case Market =>
        order.orderType(OrderType.MKT)
      case Limit(price) =>
        order.orderType(OrderType.LMT)
        order.lmtPrice(price.doubleValue())
      case Stop(price) =>
        order.orderType(OrderType.STP)
        order.auxPrice(price.doubleValue())
    }
    order.totalQuantity(quantity)
    order
  }
}
