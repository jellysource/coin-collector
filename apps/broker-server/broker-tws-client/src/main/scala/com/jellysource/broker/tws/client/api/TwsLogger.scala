package com.jellysource.broker.tws.client.api

import com.ib.controller.ApiConnection.ILogger
import com.typesafe.scalalogging.LazyLogging

private[api] sealed class TwsLogger extends ILogger with LazyLogging {
  override def log(valueOf: String): Unit = logger.debug(valueOf)
}
private[api] object TwsInLogger extends TwsLogger
private[api] object TwsOutLogger extends TwsLogger