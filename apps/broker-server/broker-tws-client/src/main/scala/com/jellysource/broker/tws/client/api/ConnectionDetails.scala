package com.jellysource.broker.tws.client.api

import com.jellysource.broker.tws.client.api.TwsApi.Connect

private case class ConnectionDetails(
  host: String,
  port: Int,
  clientId: Int,
  options: Option[String] = None,
  attempt: Int = 0
) {

  def nextAttempt: ConnectionDetails = copy(attempt = attempt + 1)

  def resetAttempt: ConnectionDetails = copy(attempt = 0)

  def toConnectCommand: Connect = Connect(host = host, port = port, clientId = clientId, options = options)
}
