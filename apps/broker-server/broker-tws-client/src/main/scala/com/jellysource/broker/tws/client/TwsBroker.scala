package com.jellysource.broker.tws.client

import java.time.LocalDateTime

import akka.actor.{Actor, ActorLogging, ActorRef, PoisonPill, Props, Terminated}
import com.jellysource.broker.domain.order._

private object TwsBroker {
  def props(accountId: String, dataFactory: TwsDataFactory, api: ActorRef): Props =
    Props(new TwsBroker(accountId, dataFactory, api))
}

private class TwsBroker(accountId: String, dataFactory: TwsDataFactory, api: ActorRef)
  extends Actor
    with ActorLogging {

  override def preStart(): Unit =
    context.watch(api)

  override def receive: Receive = {
    case placeOrder: PlaceOrder =>
      val order = Order.create(placeOrder.id, placeOrder.order)
      val twsOrder = context.actorOf(TwsOrder.props(accountId, dataFactory, order, api, sender()), s"tws-order-${placeOrder.id}")
      sender() ! OrderCreated(id = placeOrder.id, time = LocalDateTime.now(), order = placeOrder.order)
      twsOrder ! placeOrder

    case cancelOrder: CancelOrder =>
      val twsOrder = context.actorSelection(s"/user/tws-broker/tws-order-${cancelOrder.id}")
      twsOrder ! cancelOrder

    case Terminated(`api`) =>
      log.info("Api terminated. Shutting down broker.")
      self ! PoisonPill
  }

}
