package com.jellysource.broker.tws.client.api

import akka.actor.{Actor, ActorLogging, Props, Stash}
import com.ib.client.{Contract, Order}
import com.ib.controller.ApiController
import com.jellysource.broker.tws.client.TwsMonitoring
import com.jellysource.broker.tws.client.api.TwsApi._

import scala.concurrent.duration.DurationDouble

object TwsApi {

  sealed trait Command
  case class Connect(host: String, port: Int, clientId: Int, options: Option[String] = None) extends Command
  case object Disconnect extends Command
  case class PlaceOrModifyOrder(contract: Contract, order: Order) extends Command
  case class CancelOrder(id: Long) extends Command

  val props: Props = Props(new TwsApi)
}

private[api] class TwsApi extends Actor with Stash with ActorLogging {
  lazy val controller = new ApiController(
    new ConnectionHandler(self),
    TwsInLogger,
    TwsOutLogger
  )

  def connected(connectionDetails: ConnectionDetails): Receive = {
    case PlaceOrModifyOrder(contract, order) =>
      val orderHandler = new OrderHandler(sender())
      controller.placeOrModifyOrder(contract, order, orderHandler)
      orderHandler.assignId(order.orderId())
      // report handler is showing history for last 24 horus, not sure for now how to filter to include only specific order being placed
      // controller.reqExecutions(new ExecutionFilter(), new TradeReportHandler())
    case CancelOrder(id) =>
      controller.cancelOrder(id.toInt)
    case Disconnect =>
      controller.disconnect()
      context.become(disconnected())
    case ConnectionHandler.Disconnected =>
      log.info("Api disconnected due to disconnected event received from TWS")
      context.become(disconnected())
    case ConnectionHandler.Error(e) =>
      log.error(e, "Connection error.")
      controller.disconnect()
      reconnect(connectionDetails.nextAttempt)
  }

  def connecting(connectionDetails: ConnectionDetails): Receive = {
    case ConnectionHandler.Connected =>
      unstashAll()
      context.become(connected(connectionDetails.resetAttempt))
    case ConnectionHandler.Error(e) =>
      log.error(e, "Connection error.")
      reconnect(connectionDetails)
    case _ : Command =>
      stash()
  }

  def disconnected(attempt: Int = 0): Receive = {
    case Connect(host, port, clientId, options) =>
      val connectionDetails = ConnectionDetails(host = host, port = port, clientId = clientId, attempt = attempt)
      log.info(s"Connecting to TWS: $connectionDetails")
      controller.connect(host, port, clientId, options.getOrElse(""))
      TwsMonitoring.connectingCounter.inc()
      context.become(connecting(connectionDetails.nextAttempt))
    case _ : Command =>
      stash()
  }

  override def receive: Receive = disconnected()

  private def reconnect(connectionDetails: ConnectionDetails) = {
    context.become(disconnected(connectionDetails.attempt))
    val delay = reconnectionDelay(connectionDetails.attempt)
    log.info(s"Reconnect (#${connectionDetails.attempt}) in $delay...")
    context.system.scheduler.scheduleOnce(
      delay = delay,
      receiver = self,
      message = connectionDetails.toConnectCommand
    )(context.dispatcher)
  }

  private def reconnectionDelay(attempt: Int) = {
    val repeat = 5 // how many times use same delay before increasing
    val multiplier = 5 // how much to increase (multiplies attempt)
    val max = 30 // biggest delay
    Math.min(Math.max(1, (attempt + repeat - 1) / repeat ) * multiplier, max).seconds
  }

}
