package com.jellysource.broker.tws.client.api

import akka.actor.ActorRef
import com.ib.client.{OrderState, OrderStatus}
import com.ib.controller.ApiController.IOrderHandler
import com.jellysource.broker.tws.client.api.OrderHandler.{Error, OrderIdAssigned, OrderStateChanged, OrderStatusChanged}
import com.typesafe.scalalogging.LazyLogging

object OrderHandler {
  case class OrderIdAssigned(id: Int)
  case class OrderStateChanged(orderState: OrderState)
  case class OrderStatusChanged(
    status: OrderStatus,
    filled: Int = 0,
    remaining: Int = 0,
    avgFillPrice: Double = 0.0,
    permId: Long = 0,
    parentId: Int = 0,
    lastFillPrice: Double = 0.0,
    clientId: Int = 0,
    whyHeld: String = ""
  )
  case class Error(code: Int, message: String)
}

private[api] class OrderHandler(order: ActorRef) extends IOrderHandler with LazyLogging {

  override def orderState(orderState: OrderState): Unit =
    order ! OrderStateChanged(orderState)

  override def orderStatus(
    status: OrderStatus,
    filled: Int,
    remaining: Int,
    avgFillPrice: Double,
    permId: Long,
    parentId: Int,
    lastFillPrice: Double,
    clientId: Int,
    whyHeld: String
  ): Unit = {
    order ! OrderStatusChanged(
      status,
      filled,
      remaining,
      avgFillPrice,
      permId,
      parentId,
      lastFillPrice,
      clientId,
      whyHeld
    )
  }

  override def handle(errorCode: Int, errorMsg: String): Unit = {
    order ! Error(errorCode, errorMsg)
  }

  def assignId(id: Int): Unit =
    order ! OrderIdAssigned(id)
}
