package com.jellysource.broker.tws.client

import com.jellysource.broker.domain.order.{Symbol, Exchange}

case class TwsProperties(
  connection: Connection,
  accountId: String,
  primaryExchangeMapping: Map[Symbol, Exchange] = Map.empty
)
