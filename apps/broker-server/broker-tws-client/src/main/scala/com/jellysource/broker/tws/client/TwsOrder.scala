package com.jellysource.broker.tws.client

import java.time.LocalDateTime

import akka.actor.{Actor, ActorLogging, ActorRef, PoisonPill, Props}
import com.ib.client.OrderStatus.{Cancelled, Filled, PreSubmitted, Submitted}
import com.jellysource.broker.domain.order._
import com.jellysource.broker.tws.client.api.OrderHandler.{Error, OrderIdAssigned, OrderStateChanged}
import com.jellysource.broker.tws.client.api.{OrderHandler, TwsApi}
import io.prometheus.client.Histogram

private object TwsOrder {
  def props(accountId: String, dataFactory: TwsDataFactory, order: Order, api: ActorRef, bus: ActorRef) =
    Props(new TwsOrder(accountId, dataFactory, order, api, bus))
}

private class TwsOrder(
  accountId: String,
  dataFactory: TwsDataFactory,
  var order: Order,
  api: ActorRef,
  bus: ActorRef
) extends Actor with ActorLogging {

  // todo to remove warning introduce multiple receive variants, one before place, another one after place with id
  private var internalId: Long = -1
  private var orderPlaceTimer: Histogram.Timer = _
  private var orderFillTimer: Histogram.Timer = _

  override def receive: Receive = {

    case OrderIdAssigned(id) =>
      internalId = id
      log.debug(s"$ThisOrder has assigned internal id")

    case PlaceOrder(id, orderData) =>
      orderPlaceTimer = TwsMonitoring.orderPlacedMonitor.startTimer()
      order = Order.create(id, orderData)
      api ! TwsApi.PlaceOrModifyOrder(dataFactory.contract(orderData.symbol), dataFactory.order(accountId, orderData))

    case CancelOrder(id) =>
      api ! TwsApi.CancelOrder(internalId)

    case OrderHandler.OrderStatusChanged(
        status,
        filled,
        remaining,
        avgFillPrice,
        permId,
        parentId,
        lastFillPrice,
        clientId,
        whyHeld
        ) =>

      log.debug(
        s"$ThisOrder status changed: " +
          s"status = $status, " +
          s"filled = $filled, " +
          s"remaining = $remaining, " +
          s"avgFillPrice = $avgFillPrice, " +
          s"permId = $permId, " +
          s"parentId = $parentId, " +
          s"lastFillPrice = $lastFillPrice, " +
          s"clientId = $clientId, " +
          s"whyHeld = $whyHeld"
      )

      (status, order) match {
        case (Submitted | PreSubmitted | Filled, created: CreatedOrder) =>
          onPlace(created, filled, avgFillPrice)

        case (Submitted | PreSubmitted | Filled, placed: PlacedOrder) =>
          onFirstFill(placed, filled, avgFillPrice)

        case (Submitted | PreSubmitted | Filled, partially: PartiallyFilledOrder) =>
          onNextFill(partially, filled, avgFillPrice)

        case (Submitted | PreSubmitted, _) =>
          TwsMonitoring.errorWrongOrderStateCounter.inc()
          log.error(s"$ThisOrder in wrong state when submitted received: $order")

        case (Filled, filled: FilledOrder) =>
          log.info(s"$ThisOrder  full fill already reported")

        case (Filled, _) =>
          TwsMonitoring.errorWrongOrderStateCounter.inc()
          log.error(s"$ThisOrder in wrong state when filled received: $order")

        case (Cancelled, _) =>
          TwsMonitoring.orderCancelledCounter.inc()
          bus ! OrderCancelled(id = order.id, time = LocalDateTime.now)
          self ! PoisonPill

        case (other, _) =>
          TwsMonitoring.unhandledStatusCounter.inc()
          log.warning(s"$ThisOrder unhandled status: $other") // todo do we need to handle other statuses ?
      }

    case OrderStateChanged(state) =>
      log.debug(
        s"$ThisOrder state changed: " +
          s"commision = ${state.commission}, " +
          s"maxCommision = ${state.maxCommission()}, " +
          s"minCommision = ${state.minCommission()}, " +
          s"status = ${state.getStatus}, " +
          s"commissionCurrency = ${state.commissionCurrency}, " +
          s"equityWithLoan = ${state.equityWithLoan}, " +
          s"initMargin = ${state.initMargin}, " +
          s"maintMargin = ${state.maintMargin}, " +
          s"warningText = ${state.warningText}"
      )

    case Error(202, _) =>
      log.debug(s"$ThisOrder received cancel as error. Ignoring because cancel is reported in status change too.")

    case other =>
      TwsMonitoring.errorUnhandledCounter.inc()
      log.warning(s"$ThisOrder received and unhadled: $other")
  }

  private def onPlace(created: CreatedOrder, filled: Int, avgFillPrice: Double): Unit = {
    if (orderPlaceTimer != null) {
      orderPlaceTimer.observeDuration()
      orderFillTimer = TwsMonitoring.orderFilledMonitor.startTimer()
    }
    val now = LocalDateTime.now()
    order = created.place(now)
    bus ! OrderPlaced(id = order.id, time = now)
    onFirstFill(order.asInstanceOf[PlacedOrder], filled, avgFillPrice)
  }

  private def onFirstFill(placedOrder: PlacedOrder, filled: Int, avgFillPrice: Double): Unit = {
    if (filled > 0) {
      val now = LocalDateTime.now
      order = placedOrder.fill(filled, avgFillPrice, now)
      order match {
        case filled: FilledOrder =>
          reportFilled(filled, now)
        case _: PartiallyFilledOrder =>
          bus ! OrderFilledPartially(id = order.id, time = now, quantity = filled, price = avgFillPrice)
        case _ =>
          // nothing to do
      }
    }
  }

  private def onNextFill(partially: PartiallyFilledOrder, filled: Int, avgFillPrice: Double): Unit = {
    if (filled > 0) {
      if (filled == partially.fills.quantity) {
        log.info(s"$ThisOrder has duplicate fill reported: $filled at $avgFillPrice")
      } else {
        val now = LocalDateTime.now
        val quantity = filled - partially.fills.quantity
        order = partially.fill(quantity, avgFillPrice, now)
        bus ! OrderFilledPartially(id = order.id, time = now, quantity = quantity, price = avgFillPrice)
        order match {
          case filled: FilledOrder => reportFilled(filled, now)
          case _ => // nothing to do
        }
      }
    }
  }

  private def reportFilled(filled: FilledOrder, time: LocalDateTime): Unit = {
    if (orderFillTimer != null) orderFillTimer.observeDuration()
    bus ! OrderFilled(id = order.id, time = time, avgPrice = filled.fills.avgPrice)
  }

  private def ThisOrder: String = s"Order [${order.id}, $internalId]"

}
