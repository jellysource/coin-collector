package com.jellysource.broker.tws.client
import io.prometheus.client.{Counter, Histogram}

private[tws] object TwsMonitoring {

  val connectingCounter: Counter =
    Counter.build()
      .name("tws_connecting")
      .help("Counts how many times connecting to tws is done")
      .register()

  val orderPlacedMonitor: Histogram =
    Histogram.build()
      .name("tws_order_place_seconds")
      .help("Counts how long it takes to place order to tws")
      .register()

  val orderFilledMonitor: Histogram =
    Histogram.build()
      .name("tws_order_fill_seconds")
      .help("Counts how long it takes to fill order after it was placed to tws")
      .register()

  val errorUnhandledCounter: Counter =
    Counter.build()
      .name("tws_order_error_unhandled")
      .help("Counts not expected errors coming from tws during order placing")
      .register()

  val errorWrongOrderStateCounter: Counter =
    Counter.build()
      .name("tws_order_error_state")
      .help("Counts invalid statuses received from tws during order placing")
      .register()

  val unhandledStatusCounter: Counter =
    Counter.build()
      .name("tws_order_status_unhandled")
      .help("Counts not expected statues coming from tws during order placing")
      .register()

  val orderCancelledCounter: Counter =
    Counter.build()
      .name("tws_order_cancelled")
      .help("Counts cancelled orders")
      .register()

}
