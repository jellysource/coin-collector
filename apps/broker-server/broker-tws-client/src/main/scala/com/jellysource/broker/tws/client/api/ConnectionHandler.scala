package com.jellysource.broker.tws.client.api

import java.util

import akka.actor.ActorRef
import com.ib.controller.ApiController.IConnectionHandler
import com.jellysource.broker.tws.client.api.ConnectionHandler._
import com.typesafe.scalalogging.LazyLogging

import scala.collection.JavaConverters._

private[api] object ConnectionHandler {
  case object Connected
  case object Disconnected
  case class AccountList(list: Seq[String])
  case class Error(e: Exception)
  case class Message(id: Int, errorCode: Int, errorMsq: String)
  case class Show(string: String)
}

private[api] class ConnectionHandler(connector: ActorRef) extends IConnectionHandler with LazyLogging {

  override def connected(): Unit = {
    logger.debug("Connected callback called")
    connector ! Connected
  }


  override def disconnected(): Unit = {
    logger.debug("Disconnected callback called")
    connector ! Disconnected
  }


  override def accountList(list: util.ArrayList[String]): Unit = {
    logger.debug(s"Account list callback called $list")
    connector ! AccountList(list.asScala)
  }


  override def error(e: Exception): Unit = {
    logger.debug(s"Error callback called", e)
    connector ! Error(e)
  }


  override def message(id: Int, errorCode: Int, errorMsg: String): Unit = {
    logger.debug(s"Message callback called. Id: $id, error code: $errorCode, error msg: $errorMsg")
    connector ! Message(id, errorCode, errorMsg)
  }


  override def show(string: String): Unit = {
    logger.debug(s"Show callback called: $string")
    connector ! Show(string)
  }

}


