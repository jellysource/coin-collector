package com.jellysource.broker.tws.client.api

import com.ib.client.{CommissionReport, Contract, Execution}
import com.ib.controller.ApiController.ITradeReportHandler
import com.typesafe.scalalogging.LazyLogging

class TradeReportHandler extends ITradeReportHandler with LazyLogging {

  override def tradeReport(tradeKey: String, contract: Contract, execution: Execution): Unit =
    logger.debug(
      s"Trade report: key = $tradeKey, " +
        s"contract = $contract, " +
        s"orderId = ${execution.orderId}, " +
        s"orderId = ${execution.orderId}, " +
        s"clientId = ${execution.clientId}, " +
        s"execId = ${execution.execId}, " +
        s"time = ${execution.time}, " +
        s"acctNumber = ${execution.acctNumber}, " +
        s"exchange = ${execution.exchange}, " +
        s"side = ${execution.side}, " +
        s"shares = ${execution.shares}, " +
        s"price = ${execution.price}, " +
        s"permId = ${execution.permId}, " +
        s"liquidation = ${execution.liquidation}, " +
        s"cumQty = ${execution.cumQty}, " +
        s"avgPrice = ${execution.avgPrice}, " +
        s"orderRef = ${execution.orderRef}, " +
        s"evRule = ${execution.evRule}, " +
        s"evMultiplier = ${execution.evMultiplier}"
    )

  override def tradeReportEnd(): Unit =
    logger.debug(s"Trade report end")

  override def commissionReport(tradeKey: String, commissionReport: CommissionReport): Unit =
    logger.debug(
      s"Commision report: key = $tradeKey, " +
        s"execId: ${commissionReport.m_execId}, " +
        s"commission: ${commissionReport.m_commission}, " +
        s"currency: ${commissionReport.m_currency}, " +
        s"realizedPNL: ${commissionReport.m_realizedPNL}, " +
        s"yield: ${commissionReport.m_yield}, " +
        s"yieldRedemptionDate: ${commissionReport.m_yieldRedemptionDate}"
    )
}
