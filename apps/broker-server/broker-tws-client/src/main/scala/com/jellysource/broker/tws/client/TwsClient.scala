package com.jellysource.broker.tws.client

import akka.actor.{ActorRef, ActorSystem}
import com.jellysource.broker.tws.client.api.TwsApi

class TwsClient(properties: TwsProperties, actorSystem: ActorSystem) {

  private lazy val api: ActorRef = actorSystem.actorOf(TwsApi.props, "tws-api")

  private lazy val dataFactory = TwsDataFactory(properties.primaryExchangeMapping)

  private lazy val connection = properties.connection

  def connect(): ActorRef = {
    api ! TwsApi.Connect(connection.host, connection.port, connection.clientId)
    actorSystem.actorOf(TwsBroker.props(properties.accountId, dataFactory, api), "tws-broker")
  }

  def disconnect(): Unit =
    api ! TwsApi.Disconnect
}
