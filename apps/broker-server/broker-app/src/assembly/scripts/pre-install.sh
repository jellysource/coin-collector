#!/bin/sh
getent group broker || groupadd -r broker
getent passwd broker || useradd -r -d /opt/broker-server/ -s /sbin/nologin -g broker broker

case "$1" in
  1)
    # This is an initial install.
  ;;
  2)
    # This is an upgrade
  ;;
esac