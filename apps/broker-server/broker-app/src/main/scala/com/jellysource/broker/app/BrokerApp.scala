package com.jellysource.broker.app

import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer, Materializer}
import com.jellysource.broker.api.BrokerApiFactory
import com.jellysource.broker.api.publish.AmqpOrderEventPublisherConfig
import com.jellysource.broker.api.rest.RestApiConfig
import com.jellysource.broker.tws.client.{TwsClient, TwsProperties}
import com.typesafe.scalalogging.LazyLogging

import scala.collection.JavaConverters._
import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContextExecutor}

object BrokerApp extends LazyLogging {

  def main(args: Array[String]): Unit = {
    logger.info("Starting coin collector broker application")
    logger.debug(s"System properties:\n ${System.getProperties.asScala.map { case (k, v) => s"$k: $v"}.mkString("\n")}")

    val twsProperties = pureconfig.loadConfigOrThrow[TwsProperties](namespace = "tws")
    val restConfig = pureconfig.loadConfigOrThrow[RestApiConfig]("api.rest")
    val publishConfig = pureconfig.loadConfigOrThrow[AmqpOrderEventPublisherConfig]("api.publish.amqp")

    implicit val actorSystem: ActorSystem = ActorSystem("coin-collector-broker")
    implicit val materializer: Materializer = ActorMaterializer()
    implicit val executionContext: ExecutionContextExecutor = actorSystem.dispatcher

    val twsClient = new TwsClient(twsProperties, actorSystem)
    val broker = twsClient.connect()
    val restBinding = new BrokerApiFactory(restConfig, publishConfig).run(broker)

    sys.addShutdownHook {
      val finishing = restBinding.flatMap(_.unbind).flatMap(_ => actorSystem.terminate())
      logger.info("Stopping coin collector broker application")
      Await.ready(finishing, 10.seconds)
    }
  }

}
