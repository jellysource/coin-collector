package com.jellysource.akka.support.actors

import akka.actor.{Actor, Props}

object Replier {
  def props(reply: => Any) = Props(new Replier(reply))
}

class Replier(reply: => Any) extends Actor {
  override def receive: Receive = {
    case msg => sender() ! reply
  }
}
