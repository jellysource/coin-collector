package com.jellysource.akka.support.fixture

import akka.actor.ActorSystem

trait NeedAkkaSystem {
  implicit def system: ActorSystem
}
