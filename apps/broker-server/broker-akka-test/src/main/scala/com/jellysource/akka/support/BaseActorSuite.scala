package com.jellysource.akka.support

import java.util.concurrent.atomic.AtomicInteger

import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestKit}
import org.scalatest._

import scala.concurrent.Await
import scala.concurrent.duration.DurationInt

sealed trait BaseActorSuite
  extends Suite
  with Matchers
  with BeforeAndAfterAll {
}

abstract class BaseActorSpec extends TestKit(ActorSystem("TestSystem")) with BaseActorSuite with FlatSpecLike {
  override protected def afterAll(): Unit = {
    Await.result(system.terminate(), 10.seconds)
  }
}

// isolated akka fixture idea from https://gist.github.com/derekwyatt/3138807
abstract class BaseActorSpecWithFixture extends BaseActorSuite with org.scalatest.fixture.FlatSpecLike {
  protected type Fixture <: TestKit
  protected type FixtureParam = Fixture
  protected class AkkaFixture
    extends TestKit(ActorSystem("TestSystem-" + BaseActorSpecWithFixture.sysId.incrementAndGet()))
    with ImplicitSender

  def createAkkaFixture(): Fixture

  override def withFixture(test: OneArgTest): Outcome = {
    val fixture = createAkkaFixture()
    try {
      test(fixture)
    } finally {
      Await.result(fixture.system.terminate(), 10.seconds)
    }
  }
}

object BaseActorSpecWithFixture {
  val sysId = new AtomicInteger()
}
