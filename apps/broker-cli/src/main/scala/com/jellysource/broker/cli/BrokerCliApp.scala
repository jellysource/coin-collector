package com.jellysource.broker.cli

import java.util.UUID

import com.jellysource.broker.api.protocol._
import com.jellysource.broker.cli.consumer.{ConsumerConfig, ConsumerFactory}
import com.jellysource.broker.client.{BrokerClient, BrokerClientConfig}
import com.typesafe.scalalogging.LazyLogging

// todo this is just very simple proof of concept, improve later
object BrokerCliApp extends LazyLogging {

  def main(args: Array[String]): Unit = {
    // validate arguments
    if (args.length != 4) {
      println(
        """
          |Not valid arguments count.
          |
          |Usage:
          |  broker-cli action quantity symbol execution
          |
          |    action     buy or sell
          |    quantity   number of units
          |    symbol     symbol of security
          |    execution  market or limit@10.01 or stop@10.01, 10.01 can be replaced by any number and represent price
        """.stripMargin
      )
      System.exit(1)
    }

    // load configs
    val clientConfig = pureconfig.loadConfigOrThrow[BrokerClientConfig]("api.rest")
    val consumerConfig = pureconfig.loadConfigOrThrow[ConsumerConfig]("api.publish.amqp")

    // create clients
    val client = new BrokerClient(clientConfig)
    val consumerFactory = ConsumerFactory.connect(consumerConfig)

    // patterns for limit and stop arguments
    val limit = """limit@(\d+\.?\d*)""".r
    val stop = """stop@(\d+\.?\d*)""".r

    // create order from arguments
    val order =
      Order(
        id = UUID.randomUUID().toString,
        symbol = args(2),
        quantity = args(1).toInt,
        action = args(0) match {
          case "sell" => Sell
          case "buy"  => Buy
        },
        execution = args(3) match {
          case "market"     => Market
          case limit(price) => Limit(price.toDouble)
          case stop(price)  => Stop(price.toDouble)
        }
      )

    // consume events, stop on OrderFilled or OrderCancelled
    consumerFactory.consume(Seq(order.id + ".*"), delivery => {
      println(delivery.event)
      delivery.event match {
        case _: OrderFilled | _: OrderCancelled =>
          delivery.stop()
          consumerFactory.close()
        case _ =>
          // do nothing
      }
    })

    // place order to client
    client.placeOrder(order)
      .onFailure(e => {
        logger.error("Order place request was not successful", e)
        System.exit(1)
      })
  }
}
