package com.jellysource.broker.cli.consumer

case class ConsumerConfig(
  uri: String,
  exchange: String
)

