package com.jellysource.broker.cli.consumer

import com.rabbitmq.client._

class ConsumerFactory(connection: Connection, exchangeName: String) {

  private lazy val defaultChannel = connection.createChannel()
  
  private lazy val exchange = defaultChannel.exchangeDeclare(exchangeName, "topic")

  def consumeAll(consumer: Delivery => Unit): Unit = consume(Seq("#"), consumer)

  def consume(keys: Seq[String], consumer: Delivery => Unit): Unit = {
    val channel = defaultChannel
    val queue = channel.queueDeclare()
    keys.foreach { key =>
      channel.queueBind(queue.getQueue, exchangeName, key)
    }
    channel.basicConsume(queue.getQueue, new DeliveryConsumer(channel, consumer))
  }

  def close(): Unit = {
    connection.close()
  }
}

object ConsumerFactory {

  def connect(config: ConsumerConfig): ConsumerFactory = {
    val connectionFactory = createConnectionFactory(config)
    new ConsumerFactory(
      connection = connectionFactory.newConnection,
      exchangeName = config.exchange
    )
  }

  private def createConnectionFactory(config: ConsumerConfig) = {
    val connectionFactory = new ConnectionFactory()
    connectionFactory.setUri(config.uri)
    connectionFactory
  }
}