package com.jellysource.broker.cli.consumer

import com.rabbitmq.client.{AMQP, Channel, DefaultConsumer, Envelope}

private class DeliveryConsumer(
  channel: Channel,
  consume: Delivery => Unit
) extends DefaultConsumer(channel) {

  override def handleDelivery(
    consumerTag: String,
    envelope: Envelope,
    properties: AMQP.BasicProperties,
    body: Array[Byte]
  ): Unit = {
    val delivery =
      Delivery(
        consumerTag = consumerTag,
        envelope = envelope,
        properties = properties,
        body = body,
        channel = channel
      )
    consume(delivery)
  }

}
