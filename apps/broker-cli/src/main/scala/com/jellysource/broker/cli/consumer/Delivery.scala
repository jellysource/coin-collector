package com.jellysource.broker.cli.consumer

import com.jellysource.broker.api.protocol.OrderEvent
import com.jellysource.broker.api.protocol.json.JsonSerDe
import com.rabbitmq.client.{AMQP, Channel, Envelope}

case class Delivery(
  consumerTag: String,
  envelope: Envelope,
  properties: AMQP.BasicProperties,
  body: Array[Byte],
  channel: Channel
) {

  lazy val event: OrderEvent = JsonSerDe.deserializeOrderEvent(body)

  def stop(): Unit = channel.basicCancel(consumerTag)

}

