# Coin collector - Broker Command Line Interface

Broker CLI proivides command line interface for creating trading orders via **Broker server**.

    Usage:
        broker-cli action quantity symbol execution

        action     buy or sell
        quantity   number of units
        symbol     symbol of security
        execution  market or limit@10.01 or stop@10.01, 10.01 can be replaced by any number and represents price

## How to build

    ./gradlew clean build


## How to run

There are two additional applications need to be started before broker cli.

### Broker server

See [broker's README](../broker-server/README.md) for details.

### RabbitMQ

You need to have running [rabbitMQ](https://www.rabbitmq.com/) server.

Easiest way is to use docker image and just run:

    docker run -it -p 5672:5672 rabbitmq:3.7.14-alpine

### Broker cli

    ./gradlew run --args='ORDER_COMMAND'

Where:
 - **ORDER_COMMAND** command is `action quantity symbol execution` e.g. `buy 1 T market`

## How to publish

    ./gradlew bintrayUpload -PbintrayUser=YOUR_USER -PbintrayKey=YOUR_KEY

You need replace YOUR_USER and YOUR_KEY with bintray credentials.

## Deployment

There are two formats of resulting packages `TAR` and `ZIP`.

To create `TAR` package run

    ./gradlew distTar

To create `ZIP` package run

    ./gradlew distZip

### How to install

Just simply untar (or uzip) resulting package (e.g. into your home directory)

    tar -C ~ -xvf build/distributions/broker-cli*.tar

### Where it is installed

    ~/broker-cli-${VERSION}/
        ├── bin
        ├── conf
        └── lib

### How to run

    ~/broker-cli*/bin/broker-cli ${ORDER_COMMAND}

Where:
 - **ORDER_COMMAND** command is `action quantity symbol execution` e.g. `buy 1 T market`

### How to configure

Configuration file can be found under ~/broker-cli*/conf/cli.conf. It is written in [HCON](https://github.com/lightbend/config/blob/master/HOCON.md) format.

| Property name                 | Default value | Description                                   |
|-------------------------------|---------------|-----------------------------------------------|
| api.rest.host                 | localhost     | host name where broker REST API is running    |
| api.rest.port                 | 9001          | port on which broker REST API is listening    |
| api.rest.uri                  | http://${api.rest.host}:${api.rest.port} | full URI to broker REST API |
| api.publish.amqp.host         | localhost     | host where messaging broker supporting [AMQP protocol](https://www.amqp.org/) is running |
| api.publish.amqp.port         | 5672          | port on which messaging broker is listening   |
| api.publish.amqp.user         | guest         | username used to connect to messaging broker  |
| api.publish.amqp.password     | guest         | password userd to connect to messageing broker |
| api.publish.amqp.uri          | amqp://${ api.publish.amqp.user }:${ api.publish.amqp.password }@${ api.publish.amqp.host }:${ api.publish.amqp.port } | full connection URI to message broker, some additional connection parameters can be added here |
| api.publish.amqp.exchange     | coin.order.event | name of exchange from which ored events will be consumed |
