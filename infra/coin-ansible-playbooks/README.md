# Coin collector - Coin ansible playbooks

Project containing [Ansible](https://www.ansible.com/) scripts for creating infrastructure of Coin collector system.

## How to setup all

To setup whole system run:

    ./play site

> You need to have local installation of [Ansible](https://www.ansible.com/).

> You can replace `./play` with directly calling `ansible-playbook` from directory `src/main/resources/` and with adding `.yml` to the name of playbook.

## How to setup database

To setup database server and create [Coin database](../../dbs/coin-db/README.md) run:

    ./play db

If you only need to apply [Coin database](../../dbs/coin-db/README.md) changes run:

    ./play db --tags coindb

## How to setup messaging

To setup **messaging** run:

    ./play messaging

## How to setup quotecenter

To setup [Quotecenter](../../apps/quotecenter/README.md) run:

    ./play quotecenter

## How to setup strategy executor

To setup [Strategy executor](../../apps/strategy-executor/README.md) run:

    ./play executor

## How to setup broker

To setup [Broker server](../../apps/broker-server/README.md) and all it's dependencies run:

    ./play broker

If you only need to setup [Broker server](../../apps/broker-server/README.md) application run:

    ./play broker --tags app

If you need to setup TWS run:

    ./play broker --tags tws

## How to setup monitoring

To setup **monitoring** run:

    ./play monitoring


