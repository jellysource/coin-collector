#!/bin/sh

# finds directory with highest numeric value within specified directory or current

while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -d|--dir)
    DIR="$2"
    shift
    shift
    ;;
    --default)
    DIR="."
    shift
    ;;
esac
done
my=($(ls -la $DIR|awk {'print $9'}))

numreg='^[0-9]+$'
highest=0
for i in "${my[@]}"
do
    if [[ ($i =~ $numreg) && ($i -gt $highest) ]]
    then
        highest=$i
    fi
done
echo $highest