#!/bin/bash

##
# Helper script for running ansible playbooks.
# 
# Usage:
#   ./play playbook_name
##

set -e

# Find script directory
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

# Look for ansible directory in grand parent's dir by default
if [[ -z $COIN_PLAYBOOKS_DIR ]]; then
    COIN_PLAYBOOKS_DIR=$DIR/../ansible
fi

if [[ -z $1 ]]; then
    echo "You need to pass name of the playbook under $COIN_PLAYBOOKS_DIR as argument"
    echo ""
    echo "Choose one of following playbooks:"
    echo "-----------------------------------"
    (cd $COIN_PLAYBOOKS_DIR && ls -1 *.yml) 
    exit 1
fi

PLAYBOOK=$1
if [[ ! ${PLAYBOOK} =~ \.yml$ ]]; then 
    PLAYBOOK="$PLAYBOOK.yml"
fi

(cd $COIN_PLAYBOOKS_DIR && ansible-playbook ${PLAYBOOK} ${@:2})
