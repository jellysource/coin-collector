#!/bin/sh
getent group coin || groupadd -r coin
getent passwd ansible || useradd -r -d /opt/coin-ansible-playbooks -s /sbin/nologin -g coin ansible

case "$1" in
  1)
    # This is an initial install.
  ;;
  2)
    # This is an upgrade
  ;;
esac