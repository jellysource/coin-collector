#!/bin/sh

# resolve links - $0 may be a softlink
PRG="$0"
while [ -h "$PRG" ]; do
  ls=`ls -ld "$PRG"`
  link=`expr "$ls" : '.*-> \(.*\)$'`
  if expr "$link" : '/.*' > /dev/null; then
    PRG="$link"
  else
    PRG=`dirname "$PRG"`/"$link"
  fi
done

# Get standard environment variables
PRGDIR=`dirname "$PRG"`
[ -z "$LIQUIBASE_HOME" ] && LIQUIBASE_HOME=`cd "$PRGDIR/.." >/dev/null; pwd`

# import configuration
if [ -z "$LIQUIBASE_CONF" ]; then
    LIQUIBASE_CONF=coin-db
fi

source $LIQUIBASE_HOME/conf/$LIQUIBASE_CONF

# Validation
if [ -z "$JAVA_HOME" ]; then
    echo "JAVA_HOME is not set"
    exit 1
fi

if [ -z "$LIQUIBASE_HOME" ]; then
    echo "LIQUIBASE_HOME is not set"
    exit 1
fi

if [ -z "$LIQUIBASE_DRIVER" ]; then
    echo "LIQUIBASE_DRIVER is not set"
    exit 1
fi

if [ -z "$LIQUIBASE_JDBC_URL" ]; then
    echo "LIQUIBASE_JDBC_URL is not set"
    exit 1
fi

if [ -z "$LIQUIBASE_USERNAME" ]; then
    echo "LIQUIBASE_USERNAME is not set"
    exit 1
fi

if [ -z "$LIQUIBASE_PASSWORD" ]; then
    echo "LIQUIBASE_PASSWORD is not set"
    exit 1
fi

if [ -z "$LIQUIBASE_CHANGELOG" ]; then
    LIQUIBASE_CHANGELOG="db/changelog/db.changelog-master.xml"
fi

if [ -z "$LIQUIBASE_LOGFILE" ]; then
    LIQUIBASE_LOGFILE="$LIQUIBASE_HOME/log/coin-db.log"
fi

if [ -z "$LIQUIBASE_CONTEXT" ]; then
    LIQUIBASE_CONTEXT="prod"
fi

if [ -z "$LIQUIBASE_PARAMS" ]; then
    LIQUIBASE_PARAMS=""
fi

echo "using JAVA_HOME:           $JAVA_HOME"
echo "using LIQUIBASE_HOME:      $LIQUIBASE_HOME"
echo "using LIQUIBASE_OPTS:      $LIQUIBASE_OPTS"
echo "using LIQUIBASE_DRIVER:    $LIQUIBASE_DRIVER"
echo "using LIQUIBASE_JDBC_URL:  $LIQUIBASE_JDBC_URL"
echo "using LIQUIBASE_USERNAME:  $LIQUIBASE_USERNAME"
echo "using LIQUIBASE_LOGFILE:   $LIQUIBASE_LOGFILE"
echo "using LIQUIBASE_CHANGELOG: $LIQUIBASE_CHANGELOG"
echo "using LIQUIBASE_CONTEXT:   $LIQUIBASE_CONTEXT"

LIQUIBASE_PARAMS="$LIQUIBASE_PARAMS --changeLogFile=$LIQUIBASE_CHANGELOG"
LIQUIBASE_PARAMS="$LIQUIBASE_PARAMS --driver=$LIQUIBASE_DRIVER"
LIQUIBASE_PARAMS="$LIQUIBASE_PARAMS --username=$LIQUIBASE_USERNAME"
LIQUIBASE_PARAMS="$LIQUIBASE_PARAMS --password='$LIQUIBASE_PASSWORD'"
LIQUIBASE_PARAMS="$LIQUIBASE_PARAMS --url=$LIQUIBASE_JDBC_URL"

LIQUIBASE_PARAMS="$LIQUIBASE_PARAMS --includeSystemClasspath=true"
LIQUIBASE_PARAMS="$LIQUIBASE_PARAMS --logFile=$LIQUIBASE_LOGFILE"
LIQUIBASE_PARAMS="$LIQUIBASE_PARAMS --logLevel=info"
LIQUIBASE_PARAMS="$LIQUIBASE_PARAMS --contexts=$LIQUIBASE_CONTEXT"

CMD="$JAVA_HOME/bin/java $LIQUIBASE_OPTS -cp '$LIQUIBASE_HOME/lib/*:' liquibase.integration.commandline.Main $LIQUIBASE_PARAMS"
eval $CMD "$@"
