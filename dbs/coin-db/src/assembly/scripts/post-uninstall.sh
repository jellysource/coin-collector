#!/bin/sh

case "$1" in
  0)
    # This is complete un-install of old files
  ;;
  1)
    # This is an upgrade (old files were removed)
    # It's disabled, must be manually executed (preventing multiple updates on multiple machines)
    # /usr/share/wd-omni-db/bin/omni-db.sh update > /var/log/omni/omni-db/install.out 2> /var/log/omni/omni-db/install.err
  ;;
esac
