# Coin collector - Coin database

Coin db is module containing migration and operation scripts for shared database used currently by `quotecenter` and `strategy executor` modules.

## Multiple database engines support

Migration scripts are written using [liquibase](https://www.liquibase.org/) and therefor can be used teoretically to create databse in any of [supported database engines](https://www.liquibase.org/databases.html).

We support building for following engines:
 - [HSQLDB](http://hsqldb.org/) - in file database for simple local testing
 - [PostgreSQL](https://www.postgresql.org/) - production database

### How to build for HSQLDB

HSQLDB is predefined as default database engine so it's enough to run

    ./gradlew clean buildRpm

Alternativelly to be more specific you can add `db` property

    ./gradlew clean buildRpm -Pdb=local-hsqldb

### How to apply database migration scripts from Gradle for HSQLDB

    ./gradlew update

To insert test data too use 

    ./gradlew update -Pliquibase.contexts="test"

### How to build for PostgreSQL

To build for PostgreSQL you need to set `db` property to `local-postgres` value

    ./gradlew clean buildRpm -Pdb=local-postgres

### How to apply database migration scripts from Gradle for PostgreSQL

    ./gradlew update -Pdb=local-postgres

To insert test data too use 
    
    ./gradlew update -Pdb=local-postgres -Pliquibase.contexts="test"

## How to publish

    ./gradlew bintrayUpload -Pdb=local-postgres -PbintrayUser=YOUR_USER -PbintrayKey=YOUR_KEY

You need replace YOUR_USER and YOUR_KEY with bintray credentials.
    
## Deployment

Resulting package is `RPM` which can be installed on some machine. It contains operational scripts for updating database to latest version.

### How to install

    sudo yum install coin-db

### Where it is installed

    /opt/coin-db/
        ├── bin
        ├── conf -> /etc/coin-db
        ├── lib
        └── log  -> /var/log/coin-db

### How to run migration to latest version

    sudo -u coin /opt/coin-db/bin/coin-db.sh update

### How to configure

Configuration file can be found on path `/etc/coin-db/coin-db`. It is shell script containing variables definitions.

| Variable name         | Default value | Descrition                        |
|-----------------------|---------------|-----------------------------------|
| JAVA_HOME             |               | path to java runtime installation |
| LIQUIBASE_DRIVER      |               | JDBC driver class name (`org.hsqldb.jdbc.JDBCDriver` or `org.postgresql.Driver`) |
| LIQUIBASE_JDBC_URL    |               | JDCB url to target database       |
| LIQUIBASE_USERNAME    |               | user with rights to do all DDL scrtipts |
| LIQUIBASE_PASSWORD    |               | password for database user        |
| LIQUIBASE_LOGFILE     | coin-db.log   | relative path to log file under `$LIQUIBASE_HOME/log` |
| LIQUIBASE_CONTEXT     | prod          | context of migration scripts (`test` or `prod`) |





